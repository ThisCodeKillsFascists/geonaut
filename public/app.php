<?php

if (isset($_GET['_phpinfo_'])) {
  phpinfo();
  return;
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

DEFINE('__ROOT__', realpath(__DIR__ . '/..'));

// Get the host name
$hostName = $_SERVER['HTTP_HOST'];
// Check if the host name includes .local or localhost
DEFINE('__LOCAL_DEV__', strpos($hostName, '.local') !== false || strpos($hostName, 'localhost') !== false);


if (isset($_GET['_bootload_'])) {
  require_once '../app/bootload/bootload.php';
  return;
}

// we "init()" the thread
require_once '../app/kernel/AppKernel.php';



\Kernel\AppKernel::init();
