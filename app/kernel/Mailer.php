<?php

namespace Kernel;

use Core\Config;

/**
 * Send an email using Mandrillapp. Set the &lt;key&gt; from <b>/app/config/config.ini</b>
 * @url http://mandrillapp.com
 * @package Kernel
 */
class Mailer
{

  /**
   * @var Mailer|false $instance A Mailer instance
   * @internal
   */
  private static $instance = null;

  /**
   * @var \Mandrill A Mandrillapp instance, responsible of sending the email.
   */
  var $mail;

  /**
   * @var Config The instance of the Config class.
   */
  protected $config;

  protected function __construct()
  {
    $this->config = Config::singleton();

    self::$instance = $this;
  }

  /**
   * Returns a Mailer instance, creating it if it did not exist.
   * @return Mailer
   */
  public static function singleton()
  {
    if (static::$instance === null) {
      $v = __CLASS__;
      static::$instance = new $v();
    }

    return static::$instance;
  }

  /**
   * Sends an email using the PHP function, but first setting the appropriate headers, content-types... and <b>attached files</b>.
   * @param mixed $to Send it to whom? can be a string "john&#64;example.com", an array('john&#64;example.com', 'John Smith') or an array('Addresses' => array(array('john&#64;example.com', 'John Smith'), 'peter&#64;example.com', ....))
   * @param string $subject The message subject for the email
   * @param string $message The message body
   * @param mixed $from array('somebody&#64;example.com', 'Foo Bar');
   * @param mixed $replyTo 'somebody&#64;example.com'. The email address to reply to;
   * @param mixed $bcc 'im-secret&#64;example.com'. The email address to bcc to;
   * @param array $files (default array() ) Array with the filenames of the desired files to attach.
   * @throws \Exception:Mandril_Error
   * @throws \Exception
   * @return boolean TRUE if sending was OK, FALSE otherwise. This <b>does not mean</b> the email will be delivered: it just means there was no error when sending it
   * @author Nico Kupfer
   */
  public function sendEmail($to, $subject, $message, $from = [], $replyTo = '', $bcc = '', $files = array())
  {

    if (!$to || !$subject || !$message) {
      return FALSE;
    }

    // If only the address is set, we set name=address
    if (is_string($from)) {
      $from = [$from, $from];
    }
    // If only the address is set, we set name=address
    if (is_string($to)) {
      $to = [$to, $to];
    }

    if (empty($from)) {
      $from = $this->config->get('Email', 'FROM');
    }

    $content = [
      [
        'type' => "text/html",
        'value' => $message
      ]
    ];

    try {
      $url = 'https://api.sendgrid.com/v3/mail/send';
      $data = [
        'personalizations' => [
          [
            'to' => [
              [
                'email' => $to[0],
                'name' => $to[1]
              ]
            ],
            'subject' => $subject
          ]
        ],
        'content' => $content,
        'from' => [
          'email' => $from[0],
          'name' => $from[1]
        ],
        // 'reply_to' => $replyTo ? [
        //   'email' => $replyTo,
        //   'name' => $replyTo
        // ] : null
      ];

      $curl = curl_init();

      curl_setopt_array($curl, [
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_HTTPHEADER => [
          'Authorization: Bearer ' . $this->config->get('Email', 'SENDGRID_API_KEY'),
          'Content-Type: application/json'
        ],
        CURLOPT_POSTFIELDS => json_encode($data),
      ]);

      $response = curl_exec($curl);
      $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);

      if ($http_code === 202) {
        return true;
      } else {
        $error_message = json_decode($response, true)['errors'][0]['message'] ?? 'Unknown error';
        return ('Failed to send email. HTTP status code: ' . $http_code . '. Error: ' . $error_message);
      }
    } catch (\Exception $e) {
      // echo 'Caught exception: ' . $e->getMessage() . "\n";
      return $e->getMessage();
    }
  }
}
