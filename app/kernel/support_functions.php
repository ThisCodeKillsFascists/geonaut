<?php


/**
 * &lt;pre&gt;var_dump($var);&lt;/pre&gt; die();
 * @param mixed $var
 */
function ddie($var = NULL)
{
  echo '<pre>';
  var_dump($var);
  die();
}

/**
 * print_r($var); die();
 */
function ppie($var = NULL)
{
  print_r($var);
  die();
}

/**
 * Prints PHP code inside a tag, for further execution.
 * @param string $php
 * @param bool $return = FALSE
 * @example <code>php("echo 'Copyright ', date('Y')")</code> will echo <code><?php echo 'Copyright ', date('Y')?></code> and php will parse it.
 * @return string the $php code inside &lt;?php ?&gt; tags
 */
function php($php, $return = FALSE)
{
  if (!$return) {
    echo '<?php ' . $php . ' ?>';
  } else {
    return '<?php ' . $php . ' ?>';
  }
}

/**
 * Returns whether the PHP server is localhost or remote. True if the host matches <b>*.local</b> or <b>localhost</b>.
 * From the Symfony framework.
 * @return bool TRUE if the $_SERVER is a local machine; FALSE if it's a remote machine
 */
function isLocalServer()
{
  $remoteServer = (
    isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || !(in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', 'fe80::1', '::1'))
      || php_sapi_name() === 'cli-server')
  );
  return !$remoteServer;
}

/**
 * All <b>Fatal Error</b>s enter this function automatically.
 * Hence we use this to register/catch a Fatal Error and log it (<b>\Kernel\Logger</b>).
 */
// register_shutdown_function(function () {
  // $error = error_get_last();

  // if ($error !== NULL) {
  //   \Kernel\Logger::error($error);
  // }
// });

/**
 * spl_autoload_register is automatically called when PHP can't find a specified class.
 * This scans some folders and includes the aforementioned file.
 */
spl_autoload_register(function ($className) {
  $exploded = explode('\\', $className);

  // We get the namespace... if it has one
  if (count($exploded) == 1) {
    $namespace = "";
    $name = $exploded[0];
  } else {
    list($namespace, $name) = $exploded;
  }

  // local redeclaration; it's faster
  $root = __ROOT__;

  if ($namespace) {
    if (file_exists($req = $root . '/app/kernel/' . $name . '.php')) {
      // ...
    } elseif (file_exists($req = $root . '/app/controllers/' . $name . '.php')) {
      // ...
    } elseif (file_exists($req = $root . '/app/models/' . $name . '.php')) {
      // ...
    } else {
      return false;
    }
  } else {
    if (file_exists($req = $root . '/src/controllers/' . $name . '.php')) {
      // ...
    } elseif (file_exists($req = $root . '/src/models/' . $name . '.php')) {
      // ...
    } else {
      return false;
    }
  }
  // Require the file, if any
  require_once $req;
  return TRUE;
});

/**
 * Returns an image of the <b>\$text</b>, to avoid spam.
 * @param string $text The text to convert to image
 * @param array|[175,20] $size The size of the image
 * @param array|[50,50,50] $color The RGB values of the text color
 */
function email_png($text, $size = array(175, 20), $color = array(50, 50, 50))
{
  header('Content-type: image/png');

  $im = imagecreate($size[0], $size[1]);
  // Create some colors
  $white = imagecolorallocate($im, 255, 255, 255);
  $color = imagecolorallocate($im, $color[0], $color[1], $color[2]);
  imagestring($im, 5, 0, 0, $text, $color);
  // Using imagepng() results in clearer text compared with imagejpeg()
  imagepng($im);
  imagedestroy($im);
}

/**
 * Starts a clock to compute execution time. Set a key if you want to use more than one.
 * @param string $key An optional key to compute overlapping times.
 * @example
 * <code>
 * clock_start('all');
 * clock_start('query');
 *
 * $result = query('SELECT * ...')
 * clock_end('query');
 *
 * doSomething($result);
 * echo $result;
 * clock_end('all');
 *
 * $queryTime = clock_time('query');
 * $totalTime = clock_time('all');
 * </code>
 * @return float The time the clock starts; format: seconds.microseconds
 */
function clock_start($key = 'main')
{
  $time = explode(' ', microtime());
  $start = (float)$time[0] + (float)$time[1];
  $GLOBALS['_clock_'][$key]['start'] = $start;
  return $start;
}

/**
 * Stops a clock that was previously set. Returns 0 if the clock has not been started.
 * @param string $key The clock you want to stop.
 *
 * @return float The time the clock stops; format: seconds.microseconds.
 */
function clock_end($key = 'main')
{
  if (!$GLOBALS['_clock_'][$key]) {
    return 0;
  }
  $time = explode(' ', microtime());
  $end = (float)$time[0] + (float)$time[1];
  $GLOBALS['_clock_'][$key]['end'] = $end;
  return $end;
}

/**
 * Returns the execution time of a previously stopped clock. Returns -1 if the clock has not been stopped.
 * @param string $key The clock you want the total time of.
 * @param bool|false $all Returns [end_time - start_time]. If true, it also returns start_time and end_time
 *
 * @return float The total time; format: seconds.microseconds
 */
function clock_time($key = 'main', $all = false)
{
  if (!empty($GLOBALS['_clock_'][$key]['end'])) {
    return -1;
  }
  $GLOBALS['_clock_'][$key]['diff'] = $GLOBALS['_clock_'][$key]['end'] - $GLOBALS['_clock_'][$key]['start'];
  if (!$all) {
    return $GLOBALS['_clock_'][$key]['diff'];
  } else {
    return [
      'start' => $GLOBALS['_clock_'][$key]['start'],
      'end' => $GLOBALS['_clock_'][$key]['end'],
      'diff' => $GLOBALS['_clock_'][$key]['diff']
    ];
  }
}

/**
 * @return string The environment (global var <b>__ENVIRONMENT__</b>) you're working in.
 */
function env()
{
  return defined('__ENVIRONMENT__') ? __ENVIRONMENT__ : FALSE;
}

/**
 * @return bool Whether the environment (global var <b>__ENVIRONMENT__</b>) is production (<b>= "prod"</b>)
 */
function isProd()
{
  return defined('__ENVIRONMENT__') &&  __ENVIRONMENT__ == 'prod';
}

/**
 * @return bool Whether the environment (global var <b>__ENVIRONMENT__</b>) is developement (<b>= "dev"</b>)
 */
function isDev()
{
  return defined('__ENVIRONMENT__') && __ENVIRONMENT__ == 'dev';
}

function isAuth()
{
  return \Core\Session::isAuthenticated();
}

function getAttachmentFiles($directory) {
  $files = glob($directory . '/' . __ATTACHMENT_PREFIX__ . '*');
  return array_map('basename', $files);
}

/**
 * Saves an associative array to a file <b>${path}.ini</b>
 * @param $assoc_arr array The array to save
 * @param bool|false $has_sections Whether the config file has sections or is of the form &lt;KEY&gt;=&lt;VALUE&gt;
 * @param string $path The absolute path to save the $assoc_arr to
 *
 * @return array <b>[success => &lt;success&gt;, reason => &lt;When an error, returns the reason&gt;]</b>
 */
function save_ini_file($assoc_arr, $path, $has_sections = FALSE)
{
  if (empty($assoc_arr) || !count($assoc_arr)) {
    return ['success' => 0, 'reason' => "Array is empty. You might not have any active plugins"];
  }
  $content = "";

  if ($has_sections) {
    foreach ($assoc_arr as $key => $elem) {
      $content .= "[" . $key . "]\n";
      foreach ($elem as $key2 => $elem2) {
        if (is_array($elem2)) {
          for ($i = 0; $i < count($elem2); ++$i) {
            $content .= $key2 . "[] = \"" . $elem2[$i] . "\"\n";
          }
        } elseif ($elem2 == "") {
          $content .= $key2 . " = \"\"\n";
        } else {
          $content .= $key2 . " = \"" . $elem2 . "\"\n";
        }
      }
      $content .= "\n";
    }
  } else {
    foreach ($assoc_arr as $key2 => $elem2) {
      if (is_array($elem2)) {
        for ($i = 0; $i < count($elem2); ++$i) {
          $content .= "    " . $key2 . "[] = \"" . $elem2[$i] . "\"\n";
        }
      } elseif ($elem2 == "") {
        $content .= $key2 . " = \"\"\n";
      } else {
        $content .= $key2 . " = \"" . $elem2 . "\"\n";
      }
      $content .= "\n";
    }
  }
  $handle = NULL;
  if (!$handle = fopen($path, 'w')) {
    return ['success' => 0, 'reason' => 'Cannot open handle "' . $path . '"'];
  }
  if (!fwrite($handle, $content)) {
    return ['success' => 0, 'reason' => 'Cannot write $content into "' . $path . '"'];
  }
  fclose($handle);
  if (function_exists('shell_exec')) {
    shell_exec('chmod 666 ' . $path);
  }
  return ['success' => 1];
}

/**
 * Returns the client's IP address.
 *
 * @return string client's ip address.
 */
function getClientIP()
{
  if (isset($_SERVER['HTTP_CLIENT_IP']))
    return $_SERVER['HTTP_CLIENT_IP'];
  else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    return $_SERVER['HTTP_X_FORWARDED_FOR'];
  return $_SERVER['REMOTE_ADDR'];
}

/**
 * Returns the client's User Agent (if any).
 *
 * @return string browser's UA (user agent).
 */
function getClientUserAgent()
{
  if (isset($_SERVER['HTTP_USER_AGENT'])) {
    return $_SERVER['HTTP_USER_AGENT'];
  } else {
    return "";
  }
}

/**
 * Spit headers that force cache volatility.
 *
 * @return void
 */
function nocache()
{
  header('Expires: Tue, 13 Mar 1979 18:00:00 GMT');
  header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
  header('Cache-Control: no-store, no-cache, must-revalidate');
  header('Cache-Control: post-check=0, pre-check=0', false);
  header('Pragma: no-cache');
}

/**
 * Copy or move a folder recursively
 *
 * @param string $src The source folder
 * @param string $dst The destination folder
 * @param string $action The action to perform: copy (default) or rename (= move)
 */
function folder_recurse($src, $dst, $action = 'copy')
{
  $dir = opendir($src);
  @mkdir($dst);
  while (false !== ($file = readdir($dir))) {
    if (($file != '.') && ($file != '..')) {
      if (is_dir($src . '/' . $file)) {
        folder_recurse($src . '/' . $file, $dst . '/' . $file, $action);
      } else {

        $action($src . '/' . $file, $dst . '/' . $file);
      }
    }
  }
  closedir($dir);
}

/**
 * Deletes a directory and all the files in it
 */
function deleteDir($dirPath)
{
  if (!is_dir($dirPath)) {
    throw new InvalidArgumentException("$dirPath must be a directory");
  }
  if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
    $dirPath .= '/';
  }
  $files = glob($dirPath . '*', GLOB_MARK);
  foreach ($files as $file) {
    if (is_dir($file)) {
      deleteDir($file);
    } else {
      unlink($file);
    }
  }
  rmdir($dirPath);
}

/**
 * Returns a web path for a given selfie hash
 * @param $hash
 * @param $type
 *
 * @return string
 */
function selfie($hash, $type)
{
  return implode('/', [\Controller::singleton()->uploadPath, $hash, $type]);
}

/**
 * @param $str string
 * @returns string unescaped string
 */
function escape_htmlentities(&$str)
{
  $str = preg_replace_callback(
    "/(&#[0-9]+;)/",
    function ($m) {
      return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES");
    },
    $str
  );
}

/**
 * Validates that a given date has the given format.
 * @param $date
 * @param string $format
 *
 * @return bool
 */
function validateDate($date, $format = 'Y-m-d')
{
  $d = DateTime::createFromFormat($format, $date);
  // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
  return $d && $d->format($format) === $date;
}

function unescape($str)
{
  return html_entity_decode($str, HTML_ENTITIES, 'UTF-8');
}

// Function to get the Authorization header
function getAuthorizationHeader()
{
  $headers = null;
  if (isset($_SERVER['Authorization'])) {
    $headers = trim($_SERVER["Authorization"]);
  } elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) { // Nginx or fast CGI
    $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
  } elseif (function_exists('apache_request_headers')) {
    $requestHeaders = apache_request_headers();
    // Server-side fix for bug in old versions of Apache
    if (isset($requestHeaders['Authorization'])) {
      $headers = trim($requestHeaders['Authorization']);
    }
  }
  return $headers;
}

// Function to get the Bearer token from the header
function getBearerToken()
{
  $headers = getAuthorizationHeader();
  if (!empty($headers)) {
    if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
      return $matches[1];
    }
  }
  return null;
}


use Firebase\JWT\JWT;
use Firebase\JWT\Key;
// use Firebase\JWT\SignatureInvalidException;
// use Firebase\JWT\BeforeValidException;
// use Firebase\JWT\ExpiredException;

function generateToken($user_data)
{
  // Current time in milliseconds
  $currentTimeSeconds = round(microtime(true));
  // Create the payload
  $payload = [
    'iss' => 'https://travel.kupfer.es',  // Issuer
    'aud' => 'https://travel.kupfer.es',  // Audience
    'iat' => $currentTimeSeconds,                // Issued at
    'nbf' => $currentTimeSeconds,           // Not before (optional)
    'exp' => $currentTimeSeconds + 3600 * 24 * 30, // Expiration time 1 month
    'data' => $user_data
  ];

  // Encode the JWT
  try {
    $jwt = JWT::encode($payload, __SECRET_JWT_KEY__, 'HS256');
  } catch (\Exception $e) {
    var_dump($e);
  }

  return $jwt;
}

function extractToken($token)
{
  JWT::$leeway = 60; // $leeway in seconds
  $decoded = JWT::decode($token, new Key(__SECRET_JWT_KEY__, 'HS256'));
  // } catch (InvalidArgumentException $e) {
  //   var_dump("provided key/key-array is empty or malformed.", $e);
  // } catch (DomainException $e) {
  //   var_dump("DomainException", $e);
  // provided algorithm is unsupported OR
  // provided key is invalid OR
  // unknown error thrown in openSSL or libsodium OR
  // libsodium is required but not available.
  // } catch (SignatureInvalidException $e) {
  //   var_dump("SignatureInvalidException", $e);
  //   // provided JWT signature verification failed.
  // } catch (BeforeValidException $e) {
  //   var_dump("BeforeValidException", $e);
  //   // provided JWT is trying to be used before "nbf" claim OR
  //   // provided JWT is trying to be used before "iat" claim.
  // } catch (ExpiredException $e) {
  //   var_dump("ExpiredException", $e);
  // provided JWT is trying to be used after "exp" claim.
  // } catch (UnexpectedValueException $e) {
  //   var_dump("UnexpectedValueException", $e);
  // provided JWT is malformed OR
  // provided JWT is missing an algorithm / using an unsupported algorithm OR
  // provided JWT algorithm does not match provided key OR
  // provided key ID in key/key-array is empty or invalid.
  return empty($decoded) ? null : json_decode(json_encode($decoded->data), true);
}
