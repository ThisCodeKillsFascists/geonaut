<?php

namespace Kernel;

use Core\Config;
use Core\Session;


/**
 * Front logic. Dispatches and manages the input flow thread.<br/>
 * Contains many of all the $hooks present in the framework.
 * @hooks
 * <code>
 * ('After_Hooks_Setup', $hooks) // called right after the constructor
 * ('exec_beforestart', ['controller' => $controllerName, 'action' => $action]) // just after {@see Router::matchRoute}
 * ('permission_unallowed', [
 *     'permission' => $permission,
 *     'controller' => $controllerName,
 *     'action' => $action]) // when it's an unauthorized request
 * ('general_exception', ['e' => $e]) // when/if \Exception is thrown
 * ('exec_afterend', ['controller' => $controllerName, 'action' => $action]) // When execution has finished
 * </code>
 * @package Kernel
 */
class AppKernel
{

  /**
   * Initializes the framework.
   * @throws \Exception When a requested <b>Controller</b> or Controller-><b>action</b>() are requested but do not exist.
   */
  public static function init()
  {
    /**
     * Disable CORS
     */
    $allowedOrigins = ['https://travel.kupfer.es', 'http://localhost:3000', 'https://geonaut.vercel.app'];
    $origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : end($allowedOrigins);

    if (in_array($origin, $allowedOrigins)) {
      header("Access-Control-Allow-Origin: $origin");
      header('Access-Control-Allow-Credentials: true');
      header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS');
      header('Access-Control-Allow-Headers: Origin, Accept, Content-Type, Authorization, X-Requested-With');
      if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
        exit; // Exit early if the request method is OPTIONS
      }
    }


    /**
     * Add the core logic
     */
    require_once 'includes.php';

    /**
     * Compute running time
     * */
    clock_start();

    /**
     * Initialize hooks
     */
    $hooks = $GLOBALS['hooks'] = new \Core\Hooks();
    $hooks->do_action('After_Hooks_Setup', $hooks);

    /**
     * Load config files
     */
    $config = Config::singleton()
      ->load(__LOCAL_DEV__ ? 'config.local.ini' : 'config.ini')
      ->load('permissions.ini', FALSE, 'Permissions')
      ->load('routing.ini', FALSE, 'Routing')
      ->load(__ROOT__ . '/src/config/permissions.ini', TRUE, 'Permissions')
      ->load(__ROOT__ . '/src/config/routing.ini', TRUE, 'Routing')
      ->loadVendors();

    // Set cookie parameters with SameSite=None and Secure attribute
    session_set_cookie_params([
      'lifetime' => 0,
      'path' => '/',
      'secure' => true, // Requires HTTPS
      'httponly' => true,
      'samesite' => 'None' // Set SameSite attribute to None
    ]);

    /**
     * We start the session
     */
    session_start();

    // Get the Bearer token
    $token = getBearerToken();
    $payload = null;

    if ($token) {
      try {
        // Decode the JWT
        $payload = extractToken($token);
        Session::setUser($payload);
        Session::setRole($payload["role"]);
      } catch (\Exception $e) {
        header('HTTP/1.0 401 Unauthorized');
        die(json_encode([
          'success' => 0,
          'responseData' => [
            'message' =>  $e->getMessage(),
          ]
        ]));
      }
    }

    /**
     * Get called action
     */
    $routeKey = Router::matchRoute();
    $route = $config->get('Routing', $routeKey);
    if (empty($route['action'])) {
      die(json_encode([
        'success' => 0,
        'responseData' => [
          'message' => 'This is not an API route',
          'data' => $route
        ]
      ]));
    }
    list($controllerName, $action) = explode('::', $route['action']);

    /**
     * We run the hook 'exec_beforestart' to allow for preloads.
     */
    $hooks->do_action('exec_beforestart', ['controller' => $controllerName, 'action' => $action]);

    /**
     * Call requested action process
     */
    try {
      $controllerClass = $controllerName . 'Controller';
      if (!class_exists($controllerClass)) {
        throw new \Exception(str_replace('[[CLASS]]', $controllerName, $config->get('Exceptions', 'CLASS_NOT_FOUND')));
      }
      if (!method_exists($controllerClass, $action)) {
        throw new \Exception(str_replace(
          ['[[CLASS]]', '[[METHOD]]'],
          [$controllerClass, $action],
          $config->get('Exceptions', 'METHOD_NOT_FOUND')
        ));
      }

      /**
       * Check action permission
       */
      $permission = Permission::singleton()->checkPermission($controllerName, $action);
      if (!$permission['allowed']) {

        /**
         * We execute 'permission_unallowed' hook, for double-control on unauthorized requests.
         */
        $hooks->do_action(
          'permission_unallowed',
          [
            'permission' => $permission,
            'controller' => $controllerName,
            'action' => $action
          ]
        );
        /**
         * With this we ensure we don't redirect the user to a /api/<something>, for instance
         */
        if (!empty($route['after_login']) && $route['after_login'] == TRUE) {
          Session::setAfterLogin($_SERVER['REQUEST_URI']);
        }


        /**
         * Throw unallowed request
         */
        $throwTo = $permission['throw_to'];
        if (is_array($throwTo)) {
          $controllerClass = $throwTo[0];
          $action = $throwTo[1];
        } else {
          $location = __PATH__ . '/' . $config->get('Routing', $throwTo)['path'];
          header('HTTP/1.0 401 Unauthorized');
          die(json_encode([
            'success' => 0,
            'responseData' => [
              'message' => 'Please login to continue',
              'redirect' => $location
            ]
          ]));
        }
      }

      /**
       * Start ParentController()
       */
      \Controller::singleton();

      /**
       * Call requested action
       */
      $controller = $controllerClass::singleton();
      $controller->{$action}();
    } catch (\Exception $e) {
      $hooks->do_action('general_exception', ['e' => $e]);
      /**
       * Catch the exception if something's not OK
       */
      \Core\ExceptionController::singleton()->showException($e);
    }

    /* TOTAL time execution */
    clock_end();

    /**
     * We run the hook 'exec_afterend'.
     */
    $hooks->do_action('exec_afterend', ['controller' => $controllerName, 'action' => $action]);

    // echo clock_time() if you want
  }
}
