<?php
    /**
     * In this file we include the core logic for incoming requests
     */

    # Basic
    require_once __ROOT__ . '/app/languages/selector.php';
    require_once __ROOT__ . '/app/kernel/support_functions.php';
    require_once __ROOT__ . '/app/kernel/Config.php';
    require_once __ROOT__ . '/app/kernel/Permission.php';
    require_once __ROOT__ . '/app/kernel/Router.php';
    require_once __ROOT__ . '/app/models/Curl.php';

    # Session management
    require_once __ROOT__ . '/app/kernel/Session.php';

    # Core Logic
    require_once __ROOT__ . '/app/controllers/ParentController.php';
    require_once __ROOT__ . '/app/models/ParentModel.php';
    require_once __ROOT__ . '/app/kernel/Mailer.php';

    # Extras
    require_once __ROOT__ . '/app/libs/timeago/timeago.inc.php';
    require_once __ROOT__ . '/app/libs/Detection/Mobile_Detect.php';
    require_once __ROOT__ . '/app/libs/php-jwt/JWTExceptionWithPayloadInterface.php';
    require_once __ROOT__ . '/app/libs/php-jwt/ExpiredException.php';
    require_once __ROOT__ . '/app/libs/php-jwt/BeforeValidException.php';
    require_once __ROOT__ . '/app/libs/php-jwt/SignatureInvalidException.php';
    require_once __ROOT__ . '/app/libs/php-jwt/JWT.php';
    require_once __ROOT__ . '/app/libs/php-jwt/Key.php';
    require_once __ROOT__ . '/app/libs/php-jwt/JWK.php';
    require_once __ROOT__ . '/app/libs/php-jwt/CachedKeySet.php';
