<?php

    namespace Core;

    class Curl {
        public static function get($url, $fields = [], $decode = false) {
            $curl = curl_init($url . '?' . http_build_query($fields));
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
            ]);

            $result = curl_exec($curl);
            curl_close($curl);

            return $decode ? json_decode($result) : $result;
        }
    }