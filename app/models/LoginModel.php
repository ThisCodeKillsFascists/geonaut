<?php

namespace Core;

/**
 * Contains the default logic to retrieve a user from the database.
 * @package Core
 */
class LoginModel extends ParentModel {

    /**
     * @var LoginModel The class instance.
     * @internal
     */
    private static $instance = null;

    /**
     * Returns the class instance, creating it if it did not exist.
     * @return LoginModel
     */
    public static function singleton()
    {
        if (static::$instance === null) {
            $v = __CLASS__;
            static::$instance = new $v();
        }

        return static::$instance;
    }

    /**
     * Gets an user from the database and updates last_login and attempts
     * @param string $username The name of the user
     * @param string $password The password of the user
     * @param bool $hashed whether the password is hashed or not
     * @return mixed $user on success, FALSE on error
     */
    public function getUser($username, $password, $hashed = false) {
        $db = $this->config->get('Login');
        $loginTable = $db['TABLE'];
        $usernameRow = $db['TABLE_COLUMN_USER'];
        $passwordRow = $db['TABLE_COLUMN_PASS'];
        $query = "SELECT l.* FROM $loginTable l WHERE (l.`$usernameRow` = :username OR l.email = :username) AND
                  l.`$passwordRow` = " . ($hashed ? ':password' : 'SHA1(CONCAT(:password, l.salt))') . " LIMIT 1";
        $result = $this->query(
                $query, 
                array(
                    ':username' => $username, 
                    ':password' => $password
                ), TRUE);
        if ($result && is_array($result)) {
            $user = array_pop($result);

            //We update the Last_Login column
            //The reason we use 2 queries is because not all "user" tables may have both columns!
            $update = "UPDATE $loginTable SET last_login = :now WHERE id = :id";
            $this->query($update, array(':id' => $user['id'], ':now' => date('Y-m-d H:i:s')));

            // We update the Attempts column
            $update = "UPDATE $loginTable SET attempts = 0 WHERE id = :id";
            $this->query($update, array(':id' => $user['id']));

            return $user;
        } else {
            return FALSE;
        }
    }
}
