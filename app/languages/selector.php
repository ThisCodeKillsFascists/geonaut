<?php
    return;
    if (!isset($_SESSION['lang']) || isset($_GET['lang'])) {
        $lang = '';
        $acceptLang = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : FALSE;
        if (isset($_GET['lang'])) {
            $lang = $_GET['lang'];
        } elseif ($acceptLang) {
            $lang = substr($acceptLang, 0, 2);
        }

        //HACK:
        $lang = 'en';
        switch($lang) {
            case "es":
                break;
            case "ca":
                break;
            case "fr":
                break;
            case "de":
                break;
            case "en":
            default:
                $lang = "en";
                break;
        }
        $_SESSION['lang'] = $lang;
        $uri = explode('?', $_SERVER['REQUEST_URI']);
        
        if (isset($_GET['noredirect'])) {
            $uri[0] = substr($uri[0],0,strlen($uri[0])-3);
        }
        if ($acceptLang) {
            header ("Location: " . $uri[0]);
            exit();
        } else {
            include __ROOT__ . "/app/languages/" . $lang . ".php";
        }
    } else {
        include __ROOT__ . "/app/languages/" . $_SESSION['lang'] . ".php";
    }