<?php

namespace Core;

use Kernel\Permission;

/**
 * Class LoginController contains functions for user (de)authentication.
 *
 * @hooks
 * <code>
 * ('on_userauth', ['user' => $user]) // Called just after the login session has been created {@see \Core\LoginController::login}
 * ('on_userdeauth', ['user' => Session::getUser()]) // Called just before destroying the login session. {@see \Core\LoginController::deauthenticateUser}
 * </code>
 * @package Core
 */
class LoginController extends ParentController
{

    /**
     * @var LoginController The class instance.
     * @internal
     */
    private static $instance = null;

    /**
     * @var LoginModel The instance of the model
     */
    protected $model;

    /**
     * @var \Kernel\Permission The instance of the Permission class. Used for (de)authentication.
     */
    protected $permission;

    protected function __construct()
    {
        parent::__construct();
        $this->model = LoginModel::singleton();
        $this->permission = Permission::singleton();
        self::$instance = $this;
    }

    /**
     * Returns a LoginController instance, creating it if it did not exist.
     * @return LoginController
     */
    public static function singleton()
    {
        if (static::$instance === null) {
            $v = __CLASS__;
            static::$instance = new $v();
        }

        return static::$instance;
    }

    /**
     * Returns the instance of the model for this controller
     * @return LoginModel
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Performs an authentication request for a user with 'username' and 'password' post vars
     * If the credentials are not correct, LoginController->authenticateUser throws the user accordingly
     */
    public function login()
    {

        $user = $this->authenticateUser(strtolower($this->getPost('username')), $this->getPost('password'));

        Session::setUser($user);

        $this->hooks->do_action('on_userauth', ['user' => $user]);

        return $user;
    }

    /**
     * Logs a user out. That is, sets the current user with ROLE_USER privileges and redirects them to the Home path.
     */
    public function logout()
    {
        $this->deauthenticateUser();
        $this->redirect($this->url('Home'));
    }

    /**
     * Checks against the database or the config file (depending on configuration) if the credentials are correct.
     * @param string $username
     * @param string $password
     * @param bool $hashed whether the password is hashed or not
     * @return mixed $user on success. On fail redirects accordingly.
     */
    public function authenticateUser($username, $password, $hashed = false)
    {
        $config = $this->config->get('Login');
        $dbEnabled = (bool)(int)$this->config->get('Database', 'DB_SUPPORT');
        $user = [];
        if ($dbEnabled) {
            $user = $this->model->getUser($username, $password, $hashed);
        } elseif ($username == $config['LOGIN_USERNAME']) {
            $user = array(
                'username' => $username,
                'password' => $password,
                'role' => __ROLE_ADMIN__
            ); /* only one user? must be an admin! */
        } else {
            // Regular user, or invalid credentials...
        }

        if (!$user) {
            /**
             * If user didn't authenticate properly, we redirect to Login
             */
            $this->json(['success' => 0, 'message' => 'wrong credentials', 'field' => 'password', 'received' => "XX"]);
        }

        setcookie('llut', sha1($user['id'] . $user['member_since']),   time() + 3600 * 24 * 7, '/');

        //////////

        $this->permission->setUserPermission($user['role']);

        $user["id"] = (int)$user["id"];
        $user["theme"] = (int)$user["theme"];
        $user["gender"] = (int)$user["gender"];
        $user["role"] = (int)$user["role"];

        return $user;
    }

    /**
     * Deauthenticates the current user by setting ROLE_USER privileges.
     * @return LoginController;
     */
    public function deauthenticateUser()
    {
        $this->hooks->do_action('on_userdeauth', ['user' => Session::getUser()]);

        Session::cleanUser();
        $this->permission->setUserPermission(__ROLE_GUEST__);

        //empty last login cookie?
        //setcookie('llut', '', time() - 1000, '/');

        return $this;
    }
}
