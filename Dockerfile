# Dockerfile
FROM php:7.4-apache

# Install necessary PHP extensions
RUN docker-php-ext-install mysqli pdo pdo_mysql

# Install dependencies and PHP extensions
RUN apt-get update && apt-get install -y \
    libexif-dev \
    libjpeg-dev \
    libpng-dev \
    libfreetype6-dev \
    libwebp-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-install exif gd

# Enable Apache modules
RUN a2enmod rewrite headers

# Set the working directory
WORKDIR /var/www/html

# Copy the entire project
COPY . /var/www/html/

# Set the document root to the public directory
RUN sed -i -e 's|/var/www/html|/var/www/html/public|g' /etc/apache2/sites-available/000-default.conf

# Expose port 80
EXPOSE 80
