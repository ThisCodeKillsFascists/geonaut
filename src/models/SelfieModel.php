<?php

    class SelfieModel extends Model
    {

        /**
         * @var SelfieModel The class instance.
         * @internal
         */
        private static $instance = null;

        /**
         * Returns a SelfieModel instance, creating it if it did not exist.
         * @return SelfieModel
         */
        public static function singleton()
        {
            if (static::$instance === null) {
                $v = __CLASS__;
                static::$instance = new $v();
            }

            return static::$instance;
        }

        public function createSelfieDraft($userID)
        {
            $query = 'INSERT INTO selfies (user_id, title, short_desc, long_desc,
                      selfie_date, selfie_place, lat, lng, selfietype_id, visible, added_on, edited_on,
                      me_color, me_brightness, lc_color, lc_brightness, meta_me, meta_lc, draft)
                      VALUES (:userID, "", "", "", NULL, "", 0, 0, 2, 0, :now, :now, "", 0, "", 0, "", "", 1)';
            $this->query($query, [
                ':userID' => $userID,
                ':now' => date('Y-m-d H:i:s')
            ]);

            $selfieID = $this->db->lastInsertId();

            return $this->getRow('selfies', $selfieID);
        }

        public function saveSelfie($userID, $title, $shortDesc, $longDesc, $date,
                                   $place, $lat, $lng, $selfieTypeID, $visible, $colorMe, $brightnessMe,
                                   $colorLc, $brightnessLc, $meta_me, $meta_lc, $draft = 0)
        {
            $query = 'INSERT INTO selfies (user_id, title, short_desc, long_desc,
                      selfie_date, selfie_place, lat, lng, selfietype_id, visible, added_on, edited_on,
                      me_color, me_brightness, lc_color, lc_brightness, meta_me, meta_lc, draft)
                      VALUES (:userID, :title, :shortDesc, :longDesc,
                      :date, :place, :lat, :lng, :selfieTypeID, :visible, :now, :now,
                      :colorMe, :brightnessMe, :colorLc, :brightnessLc, :metadataMe, :metadataLc, :draft)';
            $this->query($query, [
                ':userID' => $userID,
                ':title' => $title,
                ':shortDesc' => $shortDesc,
                ':longDesc' => $longDesc,
                ':date' => $date,
                ':place' => $place,
                ':lat' => $lat,
                ':lng' => $lng,
                ':selfieTypeID' => $selfieTypeID,
                ':visible' => $visible,
                ':now' => date('Y-m-d H:i:s'),
                ':colorMe' => $colorMe,
                ':brightnessMe' => $brightnessMe,
                ':colorLc' => $colorLc,
                ':brightnessLc' => $brightnessLc,
                ':metadataMe' => $meta_me,
                ':metadataLc' => $meta_lc,
                ':draft' => $draft
            ]);
            $id = $this->db->lastInsertId();

            return $id;
        }

        public function editSelfieColorData($selfieID, $type, $color, $brightness)
        {
            $prefix = $type . '_';
            $query = "UPDATE selfies SET {$prefix}color = :color, {$prefix}brightness = :brightness
                      WHERE id = :selfieID LIMIT 1";

            $res = $this->query($query, [
                ':selfieID' => $selfieID,
                ':color' => $color,
                ':brightness' => $brightness,
            ]);

            return $res->rowCount();
        }

        public function editSelfie($selfieID, $title, $shortDesc, $longDesc, $date,
                                   $place, $lat, $lng, $selfieTypeID, $visible, $draft = 0)
        {
            $query = 'UPDATE selfies SET title = :title, short_desc = :shortDesc, long_desc = :longDesc,
                      selfie_date = :date, selfie_place = :place, lat = :lat, lng = :lng, selfietype_id = :selfieTypeID,
                      visible = :visible, edited_on = :edited_on, draft = :draft WHERE id = :selfieID LIMIT 1';

            $res = $this->query($query, [
                ':selfieID' => $selfieID,
                ':title' => $title,
                ':shortDesc' => $shortDesc,
                ':longDesc' => $longDesc,
                ':date' => $date,
                ':place' => $place,
                ':lat' => $lat,
                ':lng' => $lng,
                ':selfieTypeID' => $selfieTypeID,
                ':edited_on' => date('Y-m-d H:i:s'),
                ':visible' => $visible,
                ':draft' => $draft,
            ]);
            return $res->rowCount();
        }

        public function deleteSelfie($selfieID)
        {
            $res  = $this->query('DELETE FROM selfies    WHERE id = :selfieID LIMIT 1', [':selfieID' => $selfieID]);
            $res2 = $this->query('DELETE FROM selfie_url WHERE selfie_id = :selfieID' , [':selfieID' => $selfieID]);
            return !(bool)(int)$res->errorCode();
        }

        public function deleteLikes($selfieID)
        {
            $res = $this->query('DELETE FROM love WHERE selfie_id = :selfieID', [':selfieID' => $selfieID]);
            return !(bool)(int)$res->errorCode();
        }

        public function addSelfieURL($selfieID, $hash, $original = 0)
        {
            $queryUpdate = 'UPDATE selfie_url SET active = 0 WHERE selfie_id = :selfieID';
            $queryInsert = 'INSERT INTO selfie_url (selfie_id, hash, original) VALUES (:selfieID, :hash, :original)';

            $this->query($queryUpdate, [':selfieID' => $selfieID]);
            $this->query($queryInsert, [':selfieID' => $selfieID, ':hash' => $hash, ':original' => $original]);

            return true;
        }
    }
