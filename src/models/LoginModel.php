<?php

class LoginModel extends \Core\LoginModel
{

    /**
     * @var LoginModel The class instance.
     * @internal
     */
    private static $instance = null;

    /**
     * Returns a LoginModel instance, creating it if it did not exist.
     * @return LoginModel
     */
    public static function singleton()
    {
        if (static::$instance === null) {
            $v = __CLASS__;
            static::$instance = new $v();
        }

        return static::$instance;
    }

    /**
     * @param $role
     * @param $name
     * @param $email
     * @param $username
     * @param $password
     * @param $salt
     * @param $avatar
     *
     * @return mixed
     */
    public function signup($role, $name, $email, $username, $password, $salt, $profilePicDefault) {
        $query = 'INSERT INTO users (role, `name`, email, username, password, salt, profile_pic_default, member_since)
                  VALUES (:role, :name, :email, :username, :password, :salt, :profilePicDefault, CURRENT_TIMESTAMP )';

        $result = $this->query($query, [
            ':role' => $role,
            ':name' => $name,
            ':email' => $email,
            ':username' => $username,
            ':password' => $password,
            ':salt' => $salt,
            ':profilePicDefault' => $profilePicDefault,
        ]);

        return $this->db->lastInsertId();
    }
}
