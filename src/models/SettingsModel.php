<?php

    class SettingsModel extends Model
    {

        /**
         * @var SettingsModel The class instance.
         * @internal
         */
        private static $instance = null;

        /**
         * Returns a SettingsModel instance, creating it if it did not exist.
         * @return SettingsModel
         */
        public static function singleton()
        {
            if (static::$instance === null) {
                $v = __CLASS__;
                static::$instance = new $v();
            }

            return static::$instance;
        }

        public function updatePassword($userID, $password) {
            $query = 'UPDATE users SET password = :password WHERE id = :userID LIMIT 1';
            $res = $this->query($query, [':userID' => $userID, ':password' => $password]);
            return !(bool)(int)$res->errorCode();
        }

        public function updateUserDetails($userID, $name, $email) {
            $query = 'UPDATE users SET name = :name, email = :email WHERE id = :userID LIMIT 1';
            $res = $this->query($query, [':userID' => $userID, ':name' => $name, ':email' => $email]);
            return !(bool)(int)$res->errorCode();
        }

    }
