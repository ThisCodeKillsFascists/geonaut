<?php

    /**
     * Class Model acts as a stepping stone (or "intermediate agent") between the user-defined code (<b>/src/models/</b>)
     * and the system-defined code (<b>ParentModel</b>) that helps to better structure the code. See &#64;example
     * @example
     * ```
     *   // To avoid:
     *   FooController->queryProducts();
     *   // and
     *   BarController->queryProducts();
     *   // to be defined twice (one in each controller) or once (in ParentController, bad code practices)
     *   Controller->queryProducts();
     *   // can be defined and thus accessed from both <b>Foo</b> and <b>Bar</b> Controllers.
     * ```
     */
    class Model extends \Core\ParentModel
    {

        /**
         * @var Model The class instance.
         * @internal
         */
        private static $instance = null;

        /**
         * Returns a Model instance, creating it if it did not exist.
         * @return Model
         */
        public static function singleton()
        {
            if (static::$instance === null) {
                $v = __CLASS__;
                static::$instance = new $v();
            }

            return static::$instance;
        }

        /**
         * Retrieves all the selfie (up to $limit) for user $userID (or everyone if $userID = 0). <br/>
         * Set the $selfID parameter to include "love" and "edit" buttons.
         * @param int $selfID The ID of the user, or 0 if is not authenticated
         * @param int $userID The ID of the selfie-owner, or 0 to retrieve from all users
         * @param int $start = 0 The index of the first selfie. LIMIT $start, $limit
         * @param int $limit = 10 The max number of selfies. LIMIT $start, $limit
         *
         * @return array The result
         */
        public function getAllSelfies($selfID, $userID = 0, $start = 0, $limit = 10) {
            if ($userID) {
                $isOwner = ($selfID == 0 ? 0 : ($selfID == $userID ? 'yes' : 'no'));
                $query = "SELECT
                              s.id, s.user_id, s.active_hash, s.hash, s.title, s.short_desc, s.selfie_date,
                              s.selfie_place, s.lat, s.lng, s.selfietype_id, s.added_on, s.edited_on,
                              s.lc_color, s.me_color, s.lc_brightness, s.me_brightness, s.username, s.name, s.possessive,
                              IF(l.user_id = :selfID, 1, 0) AS love,
                              -- IF(f.follower = :selfID, 1, 0) AS following,
                              :isOwner as is_owner,
                              'profile' AS 'type',
                      COALESCE(lc.loves, 0) as loves
                      FROM selfies_with_user s
                      LEFT JOIN love l ON (s.id = l.selfie_id AND :selfID = l.user_id)
                      -- LEFT JOIN follow f ON s.user_id = f.followed
                      LEFT JOIN love_count lc ON lc.selfie_id = s.id
                      WHERE (s.user_id = :userID)
                      GROUP BY s.id, s.user_id, s.active_hash, s.hash, s.title, s.short_desc, s.selfie_date,
                               s.selfie_place, s.lat, s.lng, s.selfietype_id, s.added_on, s.edited_on,
                               s.lc_color, s.lc_brightness, s.me_color, s.me_brightness, s.username, s.name, s.possessive,
                               is_owner, 'type', love, loves
                      ORDER BY s.selfie_date DESC, s.added_on DESC LIMIT $start, $limit";
                $selfies = $this->query($query, [
                    ':userID' => $userID,
                    ':selfID' => $selfID,
                    ':isOwner' => $isOwner],
                    TRUE);

            } else {
                $query = "SELECT
                            s.id, s.user_id, s.active_hash, s.hash, s.title, s.short_desc, s.selfie_date,
                            s.selfie_place, s.lat, s.lng, s.selfietype_id, s.added_on, s.edited_on,
                            s.lc_color, s.me_color, s.lc_brightness, s.me_brightness, s.username, s.name, s.possessive,
                            IF(l.user_id = :selfID, 1, 0) AS love,
                            -- IF(f.follower = :selfID, 1, 0) AS following,
                            IF(:selfID = 0, 0, IF(:selfID = s.user_id, 'yes', 'no')) as is_owner,
                            'selfie' AS 'type',
                            COALESCE(lc.loves, 0) as loves
                      FROM selfies_with_user s
                      LEFT JOIN love l ON (s.id = l.selfie_id AND :selfID = l.user_id)
                      -- LEFT JOIN follow f ON s.user_id = f.followed
                      LEFT JOIN love_count lc ON lc.selfie_id = s.id
                      GROUP BY s.id, s.user_id, s.active_hash, s.hash, s.title, s.short_desc, s.selfie_date,
                               s.selfie_place, s.lat, s.lng, s.selfietype_id, s.added_on, s.edited_on,
                               s.lc_color, s.lc_brightness, s.me_color, s.me_brightness,
                               s.username, s.name, s.possessive,
                               is_owner, 'type', love, loves
                      ORDER BY s.added_on DESC LIMIT $start, $limit ";
                $selfies = $this->query($query, [':selfID' => $selfID], TRUE);

            }

            return $selfies;
        }

        public function getLastSelfieUsers($selfID = 0, $start = 0, $limit = 10) {
            $query = "SELECT
                  s.*,
                  IF(l.user_id = :selfID, 1, 0) AS love,
                  IF(:selfID = 0, 0,
                  IF(:selfID = s.user_id, 'yes', 'no')) as is_owner,
                  'user' AS 'type',
                  COALESCE(lc.loves, 0) as loves,
                  REPLACE(u.short_desc, '&#10;', '') AS profile,
                  FROM selfies_with_user s
                  INNER JOIN selfies_added_on sao ON (s.added_on = sao.added_on AND s.user_id = sao.user_id)
                  LEFT JOIN love l ON (s.id = l.selfie_id AND :selfID = l.user_id)
                  LEFT JOIN love_count lc ON lc.selfie_id = s.id
                  LEFT JOIN users u ON u.id = s.user_id
                  GROUP BY s.id, s.user_id, s.active_hash, s.hash, s.title, s.short_desc, s.selfie_date,
                           s.selfie_place, s.lat, s.lng, s.selfietype_id, s.added_on, s.edited_on,
                           s.lc_color, s.me_color, s.me_brightness, s.lc_brightness, s.username, s.name, s.possessive,
                           is_owner, 'type', profile, love, loves
                  ORDER BY s.added_on DESC
                  LIMIT $start, $limit";
            $firstSelfies = $this->query($query, [
                ':selfID' => $selfID,
            ], TRUE);

            return array_values($firstSelfies);
        }

        /**
         * Gets a single selfie from the database
         * @param $hash string the Selfie hash
         * @param bool|false $selfID the ID of the authenticated user (0 for guest)
         * @param bool|false $longDesc Whether to fetch the long description too or not (will require two queries)
         *
         * @return bool|mixed|PDO
         */
        public function getSelfie($hash, $selfID = false, $longDesc = false) {

            if ($selfID) {
                $query = "SELECT
                      s.*,
                      IF(l.user_id = $selfID, 1, 0) AS love,
                      IF(f.follower = $selfID, 1, 0) AS following,
                      IF(s.user_id = $selfID, 'yes', 'no') AS is_owner,
                      'selfie' AS 'type',
                      COALESCE(lc.loves, 0) as loves

                      FROM selfies_with_user s
                      LEFT JOIN love l ON (s.id = l.selfie_id AND $selfID = l.user_id)
                      LEFT JOIN follow f ON s.user_id = f.followed
                      LEFT JOIN love_count lc ON lc.selfie_id = s.id
                      WHERE (s.hash = :hash )
                      LIMIT 1";
            } else {
                $query = "SELECT
                      s.*,
                      0 AS love, 0 AS following, 0 as is_owner,
                      'selfie' AS 'type',
                      COALESCE(lc.loves, 0) as loves
                      FROM selfies_with_user s
                      LEFT JOIN users u ON s.user_id = u.id
                      LEFT JOIN love_count lc ON lc.selfie_id = s.id
                      WHERE (s.hash = :hash)
                      LIMIT 1";
            }
            $selfie = $this->query($query, [':hash' => $hash], TRUE);
            if (!count($selfie)) {
                return FALSE;
            }

            $selfie = array_pop($selfie);

            // We only give the long description if it's explicitly asked for
            if ($longDesc) {
                $longDesc = $this->getRow('selfies', $selfie['id'], 'id');
                $selfie['long_desc'] = html_entity_decode($longDesc['long_desc']);
            }

            return $selfie;
        }

        public function getSelfieByID($selfieID) {
            $query = 'SELECT s.* FROM selfies s WHERE s.id = :selfieID LIMIT 1';
            $res = $this->query($query, [':selfieID' => $selfieID], TRUE);
            return $res ? $res[0] : [];
        }

        public function getSelfieByHash($anyHash)
        {
            $query = 'SELECT s.* FROM selfies s LEFT JOIN selfie_url su ON su.selfie_id = s.id WHERE su.hash = :hash LIMIT 1';
            $res = $this->query($query, [':hash' => $anyHash], TRUE);

            if (empty($res)) {
                return [];
            }

            $res[0]['hash'] = $anyHash;
            return $res[0];
        }

        /**
         * Gets the selfies that correspond to previous and next in relation to the given $selfieID.
         *
         * @param int $selfID The ID of the current user, if any. Used to show "Love" or "Edit" buttons
         *
         * @return array [ prev =&gt; {}, next =&lt; {} ]
         */
        public function getSelfieNext2($selfieCurrent, $selfID) {
            $query = '(
                    SELECT \'next\' as selfie_type, su.hash
                    FROM selfies s
                    INNER JOIN selfie_url su ON su.selfie_id = s.id
                    WHERE s.user_id = :userID
                    AND su.original = 1
                    -- AND su.active = 1
                    AND (s.selfie_date > :sCurrDate OR (s.selfie_date = :sCurrDate && s.added_on > :sAddedOn))
                    ORDER BY s.selfie_date ASC, s.added_on ASC
                    LIMIT 1
                  )

                  UNION

                  (
                    SELECT \'prev\' as selfie_type, su.hash
                    FROM selfies s
                    INNER JOIN selfie_url su ON su.selfie_id = s.id
                    WHERE s.user_id = :userID
                    AND su.original = 1
                    -- AND su.active = 1
                    AND (s.selfie_date < :sCurrDate OR (s.selfie_date = :sCurrDate && s.added_on < :sAddedOn))
                    ORDER BY s.selfie_date DESC, s.added_on DESC
                    LIMIT 1
                  )
                  ';
            $selfies = $this->query($query, [
                ':userID'    => $selfieCurrent['user_id'],
                ':sCurrDate' => $selfieCurrent['selfie_date'],
                ':sAddedOn'  => $selfieCurrent['added_on']
            ], TRUE);

            $prev = null;
            $next = null;
            if (count($selfies)) {
              if (count($selfies) === 1) {
                if ($selfies[0]['selfie_type'] === "next") {
                  $next = $selfies[0];
                } else {
                  $prev = $selfies[0];
                }
              } else {
                $next = $selfies[0];
                $prev = $selfies[1];
              }
            } else {
              // both null
            }

            $neighbours = [];

            if ($prev) {
                $neighbours['prev'] = $this->getSelfie($prev['hash'], $selfID, true);
            }
            if ($next) {
                $neighbours['next'] = $this->getSelfie($next['hash'], $selfID, true);
            }
            return $neighbours;
        }

        /**
         * Retrieve the username (from the userID) or the userID (from the username) for a given userID/username
         *
         * @param bool|false $userID The ID of the user
         * @param bool|false $username The username of the user
         *
         * @return string The username or the userID, accordingly
         */
        public function convertUserKey($userID = false, $username = false) {
            if ($userID !== false) {
                $res = $this->query('SELECT u.username FROM users u WHERE u.id = :userID LIMIT 1', [':userID' => $userID], true);
                $value = count($res) ? $res[0]['username'] : false;
            } else {
                $res = $this->query('SELECT u.id FROM users u WHERE LOWER(u.username) = :username LIMIT 1', [':username' => strtolower($username)], true);
                $value = count($res) ? $res[0]['id'] : false;
            }
            return $value;
        }

        /**
         * Gets the information for a user: PROFILE + followers + following + count(images)
         * @param int $userID the ID of the user to retrieve
         * @param int $selfID just to know if the $userID is the same as the $selfID who's requesting the data
         *
         * @return array the information for the user $userID
         */
        public function getUserInfo($userID, $selfID) {
            $query = 'SELECT SUM(followers) AS followers, SUM(following) AS following, SUM(pictures) AS pictures FROM (
                      SELECT COUNT(f.follower) AS followers, 0 AS following, 0 AS pictures FROM follow f WHERE f.followed = :userID
                      UNION
                      SELECT 0 AS followers, COUNT(f.followed) AS following, 0 AS pictures FROM follow f WHERE f.follower = :userID
                      UNION
                      SELECT 0 AS followers, 0 AS following, COUNT(s.id) AS pictures FROM selfies s WHERE s.user_id = :userID
                  ) a';
            $userInfo = $this->query($query, [':userID' => $userID], true);
            $userInfoFollowers = $userInfo[0];

            $query2 = 'SELECT
                      u.name,
                      u.username,
                      u.short_desc AS profile,
                      u.hobbies,
                      u.gender,
                      IF(u.gender = "f", "her", "his") AS possessive,
                      u.birthday,
                      IF(:userID = :selfID, 1, 0) AS isself,
                      u.profile_pic,
                      u.profile_pic_default
                   FROM users u WHERE u.id = :userID
                   LIMIT 1';
            $userInfo2 = $this->query($query2, [':userID' => $userID, ':selfID' => $selfID], TRUE);
            $userInfoProfile = $userInfo2[0];

            return array_merge($userInfoFollowers, $userInfoProfile);
        }

        public function getUserAndLastSelfie($username, $selfID = 0) {
            if ($selfID) {
                $query = "SELECT
                      s.*,
                      IF(l.user_id = $selfID, 1, 0) AS love,
                      IF(f.follower = $selfID, 1, 0) AS following,
                      IF(s.user_id = $selfID, 'yes', 'no') AS is_owner,
                      'user' AS 'type',
                      COALESCE(lc.loves, 0) as loves

                      FROM selfies_with_user s
                      INNER JOIN selfies_added_on sao
                          ON (sao.user_id = s.user_id) AND (sao.added_on = s.added_on)
                      LEFT JOIN love l ON (s.id = l.selfie_id AND $selfID = l.user_id)
                      LEFT JOIN follow f ON s.user_id = f.followed
                      LEFT JOIN love_count lc ON lc.selfie_id = s.id
                      WHERE (s.username = :username)
                      LIMIT 1";
            } else {
                $query = "SELECT
                      s.*,
                      0 AS love, 0 AS following, 0 as is_owner,
                      'user' AS 'type',
                      COALESCE(lc.loves, 0) as loves
                      FROM selfies_with_user s
                      INNER JOIN selfies_added_on sao
                          ON (sao.user_id = s.user_id) AND (sao.added_on = s.added_on)
                      LEFT JOIN users u ON s.user_id = u.id
                      LEFT JOIN love_count lc ON lc.selfie_id = s.id
                      WHERE (s.username = :username)
                      LIMIT 1";
            }
            $userAndSelfie = $this->query($query, [':username' => $username], TRUE);

            if (!count($userAndSelfie)) {
                return FALSE;
            }

            $selfie = array_pop($userAndSelfie);

            return $selfie;
        }

        /**
         * Gets a list of the selfies that $selfID (the user) is following
         * @param $selfID int the ID of the user
         * @param int $start LIMIT
         * @param int $limit LIMIT
         *
         * @return array
         */
        public function getFollowedSelfies($selfID, $start = 0, $limit = 20) {
            $queryFollowing = "
            SELECT
                'selfie' AS 'type',
                COALESCE (lc.loves, 0) AS loves,
                IF(l.user_id = :selfID, 1, 0) AS love,
                IF(s.user_id = :selfID, 'yes', 'no') AS is_owner,
                -- IF(f.follower = :selfID, 1, 0) AS following,
                s.*
            FROM
                follow f
            RIGHT JOIN selfies_with_user s ON s.user_id = f.followed
            LEFT JOIN love_count lc ON lc.selfie_id = s.id
            LEFT JOIN love l ON s.id = l.selfie_id AND l.user_id = :selfID

            WHERE
                f.follower = :selfID
	        ORDER BY s.added_on DESC
            LIMIT $start, $limit";
            $selfies = $this->query(
                $queryFollowing,
                [
                    ':selfID' => $selfID,
//                    ':start' => $start,
//                    ':limit' => $limit
                ], true);

            return $selfies;
        }

        /**
         * Gets a list of the selfies that $selfID (the user) has loved
         * @param $selfID int the ID of the user
         * @param int $start LIMIT
         * @param int $limit LIMIT
         *
         * @return array
         */
        public function getLovedSelfies($selfID, $start = 0, $limit = 20) {
            $queryLoved = "SELECT
                        s.*,
                        IF(l.user_id = :selfID, 1, 0) AS love,
                        IF(s.user_id = :selfID, 'yes', 'no') AS is_owner,
                        'selfie' AS 'type',
                        1 as loves
                      FROM selfies_with_user s
                      LEFT JOIN love l ON s.id = l.selfie_id
                      LEFT JOIN love_count lc ON lc.selfie_id = s.id
                      WHERE (l.user_id = :selfID)
                      GROUP BY s.id, l.love_on, s.user_id, s.active_hash, s.hash, s.title, s.short_desc, s.selfie_date,
                               s.selfie_place, s.lat, s.lng, s.selfietype_id, s.added_on, s.edited_on,
                               s.lc_color, s.lc_brightness, s.me_color, s.me_brightness, s.username, s.name, s.possessive,
                               is_owner, 'type', love, loves
                      ORDER BY l.love_on DESC
                      LIMIT $start, $limit";
            $selfies = $this->query($queryLoved, [':selfID' => $selfID], TRUE);

            return $selfies;
        }

        /**
         * Checks whether the user $follower is following $followed, and returns a bool.
         *
         * @param $follower
         * @param $followed
         *
         * @return bool Whether $follower is following $followed
         */
        public function follows($follower, $followed) {
            $query = 'SELECT f.* FROM follow f WHERE f.follower = :follower AND f.followed = :followed LIMIT 1';
            $result = $this->query($query, [':follower' => $follower, ':followed' => $followed], TRUE);

            // Returns true if $follower is following $following
            return (bool)(count($result));
        }

        /**
         * Checks whether the user $userID loves $selfieID, and returns a bool.
         *
         * @param $userID
         * @param $selfieID
         *
         * @return bool Whether $userID loves $selfieID
         */
        public function loves($userID, $selfieID) {
            $query = 'SELECT l.* FROM love l WHERE l.user_id = :userID AND l.selfie_id = :selfieID LIMIT 1';
            $result = $this->query($query, [':userID' => $userID, ':selfieID' => $selfieID], TRUE);

            // Returns true if $follower is following $following
            return (bool)(count($result));
        }

        /**
         * Returns the user info AND the last selfie that the user has added (i.e. sort by "added_on" and NOT "taken_on")
         *
         * @param int $userID
         *
         * @return array
         */
        public function followingUsers($userID) {
            $query = "
                SELECT
                COALESCE (lc.loves, 0) AS loves,
                IF(l.user_id = :selfID, 1, 0) AS love,
                'no' as is_owner,
                'selfie' AS type,
                -- IF(f.follower = :selfID, 1, 0) AS following,
                s.*
            FROM
                follow f
            RIGHT JOIN selfies_with_user s ON s.user_id = f.followed
            INNER JOIN selfies_added_on sao ON (s.added_on = sao.added_on AND s.user_id = sao.user_id)
            LEFT JOIN love_count lc ON lc.selfie_id = s.id
            LEFT JOIN love l ON s.id = l.selfie_id AND l.user_id = :selfID

            WHERE
                f.follower = :selfID
	        ORDER BY s.added_on DESC";


            $selfies = $this->query($query, [':selfID' => $userID], TRUE);

            return $selfies;
        }

        /**
         * Returns a list of the IDs of the users that follow the $userID
         * @param $userID
         * @return array $users
         */
        public function getFollowersForUser($userID) {
            $query = 'SELECT f.follower AS follower_id FROM follow f WHERE f.followed = :userID';
            return $this->query($query, [':userID' => $userID], TRUE);
        }

        /**
         * This function searches ONE item for each type of possible search mode: user, selfie, location and date.
         * If there's only ONE result, the controller will redirect;
         * if there's more than one (i.e. more than one type of result) then
         * we redirect to the search page where the system searches everything again, separately.
         *
         * @param $searchQuery
         *
         * @return PDO
         */
        public function searchAll($searchQuery) {
            $result = array_merge(
                $this->search('selfie',   $searchQuery, 0, 2),
                $this->search('user',     $searchQuery, 0, 2),
                $this->search('location', $searchQuery, 0, 2),
                $this->search('date',     $searchQuery, 0, 2)
            );

            return $result;
        }


        /**
         * Searches for selfies looking in the appropriate fields
         *
         * @param $type
         * @param $searchQuery
         * @param $start
         * @param $limit
         *
         * @return array|PDO
         */
        public function search($type, $searchQuery, $start, $limit) {

            $searchQuery = strtolower($searchQuery);
            // $searchQuery = preg_replace("/[^A-Za-z0-9 -]/", '%', $searchQuery);

            switch ($type) {
                case 'selfie':
                    $query = '
                        SELECT distinct sout.id as ID, "selfie" as `type`, 0 as username, sout.hash, sout.active_hash, sout.added_on FROM (
                        SELECT s.id, s.active_hash, s.hash, s.added_on FROM selfies_with_user s WHERE (s.hash = :q OR s.active_hash = :q)
                        UNION
                        SELECT s.id, s.active_hash, s.hash, s.added_on FROM selfies_with_user s WHERE LOWER(s.title) LIKE :qAprox or SOUNDEX(s.title) = SOUNDEX(:q)
                        UNION
                        SELECT s.id, s.active_hash, s.hash, s.added_on FROM selfies_with_user s WHERE LOWER(s.short_desc) LIKE :qAprox OR SOUNDEX(s.short_desc) = SOUNDEX(:q)
                        UNION
                        (
                          SELECT s.id, s.active_hash, s.hash, s.added_on
                          FROM selfies_with_user s
                          INNER JOIN selfies ON s.id = selfies.id
                          WHERE LOWER(selfies.long_desc) LIKE :qAprox
                         )
                         ORDER BY added_on desc
                        ) sout LIMIT ' . $start . ', ' . $limit;

                    $result = $this->query($query, [
                        ':qAprox' => '%' . $searchQuery . '%',
                        ':q' => $searchQuery,
                    ], TRUE);
                    break;

                case 'user':
                    $query = '
                        SELECT "user" as `type`, u.username, 0 as hash, 0 as active_hash, s.added_on
                        FROM users u
                        left join selfies_added_on s
                        on u.id = s.user_id
                        WHERE (u.username LIKE :userQuery)
                        OR (u.name LIKE :userQuery)
                        GROUP BY u.username, s.added_on, hash, active_hash
                        LIMIT ' . $start . ', ' . $limit;
                    $result = $this->query($query, [
                        ':userQuery' => $searchQuery . '%'
                    ], TRUE);
                    break;

                case 'location':
                    $query = '
                        SELECT "location" as `type`, 0 as username, s.hash, s.active_hash, s.added_on FROM
                        selfies_with_user s WHERE s.selfie_place LIKE :qAprox
                        LIMIT ' . $start . ', ' . $limit;
                    $result = $this->query($query, [
                        ':qAprox' => '%' . $searchQuery . '%',
                    ], TRUE);
                    break;

                case 'date':
                    preg_match('/([Jj]an(uary)?|[Ff]eb(ruary)?|[Mm]ar(ch)?|[Aa]pr(il)?|[Mm]ay|[Jj]un(e)?|[Jj]ul(y)?|' .
                               '[Aa]ug(ust)?|[Ss]ep(tember)?|[Oo]ct(ober)?|[Nn]ov(ember)?|[Dd]ec(ember)?)[\ ]?([0-9]{2,4})?/',
                        $searchQuery,
                        $valid);

                    if (empty($valid[0]) && date('Y-m', strtotime($searchQuery)) === '1970-01') {
                        // invalid date
                        return [];
                    }

                    $month  = date('m', strtotime($searchQuery));
                    $year   = date('Y', strtotime($searchQuery));

                    $query = '
                        SELECT "date" as `type`, 0 as username, s.hash, s.active_hash, s.added_on
                        FROM selfies_with_user s
                        WHERE MONTH(s.selfie_date) = :month AND YEAR(s.selfie_date) = :year
                        LIMIT ' . $start . ', ' . $limit;
                    $result = $this->query($query, [
                        ':month' => $month,
                        ':year' => $year
                    ], TRUE);

                    break;

                default:
                    $result = [];
            }

            return $result;

        }

        public function getSelfieDraft($userID)
        {
            $query = 'SELECT s.*, su.hash FROM selfies s LEFT JOIN selfie_url su ON su.selfie_id = s.id
                      WHERE s.draft = 1 AND s.visible = 0 AND s.user_id = :userID AND su.original = 1
                      ORDER BY s.added_on DESC LIMIT 1';

            $res = $this->query($query, [':userID' => $userID], TRUE);

            return $res ? $res[0] : null;
        }

        public function getActiveHash($possibleHash) {
            $activeHashQuery = $this->query(
                'SELECT su2.hash FROM selfie_url su
             INNER JOIN selfie_url su2 ON su2.selfie_id = su.selfie_id
             WHERE su.hash = :hash AND su2.active = 1
             LIMIT 1',
                [':hash' => $possibleHash],
                TRUE);

            if (empty($activeHashQuery)) {
                return false;
            } else {
                $activeHash = $activeHashQuery[0]['hash'];

                return $activeHash;
            }
        }

        public function getOriginalHash($hash) {
            $originalHashQuery = $this->query(
                'SELECT su2.hash FROM selfie_url su
             INNER JOIN selfie_url su2 ON su2.selfie_id = su.selfie_id
             WHERE su.hash = :hash AND su2.original = 1
             LIMIT 1',
                [':hash' => $hash],
                TRUE);

            return !empty($originalHashQuery) ? $originalHashQuery[0]['hash'] : false;
        }

        public function getSelfieID($hash) {
            $query = 'SELECT su.selfie_id FROM selfie_url su WHERE su.hash = :hash LIMIT 1';
            $result = $this->query($query, [':hash' => $hash], TRUE);

            if (empty($result)) { // we might be looking for an unexisting hash!
                return 0;
            } else {
                return $result[0]['selfie_id'];
            }
        }

        public function getSelfieHash($selfieID, $original = 1, $active = 0) {
            $query = 'SELECT su.hash as `original`, su2.hash AS `active`
                      FROM selfie_url su LEFT JOIN selfie_url su2 ON su.selfie_id = su2.selfie_id
                      WHERE su.selfie_id = :selfieID AND su.original = 1 AND su2.active = 1 LIMIT 1';
            $result = $this->query($query, [':selfieID' => $selfieID], TRUE);

            if (empty($result)) { // we might be looking for an unexisting hash!
                return 0;
            } elseif ($original === 1 && $active === 0) {
                return $result[0]['original'];
            } elseif ($original === 0 && $active === 1) {
                return $result[0]['active'];
            } else {
                return $result[0];
            }
        }

        public function getSelfieActiveHash($selfieID) {
            $query = 'SELECT su.hash FROM selfie_url su WHERE su.selfie_id = :selfieID AND su.active = 1 LIMIT 1';
            $result = $this->query($query, [':selfieID' => $selfieID], TRUE);

            if (empty($result)) { // we might be looking for an unexisting hash!
                return 0;
            } else {
                return $result[0]['hash'];
            }
        }
    }
