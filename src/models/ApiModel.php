<?php

    class ApiModel extends Model
    {

        /**
         * @var ApiModel The class instance.
         * @internal
         */
        private static $instance = null;

        /**
         * Returns a ApiModel instance, creating it if it did not exist.
         * @return ApiModel
         */
        public static function singleton()
        {
            if (static::$instance === null) {
                $v = __CLASS__;
                static::$instance = new $v();
            }

            return static::$instance;
        }

        public function follow($follower, $followed) {
            $query = 'INSERT INTO follow VALUES (:follower, :followed)';
            $result = $this->query($query, [':follower' => $follower, ':followed' => $followed]);
            return true;
        }

        public function unfollow($follower, $followed) {
            $query = 'DELETE FROM follow WHERE follower = :follower AND followed = :followed LIMIT 1';
            $result = $this->query($query, [':follower' => $follower, ':followed' => $followed]);
            return true;
        }

        public function love($userID, $selfieID) {
            $query = 'INSERT INTO love (user_id, selfie_id) VALUES (:userID, :selfieID)';
            $result = $this->query($query, [':userID' => $userID, ':selfieID' => $selfieID]);
            return true;
        }

        public function unlove($userID, $selfieID) {
            $query = 'DELETE FROM love WHERE (user_id = :userID AND selfie_id = :selfieID)';
            $result = $this->query($query, [':userID' => $userID, ':selfieID' => $selfieID]);
            return true;
        }

        public function usernameTaken($username) {
            $query = 'SELECT u.id, u.username FROM users u WHERE u.username = :username LIMIT 1';
            $result = $this->query($query, [':username' => $username], TRUE);
            return count($result) ? true : false;
        }

        public function getSelfiesForMap($userID) {
            $query = 'SELECT s.lat, s.lng, s.title, s.hash, s.selfie_date, s.active_hash
                      FROM selfies_with_user s
                      WHERE s.user_id = :userID
                      ORDER BY s.selfie_date ASC';
            return $this->query($query, [':userID' => $userID], TRUE);
        }
    }
