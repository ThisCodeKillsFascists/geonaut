<?php

class EmailModel extends Model
{

    /**
     * @var EmailModel The class instance.
     * @internal
     */
    private static $instance = null;

    /**
     * Returns a EmailModel instance, creating it if it did not exist.
     * @return EmailModel
     */
    public static function singleton()
    {
        if (static::$instance === null) {
            $v = __CLASS__;
            static::$instance = new $v();
        }

        return static::$instance;
    }

    public function selectUserByToken($token) {
        $query = 'SELECT u.* FROM users u WHERE SHA1(CONCAT(u.id, u.member_since)) = :userToken LIMIT 1';

        $result = $this->query($query, [':userToken' => $token], TRUE);

        return $result ? $result[0] : false;
    }

    public function confirmEmail($userID) {
        $query = 'UPDATE users SET email_confirmed = 1 WHERE id = :userID LIMIT 1';

        $result = $this->query($query, [':userID' => $userID]);

        return $result->rowCount();
    }

    public function getRandomSelfie($limit = 1) {
        $query = 'SELECT s.* FROM selfies_with_user s ORDER BY RAND() LIMIT ' . $limit;

        return $this->query($query, [], TRUE);
    }
}
