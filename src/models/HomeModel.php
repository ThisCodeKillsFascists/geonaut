<?php

class HomeModel extends Model
{

    /**
     * @var HomeModel The class instance.
     * @internal
     */
    private static $instance = null;

    /**
     * Returns a HomeModel instance, creating it if it did not exist.
     * @return HomeModel
     */
    public static function singleton()
    {
        if (static::$instance === null) {
            $v = __CLASS__;
            static::$instance = new $v();
        }

        return static::$instance;
    }
}
