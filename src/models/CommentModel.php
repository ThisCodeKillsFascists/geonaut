<?php

    class CommentModel extends Model
    {

        /**
         * @var CommentModel The class instance.
         * @internal
         */
        private static $instance = null;

        /**
         * Returns a CommentModel instance, creating it if it did not exist.
         * @return CommentModel
         */
        public static function singleton()
        {
            if (static::$instance === null) {
                $v = __CLASS__;
                static::$instance = new $v();
            }

            return static::$instance;
        }

        public function getComments($selfieID) {

            $query = '
                SELECT c.*, u.username, u.name, u.profile_pic, u.profile_pic_default
                FROM comments c
                INNER JOIN users u ON u.id = c.user_id
                WHERE c.selfie_id = :selfieID
                ORDER BY added_on ASC
            ';

            $comments = $this->query($query, [':selfieID' => $selfieID], TRUE);
            return $comments;
        }

        public function saveComment($comment, $selfieID, $userID) {
            $query = 'INSERT INTO comments (comment, selfie_id, user_id, added_on)
                                    values (:comment, :selfieID, :userID, CURRENT_TIMESTAMP)';

            $this->query($query, [
                ':comment' => $comment,
                ':selfieID' => $selfieID,
                ':userID' => $userID
            ]);

            $commentID = $this->db->lastInsertId();

            return $commentID;
        }

        /**
         * NO AVATAR
         * @param $commentID
         *
         * @return bool|mixed
         */
        public function getCommentByID($commentID) {
            $query = '
                SELECT c.*, u.username, u.name
                FROM comments c
                INNER JOIN users u ON u.id = c.user_id
                WHERE c.id = :commentID
                ORDER BY added_on DESC';

            $commentArray = $this->query($query, [':commentID' => $commentID], TRUE);

            if (count($commentArray)) {
                return array_pop($commentArray);
            } else {
                return false;
            }
        }

        public function deleteCommentsBySelfie($selfieID) {
            $query = 'DELETE FROM comments WHERE selfie_id = :selfieID';
            $this->query($query, [':selfieID' => $selfieID]);
            return 1;
        }

        public function getUsersThatCommented($selfieID) {
            $query = 'SELECT u.id AS `index`, u.username, u.name, u.id AS user_id, c.id AS comment_id, c.comment
                      FROM comments c
                      INNER JOIN users u ON u.id = c.user_id
                      WHERE c.selfie_id = :selfieID
                      ORDER BY added_on DESC';
            $users = $this->queryIndexed($query, [':selfieID' => $selfieID], FALSE);
            return $users;
        }
    }