<?php

    class NotificationModel extends Model
    {

        /**
         * @var NotificationModel The class instance.
         * @internal
         */
        protected static $instance;

        /**
         * Returns a NotificationModel instance, creating it if it did not exist.
         * @return NotificationModel
         */
        public static function singleton() {
            if (!self::$instance) {
                $v = __CLASS__;
                self::$instance = new $v;
            }
            return self::$instance;
        }

        public function saveNotification($notifType, $fromUserID, $toUserID, $selfieID, $commentID = null) {
            $query = 'INSERT INTO notifications
                          (notif_type, user_id_from, user_id_to, selfie_id, extra)
                      VALUES
                          (:notifType, :fromUserID, :toUserID, :selfieID, 0)';

            $this->query($query, [
                ':notifType'  => $notifType,
                ':fromUserID' => $fromUserID,
                ':toUserID'   => $toUserID,
                ':selfieID'   => $selfieID,
            ]);
            $insertID = $this->db->lastInsertId();

            if ($commentID) {
                $this->query('UPDATE notifications SET comment_id = :commentID WHERE id = :insertID',[
                    ':insertID' => $insertID,
                    ':commentID' => $commentID
                ]);
            }

            return $insertID;
        }

        public function removeNotification($notifType, $fromUserID, $selfieID, $commentID) {
            $whereCommentID = ($commentID ? ' comment_id = :commentID ' : ' (:commentID OR 1) ');
            $query = "DELETE FROM notifications
                        WHERE (
                          notif_type   = :notifType AND
                          user_id_from = :fromUserID AND
                          -- user_id_to   = :toUserID AND
                          selfie_id    = :selfieID AND
                          $whereCommentID
                          )";
            $this->query($query, [
                ':notifType' => $notifType,
                ':fromUserID' => $fromUserID,
                // ':toUserID' => $toUserID,
                ':selfieID' => $selfieID,
                ':commentID' => $commentID
            ]);
        }

        public function getAllNotifications($userID, $limit = 10) {
            $query = 'SELECT n.notif_type, n.added_on, n.seen, n.id, n.extra,
                             u1.username AS username, u1.name, u1.profile_pic, u1.profile_pic_default,
                             s.active_hash, s.hash, s.title, s.added_on AS selfie_added_on,
                             c.comment, n.comment_id
                      FROM notifications n
                      INNER JOIN users u1 ON n.user_id_from = u1.id
                      INNER JOIN selfies_with_user s ON n.selfie_id = s.id
                      LEFT JOIN  comments `c` ON n.comment_id = c.id
                      WHERE n.user_id_to = :userID
                      ORDER BY n.seen IS NULL DESC, n.seen DESC, n.added_on DESC
                      LIMIT ' . $limit;

            $notifications = $this->query($query, [':userID' => $userID], TRUE);
            return $notifications;
        }

        public function countUnreadNotifications($userID) {
            $query = 'SELECT count(n.id) AS unread FROM notifications n WHERE n.seen IS NULL AND n.user_id_to = :userID';
            $unread = $this->query($query, [':userID' => $userID], TRUE);
            return $unread[0]['unread'];
        }

        public function markNotificationState($notificationID, $userID, $changeToState = false) {
            $notification = $this->getRow('notifications', $notificationID);

            if (empty($notification) || $notification['user_id_to'] != $userID) {
                return false;
            }
            // Notification exists and belongs to this user

            $seen = !!$notification['seen'];

            // if the notification has been SEEN and we're not trying to change the state to NOT SEEN:
            if ($seen && $changeToState !== 1) {
                // we only change to unseen:
                $query = 'UPDATE notifications SET seen = null WHERE id = :notificationID LIMIT 1';
                $this->query($query, [':notificationID' => $notificationID]);
                return 0;
            }
            if (!$seen && $changeToState !== 0) {
                // we only change it to seen
                $query = 'UPDATE notifications SET seen = CURRENT_TIMESTAMP WHERE id = :notificationID LIMIT 1';
                $this->query($query, [':notificationID' => $notificationID]);
                return 1;
            }

            // if it's in the desired state (because we've set a $changeToState) it won't enter any if. So:
            return $changeToState;

        }

        public function deleteAllNotifications($selfieID) {
            $query = 'DELETE FROM notifications WHERE selfie_id = :selfieID';
            $this->query($query, [':selfieID' => $selfieID]);
            return 1;
        }

        public function getUnreadNotification($type, $selfieID, $toUserID) {
            $query = 'SELECT n.*
                      FROM notifications n
                      WHERE n.seen IS NULL
                      AND n.notif_type = :type
                      AND n.selfie_id = :selfieID
                      AND n.user_id_to = :toUserID
                      LIMIT 1';

            $result = $this->query($query, [':type' => $type, ':selfieID' => $selfieID, ':toUserID' => $toUserID], TRUE);

            return count($result) ? $result[0] : false;
        }
    }
