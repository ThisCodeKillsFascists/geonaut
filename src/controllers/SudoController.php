<?php

    class SudoController extends Controller
    {

        /**
         * @var SudoController The class instance.
         * @internal
         */
        private static $instance = null;

        /**
         * Returns a SudoController instance, creating it if it did not exist.
         * @return SudoController
         */
        public static function singleton()
        {
            if (static::$instance === null) {
                $v = __CLASS__;
                static::$instance = new $v();
            }

            return static::$instance;
        }

        protected function __construct() {
            parent::__construct();

            self::$instance = $this;
        }

        public function reorganize() {
            //return;
            $selfies = HomeModel::singleton()->query('SELECT * FROM selfies', [], true);

            $folder = $this->uploadFolder . $this->uploadFName;
            foreach ($selfies as $s) {
                if ($s['id'] == 31 || $s['id'] == 1 || $s['id'] == 2) continue;
                $hashFolder = $folder . '/' . $s['hash'];

                mkdir($hashFolder);
                rename($folder . '/' . $s['me_image'], $hashFolder . '/' . __ME__);
                rename($folder . '/' . $s['lc_image'], $hashFolder . '/' . __LC__);
                rename($folder . '/thumbs/' . $s['me_image'], $hashFolder . '/thumb_' . __ME__);
                rename($folder . '/thumbs/' . $s['lc_image'], $hashFolder . '/thumb_' . __LC__);
                copy($hashFolder . '/thumb_' . __ME__, $hashFolder . '/mini_' . __ME__);
                copy($hashFolder . '/thumb_' . __LC__, $hashFolder . '/mini_' . __LC__);

                if ($s['cp_image'] && file_exists($folder . '/composite/' . $s['cp_image']))
                    rename($folder . '/composite/' . $s['cp_image'], $hashFolder . '/' . __CP__);

                HomeModel::singleton()->query('UPDATE selfies SET me_image = "' . __ME__ .'", lc_image = "'.__LC__.'"
                 , cp_image = "'. __CP__ .'" WHERE id = ' . $s['id']);
            }
        }

        public function copymini()
        {
            $folder = __ROOT__ . '/src/views/assets/images/pictures/uploads';
            $handle = scandir($folder);

            foreach ($handle as $h) {
                if ($h[0] == '.') continue;
                $f = $folder . '/' . $h . '/';
                copy($f . 'thumb_me.jpg', $f . 'mini_me.jpg');
                copy($f . 'thumb_lc.jpg', $f . 'mini_lc.jpg');
            }
        }

        public function shortenHash() {
            $selfies = $this->model->query('Select * from selfies', [], true);
            foreach ($selfies as $s) {
                $newHash = LoginSelfieController::singleton()->getUniqueSelfieHash();
                $this->model->updateRow('selfies', 'hash', $s['id'], $newHash);
                rename($this->uploadFolder . '/uploads/' . $s['hash'], $this->uploadFolder . '/uploads/' . $newHash);
                echo $s['title'], ' - ', $newHash, '<br/>';
            }
            die('?');
        }

        public function copyHashes() {
            $hashes = $selfies = $this->model->query('Select * from selfies', [], true);
            foreach ($hashes as $h) {
                HomeModel::singleton()->query('INSERT INTO selfie_url (selfie_id, hash) VALUES (:selfieid, :hash)',
                    [
                        ':selfieid' => $h['id'],
                        ':hash' => $h['hash']
                    ]);
            }
        }
    }