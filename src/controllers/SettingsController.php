<?php

    use Core\Session;

    class SettingsController extends Controller
    {

        /**
         * @var SettingsController The class instance.
         * @internal
         */
        private static $instance = null;

        /**
         * @var SettingsModel mixed The model instance associated to SettingsController
         */
        protected $model;

        /**
         * Returns a SettingsController instance, creating it if it did not exist.
         * @return SettingsController
         */
        public static function singleton()
        {
            if (static::$instance === null) {
                $v = __CLASS__;
                static::$instance = new $v();
            }

            return static::$instance;
        }

        protected function __construct() {
            parent::__construct();
            $this->model = SettingsModel::singleton();

            self::$instance = $this;
        }

        /**
         * Returns the instance of the model for this controller
         * @return SettingsModel
         */
        public function getModel() {
            return $this->model;
        }

        public function saveSettings() {

            $user = Session::getUser();

            //
            // PASSWORDS
            //
            // Check for matching and valid passwords
            $pass = $this->getPost('password', '');
            $passConfirm = $this->getPost('confirm');
            $passHash = false;
            if ($pass) {
              if (strlen($pass) < 8) {
                $this->json(['success' => 0, 'message' => 'password must be 8 characters or longer']);
              } elseif ($pass !== $passConfirm) {
                $this->json(['success' => 0, 'message' => 'passwords don\'t match']);
              } else {
                // Change password here
                $passHash = sha1($pass . $user['salt']);
                $savedPass = $this->model->updatePassword($this->selfID, $passHash);
              }
            }

            //
            // EMAIL
            //
            $email = $this->getPost('email');
            preg_match($this->config->get('Regex', 'EMAIL'), $email, $emailMatches);
            if (empty($emailMatches)) {
                $message = "That's not a valid email.<br/>I'm using ({$this->config->get('Regex', 'EMAIL')}) to validate";
                $this->json([
                    'success' => 0,
                    'message' => $message,
                    'type' => 'error']);
            }
            $userWithSameEmail = $this->model->getRow('users', $email, 'email');
            if (!empty($userWithSameEmail) && $user['id'] == $userWithSameEmail['id']) {
                // same user means same email, so we ignore it
            } elseif (empty($userWithSameEmail)) {
                // no users with that email, we can change it:
                $this->model->updateRow('users', 'email', $user['id'], $email);
            } else {
                // another user has this email!
                $this->json(['success' => 0, 'message' => "you cannot use this email, it belongs to someone else"]);
            }


            //
            // USERNAME
            //
            $username = $this->getPost('username');
            preg_match($this->config->get('Regex', 'USERNAME'), $username, $usernameMatches);
            if (empty($usernameMatches)) {
                $this->json([
                    'success' => 0,
                    'message' => 'username must consist of letters and numbers only',
                    'type' => 'error']);
            }
            $userWithSameUsername = $this->model->getRow('users', $username, 'username');
            if (!empty($userWithSameUsername) && $user['id'] == $userWithSameUsername['id']) {
                // same user means same username, so we ignore it
            } elseif (empty($userWithSameUsername)) {
                // no users with that username, we can change it:
                $this->model->updateRow('users', 'username', $user['id'], $username);
            } else {
                // another user has this email!
                $this->json(['success' => 0, 'message' => "you cannot use this username, it belongs to someone else"]);
            }


            //
            // NAME
            //
            // Check for valid/invalid $name
            $name = $_POST['name'] ?: $user['name'];
            preg_match($this->config->get('Regex', 'NAME'), $name, $nameMatches);
            if (empty($nameMatches)) {
                $this->json([
                    'success' => 0,
                    'message' => "That's not a valid name",
                    'type' => 'error'
                ]);
            } else {
                $this->model->updateRow('users', 'name', $user['id'], htmlentities($name));
            }


            //
            // NOTIFICATIONS BELL
            //
            // Position of the bell
            $bellPosition = $this->getPost('bell_position') ?: $user['bell_position'];
            if (!in_array($bellPosition, ['menu', 'top'])) {
                $this->json([
                    'success' => 0,
                    'message' => 'Not a valid option',
                    'type' => 'error'
                ]);
            } else {
                $this->model->updateRow('users', 'bell_position', $user['id'], $bellPosition);
            }

            //
            // THEME
            //
            $theme = $this->getPost('theme') ?: $user['theme'];
            if (!in_array($theme, [1, 2])) {
                $this->json([
                    'success' => 0,
                    'message' => 'Not a valid option',
                    'type' => 'error'
                ]);
            } else {
                $this->model->updateRow('users', 'theme', $user['id'], $theme);
            }

            $shortDesc = $this->getPost('short_desc', '');
            $this->model->updateRow('users', 'short_desc', $user['id'], $shortDesc);
            $gender = $this->getPost('gender', '');
            $this->model->updateRow('users', 'gender', $user['id'], $gender);
            $profilePicture = $this->getPost('profile_pic');
            $this->model->updateRow('users', 'profile_pic', $user['id'], $profilePicture);

            // If we got all the way here, everything is OK
            $user = Session::getUser();
            $user['name'] = $name;
            $user['email'] = $email;
            $user['username'] = $username;
            $user['bell_position'] = $bellPosition;
            $user['theme'] = $theme;
            $user['profile_pic'] = $profilePicture;
            Session::setUser($user);
            LoginController::singleton()->setUserAvatar($user);
            $message = "bzzzuuuuuuuppp... saved!";
            $this->json([
                'message' => $message,
                'type' => 'success',
                'loginData' => [
                    'name' => $user['name'],
                    'username' => $user['username'],
                    'bell_position' => $user['bell_position'],
                    'avatar' => $user['avatar'],
                ],
                'token' => generateToken($user)
            ]);
        }

        /**
         * Returns a valid date from some [maybe "0"] year, month and date, to store as the user's birthday
         * @param $date array the date object
         *
         * @return string
         */
        public function getBirthday($date) {
            if ($date['year'] == '0' || $date['month'] == '0' || $date['day'] == '0') {
                return false;
            } else {
                return $date['year'] . '-' . $date['month'] . '-' . $date['day'];
            }
        }

        public function saveTheme() {
            $darkTheme = ($this->getPost('theme') == 1);
            $theme = $darkTheme ? 2 : 1;
            $user = Session::getUser();
            $this->model->updateRow('users', 'theme', $user['id'], $theme);

            $user['theme'] = $theme;
            Session::setUser($user);

            $this->json(['message' => 'saved']);
        }
    }
