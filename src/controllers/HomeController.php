<?php

use Core\Session;
use Detection\MobileDetect;

/**
 * The HomeController is a user-defined controller. Should be in charge of managing the home.
 */
class HomeController extends Controller
{

    /**
     * @var HomeController The class instance.
     * @internal
     */
    private static $instance = null;

    /**
     * @var HomeModel The instance of HomeModel.
     */
    protected $model;

    /**
     * Returns a HomeController instance, creating it if it did not exist.
     *
     * @return HomeController
     */
    public static function singleton()
    {
        if (static::$instance === null) {
            $v = __CLASS__;
            static::$instance = new $v();
        }

        return static::$instance;
    }

    protected function __construct()
    {
        parent::__construct();
        $this->model = HomeModel::singleton();

        self::$instance = $this;
    }

    /**
     * Returns the instance of the model for this controller
     *
     * @return \HomeModel
     */
    public function getModel()
    {
        return $this->model;
    }

    public function showHome()
    {
        $this->redirect("https://geonaut.vercel.app");
    }

}
