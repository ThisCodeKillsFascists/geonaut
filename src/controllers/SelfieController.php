<?php

    use Core\Session;

    class SelfieController extends Controller
    {

        /**
         * @var SelfieController The class instance.
         * @internal
         */
        private static $instance = null;

        /**
         * @var mixed The model instance associated to SelfieController
         */
        protected $model;

        /**
         * Returns a SelfieController instance, creating it if it did not exist.
         * @return SelfieController
         */
        public static function singleton()
        {
            if (static::$instance === null) {
                $v = __CLASS__;
                static::$instance = new $v();
            }

            return static::$instance;
        }

        protected function __construct() {
            parent::__construct();
            $this->model = SelfieModel::singleton();

            self::$instance = $this;
        }

        /**
         * Returns the instance of the model for this controller
         * @return SelfieModel
         */
        public function getModel() {
            return $this->model;
        }

    }