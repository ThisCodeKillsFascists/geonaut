<?php

use Core\Session;

class LoginController extends Controller
{
  /**
   * @var LoginController The class instance.
   * @internal
   */
  private static $instance = null;

  /**
   * @var LoginModel The instance of LoginModel.
   */
  protected $model;

  /**
   * @var \Core\LoginController
   */
  protected $login;

  /**
   * Returns a LoginController instance, creating it if it did not exist.
   * @return LoginController
   */
  public static function singleton()
  {
    if (static::$instance === null) {
      $v = __CLASS__;
      static::$instance = new $v();
    }

    return static::$instance;
  }

  protected function __construct()
  {
    parent::__construct();
    $this->model = LoginModel::singleton();
    $this->login = \Core\LoginController::singleton();
    self::$instance = $this;
  }

  public function login()
  {
    $user = $this->login->login();

    $this->setUserAvatar($user);
    Session::setUser($user);

    $user_data = [
      'id' => $user['id'],
      'name' => $user['name'],
      'username' => $user['username'],
      'avatar' => $user['avatar'],
      'email' => $user['email'],
      'bell_position' => $user['bell_position'],
      'theme' => $user['theme']
    ];
    $this->json([
      'redirect' => $this->url('FrontendLoginHome'),
      'message' => 'welcome back!',
      'user' => $user_data,
      'token' => generateToken($user)
    ]);
  }

  public function signup()
  {
    $name = $this->getPost('name');
    $username = $this->getPost('username');
    $email = $this->getPost('email');
    $password = $this->getPost('password');

    if (strlen(trim($name)) < 4) {
      $this->json([
        'success' => 0,
        'message' => 'name too short',
        'field' => 'name',
        'received' => $name
      ]);
    } elseif (!preg_match($this->config->get('Regex', 'NAME'), $name)) {
      $this->json([
        'success' => 0,
        'message' => 'name consists of letters, spaces and [-\'] only',
        'field' => 'name',
        'received' => $name
      ]);
    } elseif (!preg_match($this->config->get('Regex', 'USERNAME'), $username)) {
      $this->json([
        'success' => 0,
        'message' => 'username must consist of letters and numbers only',
        'field' => 'username',
        'received' => $username
      ]);
    } elseif (count($this->model->getRow('users', $username, 'username'))) {
      $this->json([
        'success' => 0,
        'message' => 'this username is in use!',
        'field' => 'username',
        'received' => $username
      ]);
    } elseif (!preg_match($this->config->get('Regex', 'EMAIL'), $email)) {
      $this->json([
        'success' => 0,
        'message' => 'this email is not valid',
        'field' => 'email',
        'received' => $email
      ]);
    } elseif (count($this->model->getRow('users', $email, 'email'))) {
      $this->json([
        'success' => 0,
        'message' => 'this email is in use',
        'field' => 'email',
        'received' => $email
      ]);
    } elseif (strlen($password) < 8) {
      $this->json([
        'success' => 0,
        'message' => 'password must be 8+ characters',
        'field' => 'password',
        'received' => str_repeat('*', strlen($password))
      ]);
    } else {
      // All fields valid...
    }


    $salt = sha1($name . $username . $password . $email);
    $role = 2;
    $saltedPassword = sha1($password . $salt);

    $avatarHandle = scandir(__ROOT__ . '/public/images/icons/default_profile');
    $profilePicDefault = $avatarHandle[mt_rand(3, count($avatarHandle) - 1)];

    $newUserID = $this->model->signup($role, $name, $email, $username, $saltedPassword, $salt, $profilePicDefault);


    if ($newUserID) {
      $user = $this->login->authenticateUser($username, $password);
      $this->setUserAvatar($user);
      Session::setUser($user);
      try {
        $sentEmail = $this->sendWelcomeEmail($user);
      } catch (Exception $e) {
        // var_dump($e);
        $this->model->query("DELETE from users WHERE id = :id", [':id' => $newUserID]);
        $this->json(['success' => 0, 'message' => (string)$e]);
      }

      $this->json([
        'message' => 'welcome!',
        'redirect' => $this->url('FrontendLoginHome'),
        'user' => $user,
        'sent' => $sentEmail,
        'sent_to' => $email,
        'token' => generateToken($user)
      ]);
    } else {
      $this->json(['success' => 0, 'message' => 'couldn\'t save for some unknown reason']);
    }
  }

  public function forgot()
  {
    $email = $this->getPost('email');
    $user = $this->model->getRow('users', $email, 'email');

    if (!$user) {
      $this->json(['message' => 'check your email']);
    } else {
      // OK!
      $mailer = \Kernel\Mailer::singleton();

      $userToken = sha1($user['id'] . $user['member_since'] . mt_rand(0, 1000));
      $user['token'] = $userToken;

      $this->model->updateRow('users', 'token', $user['id'], $userToken);

      $sentEmail = $mailer->sendEmail(
        $email,
        'password reset - ' . __SITE_NAME__,
        $this->get('html/email/forgot', [
          'user' => $user,
          'nico' => $this->model->getRow('users', 1),
        ], 'emails/default'),
        $this->config->get('Email', 'FROM'),
        $this->config->get('Email', 'REPLY_TO')
      );

      $this->json(['message' => 'check your email', 'sent' => $sentEmail]);
    }
  }

  public function doReset()
  {
    $token = $this->getPost('token');
    $password = $this->getPost('password');
    $confirm  = $this->getPost('confirm');
    $user = $this->model->getRow('users', $token, 'token');
    if (!$user) {
      $this->json(['success' => 0, 'message' => 'invalid or expired token. request email again']);
    }

    if ($password !== $confirm) {
      $this->json(['success' => 0, 'message' => 'passwords don\'t match']);
    } elseif (strlen($password) < 8) {
      $this->json(['success' => 0, 'message' => 'password must be 8 characters or more']);
    }

    // We have a valid user and a valid password
    $saltedPassword = sha1($password . $user['salt']);

    $this->model->updateRow('users', 'password', $user['id'], $saltedPassword);

    // We delete the token:
    $this->model->updateRow('users', 'token', $user['id'], '');
    // We redirect the user to the login page
    $this->json(['message' => 'password changed. please log in again', 'redirect' => $this->url('FrontendLogin')]);
  }

  public function logout()
  {
    $this->login->deauthenticateUser();
    $this->json([
      'redirect' => $this->url('Home'),
      'message'  => 'see you soon!'
    ]);
  }

  public function sendWelcomeEmail($user)
  {
    $mailer = \Kernel\Mailer::singleton();

    $userToken = sha1($user['id'] . $user['member_since']);

    $sentEmail = $mailer->sendEmail(
      $user['email'],
      'welcome to ' . __SITE_NAME__,
      $this->get('html/email/welcome', [
        'user' => $user,
        'userToken' => $userToken,
        'nico' => $this->model->getRow('users', 1),
        'viewInBrowser' => true,
        'randomSelfies' => EmailModel::singleton()->getRandomSelfie(3)
      ], 'emails/default'),
      $this->config->get('Email', 'FROM'),
      $this->config->get('Email', 'REPLY_TO')
    );

    return $sentEmail;
  }
}
