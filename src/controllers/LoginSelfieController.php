<?php

use Core\Session;
use ColorThief\ColorThief;

class LoginSelfieController extends Controller
{
    // MARK: Properties

    /**
     * @var LoginSelfieController The class instance.
     * @internal
     */
    private static $instance = null;

    /**
     * @var SelfieModel The model instance associated to LoginSelfieController
     */
    protected $model;

    protected $geocoding = 'https://maps.googleapis.com/maps/api/geocode/json?key=' . __GAPIKEY__;

    protected $staticMapURL = 'https://maps.googleapis.com/maps/api/staticmap?key=' . __GAPIKEY__;

    protected $searchBox = 'https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.1.1/mapbox-gl-geocoder.min.js';

    protected $selfieTypeID = '2';
    // MARK: Declarations

    /**
     * Returns a LoginSelfieController instance, creating it if it did not exist.
     * @return LoginSelfieController
     */
    public static function singleton()
    {
        if (static::$instance === null) {
            $v = __CLASS__;
            static::$instance = new $v();
        }

        return static::$instance;
    }

    protected function __construct()
    {
        parent::__construct();

        $this->model = SelfieModel::singleton();

        self::$instance = $this;
    }

    /**
     * Returns the instance of the model for this controller
     * @return SelfieModel
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return array
     */
    public function createSelfieDraft()
    {
        $selfieDraft = $this->model->getSelfieDraft($this->selfID);

        if (empty($selfieDraft)) {
            $selfieDraft = $this->model->createSelfieDraft($this->selfID);

            // We generate a unique hash for the selfie
            $selfieHash = $this->getUniqueSelfieHash();

            $selfieID = $selfieDraft['id'];
            // First, we add the unique hash:
            $this->model->addSelfieURL($selfieID, $selfieHash, 1);
            $selfieDraft['hash'] = $selfieHash;
        } else {
            $selfieDraft['hash'] = $this->model->getSelfieHash($selfieDraft['id']);
        }

        return $selfieDraft;
    }

    public function saveSelfie()
    {
        // Check if it's a valid selfie and if the user owns it:
        $selfieHash = $this->getPost('hash');
        $selfie = $this->model->getSelfieByHash($selfieHash);

        $this->checkSelfieValidity($this->selfID, $selfie);
        // if it steps out, it's valid

        $isNewSelfie = ($selfie['draft'] == 1);

        $title = $this->getPost('title');
        $subtitle = $this->getPost('subtitle');
        $longDesc = $this->filterLongDescIframes($_POST['description']);
        $date = $this->getPost('date');
        $selfieURL = $this->getPost('url');
        // Get location parameters
        $lat = $this->getPost('lat', 0);
        $lng = $this->getPost('lng', 0);
        $place = $this->getPost('place', 0);

        $this->checkSelfieDetailsValidity($title, $subtitle, $date, $lat, $lng, $place);

        if ($isNewSelfie) {
            $me = __ME__;
            $lc = __LC__;

            $uploadFolder = $this->uploadFolder($selfieHash);

            // We check that the images exist
            if (!file_exists($uploadFolder . '/' . $me)) {
                $this->json(['success' => 0, 'message' => "where's the picture of your pretty face?"]);
            }
            if (!file_exists($uploadFolder . '/' . $lc)) {
                $this->json(['success' => 0, 'message' => "you must upload a picture of what you see!"]);
            }

            // we create an image composite with both images:
            if ($this->composeImage($selfieHash, $me, $lc) === false) {
                $this->json(['success' => 0, 'message' => 'you need to upload images first!']);
            }

        }

        $selfieURL = $this->addSelfieURL($selfieHash, $selfieURL);

        if (!$selfieURL) {
            // we can't use custom URL, it has been taken!
            $this->json(['success' => 0, 'message' => 'sorry, this URL is taken. Please use a different one']);
        } // else:
        // continue;
        //   endif;

        $edited = (bool)$this->model->editSelfie(
            $selfie['id'],
            $title,
            $subtitle,
            $longDesc,
            $date,
            $place,
            $lat,
            $lng,
            $this->selfieTypeID,
            1,
            0,
        );

        // Return response accordingly
        if ($edited) {
            $message = 'edits saved';
            if ($isNewSelfie) {
                $this->model->updateRow("selfies", "added_on", $selfie['id'], date('Y-m-d H:i:s'));
                $message = 'published';
                NotificationController::singleton()->add('post', $selfie['id']);
            }
            // Selfie saved!
            $this->json(['message' => $message, 'redirect' => $this->url('Selfie', [':selfie' => $selfieURL])]);
        } else {
            $this->json(['success' => 0, 'message' => 'there was a problem with the database. try again...']);
        }
    }

    public function deleteSelfie()
    {
        $selfieHash = $_GET['selfie'];
        $selfie = $this->model->getSelfieByHash($selfieHash);

        // Check if it's a valid selfie and if the user owns it:
        $this->checkSelfieValidity($this->selfID, $selfie);
        // if it steps out, it's valid

        $deleteLikes = $this->model->deleteLikes($selfie['id']);
        $deleteComments = CommentModel::singleton()->deleteCommentsBySelfie($selfie['id']);
        $deleteNotifications = NotificationModel::singleton()->deleteAllNotifications($selfie['id']);

        // Now we delete the selfie
        $deleted = (bool)$this->model->deleteSelfie($selfie['id']);

        if ($deleted) {
            $directory = $this->uploadFolder . $this->uploadFName . '/' . $selfie['hash'];
            if (is_dir($directory)) {
                deleteDir($directory);
            }
            $this->json(['redirect' => $this->url('FrontendLoginHome'), 'message' => 'deleted!']);
        } else {
            $this->json(['success' => 0, 'message' => 'there was an unknown error on the server']);
        }
    }

    public function saveChanges()
    {
        $selfieHash = $this->getPost('hash');
        $selfie = $this->model->getSelfieByHash($selfieHash);

        if ($selfie['visible'] == 1) {
            $this->json(['success' => 0, 'message' => 'we don\'t save changes to published selfies']);
        }

        // Check if it's a valid selfie and if the user owns it:
        $this->checkSelfieValidity($this->selfID, $selfie);

        $title = $this->getPost('title') ?: '';
        $subtitle = $this->getPost('subtitle') ?: '';
        $longDesc = $this->filterLongDescIframes($_POST['description']) ?: '';
        $date = $this->getPost('date') ?: date('Y-m-d');

        // Get location parameters
        $lat = $this->getPost('lat', 0);
        $lng = $this->getPost('lng', 0);
        $place = $this->getPost('place', 0);

        $edited = (bool)$this->model->editSelfie(
            $selfie['id'],
            $title,
            $subtitle,
            $longDesc,
            $date,
            $place,
            $lat,
            $lng,
            $this->selfieTypeID,
            0,
            $selfie['draft']
        );

        $this->json(['message' => 'saved', 'success' => $edited]);
    }

    // MARK: Logic

    public function uploadSelfie()
    {

        $uploadedFile = $_FILES["_file_"]["tmp_name"];

        $image_data = getimagesize($uploadedFile);
        if ($image_data === false) {
            $this->json(['success' => 0, 'message' => 'file has no size. is it an image?']);
        }

        $selfieHash = $this->getPost('hash');
        $selfie = $this->model->getSelfieByHash($selfieHash);

        if (!$selfie) {
            $this->json(['success' => 0, 'message' => 'id cannot be empty. refresh the page']);
        }

        // Type of image: ME or Landscape
        $type = $this->getPost('type', false);
        if (!$type) {
            $this->json([
                'success' => 0,
                'message' => 'wrong image type. this shouldn\'t have happened'
            ]);
        }

        // we store the name and destination path:
        $destinationName = $type . '.jpg'; // extension doesn't change the EXIF/MIME type.
        $destination = $this->uploadTmp . '/original_' . $destinationName;

        // Delete all images:
        if (file_exists($destination)) {
            unlink($destination);
        }

        // if file is OK, and it saves in the server:
        if (move_uploaded_file($uploadedFile, $destination)) {

            // Check if it's a supported file type:
            if (!in_array(exif_imagetype($destination), [IMAGETYPE_JPEG, IMAGETYPE_PNG])) {
                $this->json([
                    'success' => 0,
                    'type' => $type,
                    'uploadedType' => exif_imagetype($destination),
                    'message' => 'image type not supported. use PNG or JPEG'
                ]);
            }

            $imageType = exif_imagetype($destination);

            // File upload was completed
            $metadata = [];

            if (in_array($imageType, [IMAGETYPE_JPEG, IMAGETYPE_TIFF_II, IMAGETYPE_TIFF_MM])) {

                try {
                    $metadata = exif_read_data($destination, 0, true);
                } catch (Exception $e) {
                    //nothing
                }

                if (!empty($metadata['GPS'])) {
                    // Sometimes the GPS is not set but the coordinates are all ["0/0", "0/0", "0/0"]
                    $gps = $this->getImageLocationFromGPS($metadata['GPS']);
                } else {
                    $gps = false;
                }
            } else {
                // no GPS if it's not a JPG or TIFF
                $gps = false;
            }

            $this->model->updateRow('selfies', 'meta_' . $type, $selfie['id'], json_encode($metadata));

            // Convert to JPG
            switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageSrc = imagecreatefrompng($destination);
                    break;
                case IMAGETYPE_JPEG:
                    $imageSrc = imagecreatefromjpeg($destination);
                    break;
                case IMAGETYPE_GIF:
                    $imageSrc = imagecreatefromgif($destination);
                    break;
                default:
                    return $imageType;
            }

            // autorotate just in case
            if (!empty($metadata['IFD0']['Orientation'])) {
                switch ($metadata['IFD0']['Orientation']) {
                    case 8:
                        $imageSrc = imagerotate($imageSrc, 90, 0);
                        break;
                    case 3:
                        $imageSrc = imagerotate($imageSrc, 180, 0);
                        break;
                    case 6:
                        $imageSrc = imagerotate($imageSrc, -90, 0);
                        break;
                }
            }



            $directory     = $this->uploadFolder($selfieHash);
            $directoryPath = $this->uploadPath($selfieHash);
            if (!is_dir($directory)) {
                mkdir($directory);
            }
            $originalFileDestination = $directory . '/original_' . $destinationName;



            // and save on the final folder:
            imagejpeg($imageSrc, $originalFileDestination);

            /*
                 * Now we have the original image, original_xx.jpg, converted to JPG, in the folder.
                 */

            // names don't change, but size does:
            $resized = $this->resizeFile($directory, $destinationName);

            $imageColor = $this->getImageColor($selfie['id'], $type, $originalFileDestination);

            if ($resized === true) {
                $this->json([
                    'gps' => $gps,
                    'finalName' => $destinationName,
                    'img' => $directoryPath . '/' . $destinationName,
                    'mini' => $directoryPath . '/' . 'mini_' . $destinationName,
                    'date' => !empty($metadata['EXIF']) ? strtotime($metadata['EXIF']['DateTimeOriginal']) : 0,
                    'message' => ($type == 'me' ? "your" : "the landscape") . " image has been uploaded",
                    'color' => $imageColor,
                    'resized' => 1
                ]);
            } else {
                $this->json([
                    'success' => 0,
                    'type' => $type,
                    'message' => 'could not resize images, sorry'
                ]);
            }
        } else {
            $this->json(['message' => 'chunk uploaded']);
            // This is not a final chunk, continue to upload
        }
    }

    public function uploadAttachment()
    {

        $uploadedFile = $_FILES["_file_"]["tmp_name"];

        $image_data = getimagesize($uploadedFile);
        if ($image_data === false) {
            $this->json(['success' => 0, 'message' => 'file has no size. is it an image?']);
        }

        $selfieHash = $this->getPost('hash');
        $selfie = $this->model->getSelfieByHash($selfieHash);

        if (!$selfie) {
            $this->json(['success' => 0, 'message' => 'id cannot be empty. refresh the page']);
        }

        // Type of image: any except "me" or "lc"
        $img_name = $this->getPost('type', false);
        if (!$img_name || !is_string($img_name) || strlen($img_name) <= 3) {
            $this->json([
                'success' => 0,
                'message' => 'image name not provided or long enough'
            ]);
        }
        $img_name = pathinfo($img_name, PATHINFO_FILENAME);
        $img_name = str_replace("/", "", $img_name);
        $invalid_prefixes = ["original", "mini", "thumb"];
        $has_invalid_name = false;
        foreach ($invalid_prefixes as $prefix) {
            if (strpos($img_name, $prefix) === 0) {
                $has_invalid_name = true;
                break;
            }
        }
        if ($has_invalid_name || in_array($img_name, ["me", "lc"])) {
            $this->json([
                'success' => 0,
                'message' => 'wrong image name. cannot be "me", "lc" nor start with "original", "mini" or "thumb"'
            ]);
        }

        // we store the name and destination path:
        $destinationName = __ATTACHMENT_PREFIX__ . $img_name . '.jpg'; // extension doesn't change the EXIF/MIME type.
        $destination = $this->uploadTmp . '/' . $destinationName;

        // Delete image if it matches the name:
        if (file_exists($destination)) {
            unlink($destination);
        }

        // if file is OK, and it saves in the server:
        if (move_uploaded_file($uploadedFile, $destination)) {

            // Check if it's a supported file type:
            if (!in_array(exif_imagetype($destination), [IMAGETYPE_JPEG, IMAGETYPE_PNG])) {
                $this->json([
                    'success' => 0,
                    'src' => $img_name,
                    'uploadedType' => exif_imagetype($destination),
                    'message' => 'image type not supported. use PNG or JPEG'
                ]);
            }

            $imageType = exif_imagetype($destination);

            // File upload was completed
            $metadata = [];

            if (in_array($imageType, [IMAGETYPE_JPEG, IMAGETYPE_TIFF_II, IMAGETYPE_TIFF_MM])) {
                try {
                    $metadata = exif_read_data($destination, 0, true);
                } catch (Exception $e) {
                    //nothing
                }
            }

            // Convert to JPG
            switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageSrc = imagecreatefrompng($destination);
                    break;
                case IMAGETYPE_JPEG:
                    $imageSrc = imagecreatefromjpeg($destination);
                    break;
                case IMAGETYPE_GIF:
                    $imageSrc = imagecreatefromgif($destination);
                    break;
                default:
                    return $imageType;
            }

            // autorotate just in case
            if (!empty($metadata['IFD0']['Orientation'])) {
                switch ($metadata['IFD0']['Orientation']) {
                    case 8:
                        $imageSrc = imagerotate($imageSrc, 90, 0);
                        break;
                    case 3:
                        $imageSrc = imagerotate($imageSrc, 180, 0);
                        break;
                    case 6:
                        $imageSrc = imagerotate($imageSrc, -90, 0);
                        break;
                }
            }

            $directory     = $this->uploadFolder($selfieHash);
            $directoryPath = $this->uploadPath($selfieHash);
            if (!is_dir($directory)) {
                mkdir($directory);
            }
            $originalFileDestination = $directory . '/' . $destinationName;

            // and save on the final folder:
            imagejpeg($imageSrc, $originalFileDestination);

            $this->json([
                'finalName' => $destinationName,
                'src' => $directoryPath . '/' . $destinationName,
                'uploadedType' => exif_imagetype($destination),
                'message' => "image uploaded",
            ]);
        } else {
            $this->json(['success' => 0, 'message' => 'error!']);
        }
    }

    protected function resizeFile($directory, $fileName)
    {
        // Presetting the names
        $fileSrc      = $directory . '/original_' . $fileName;

        $fileDst      = $directory . '/'         . $fileName;
        $fileDstThumb = $directory . '/thumb_'   . $fileName;
        $fileDstMini  = $directory . '/mini_'    . $fileName;

        if (file_exists($fileDst)) {
            unlink($fileDst);
        }
        if (file_exists($fileDstThumb)) {
            unlink($fileDstThumb);
        }
        if (file_exists($fileDstMini)) {
            unlink($fileDstMini);
        }

        // we know it's a JPEG :)
        $imageSrc = imagecreatefromjpeg($fileSrc);

        $prop = 1.3333; // proportion

        // We want to keep proportions. Also, we want to resize if the file is more than 1000px wide.
        list($fileSrcWidth, $fileSrcHeight) = getimagesize($fileSrc);
        $dstWidth = (int)min(1000, $fileSrcWidth);
        $dstHeight = (int)round($dstWidth / $prop);

        // We want the "resized" image to be centered (because we don't want distortions)
        if (($fileSrcHeight * $prop) >= $fileSrcWidth) { // too squared
            $x = 0;
            $y = (int)round((($fileSrcHeight * $prop) - $fileSrcWidth) / (2 * $prop));
            // we cheat the system to believe the picture is not so "tall" (squared)
            $fileSrcHeight = (int)($fileSrcWidth / $prop);
        } else { // too panoramic
            $x = (int)round(($fileSrcWidth - $fileSrcHeight * $prop) / (2 * $prop));
            $y = 0;
            // we cheat the system to believe the picture is not so "wide" (panoramic)
            $fileSrcWidth = (int)($fileSrcHeight * $prop);
        }

        // We create a true-color 1000x750 image
        $imageDst = imagecreatetruecolor($dstWidth, $dstHeight);
        imagecopyresized($imageDst, $imageSrc, 0, 0, $x, $y, $dstWidth, $dstHeight, $fileSrcWidth, $fileSrcHeight);
        $createdDst = imagejpeg($imageDst, $fileDst);

        // We create a true-color 400x300 image (thumb)
        $dstWidthThumb  = 400;
        $dstHeightThumb = round($dstWidthThumb / $prop);
        $imageDstThumb = imagecreatetruecolor($dstWidthThumb, $dstHeightThumb);
        imagecopyresized($imageDstThumb, $imageSrc, 0, 0, $x, $y, $dstWidthThumb, $dstHeightThumb, $fileSrcWidth, $fileSrcHeight);
        $createdThumb = imagejpeg($imageDstThumb, $fileDstThumb);

        // We create a true-color 160x120 image (mini)
        $dstWidthMini  = 160;
        $dstHeightMini = round($dstWidthMini / $prop);
        $imageDstMini = imagecreatetruecolor($dstWidthMini, $dstHeightMini);
        imagecopyresized($imageDstMini, $imageSrc, 0, 0, $x, $y, $dstWidthMini, $dstHeightMini, $fileSrcWidth, $fileSrcHeight);
        $createdMini = imagejpeg($imageDstMini, $fileDstMini);

        // True only if all were created successfully.
        return $createdDst && $createdThumb && $createdMini;
    }

    protected function composeImage($selfieHash, $fileNameMe, $fileNameLc)
    {
        $uploadFolder = $this->uploadFolder($selfieHash);
        $fileMe = $uploadFolder . '/' . $fileNameMe;
        $fileLc = $uploadFolder . '/' . $fileNameLc;
        // <some-hash>-<other-hash>.jpg
        $fileNameCp = 'cp.jpg';
        $fileCp = $uploadFolder . '/' . $fileNameCp;

        if (empty($fileNameMe) || empty($fileNameLc) || !file_exists($fileMe) || !file_exists($fileLc)) {
            return false;
        }

        ini_set('memory_limit', '256M');

        list($blankWidth, $blankHeight) = getimagesize($fileMe); // both have the same size ;)

        $blank = imagecreatetruecolor($blankWidth, 2 * $blankHeight);
        $fileMeImage = imagecreatefromjpeg($fileMe);
        $fileLcImage = imagecreatefromjpeg($fileLc);

        imagecopymerge($blank, $fileMeImage, 0, 0,            0, 0, $blankWidth, $blankHeight, 100);
        imagecopymerge($blank, $fileLcImage, 0, $blankHeight, 0, 0, $blankWidth, $blankHeight, 100);

        return imagejpeg($blank, $fileCp) ? $fileNameCp : FALSE;
    }

    public function getImageLocationFromGPS($metadataGPS)
    {
        if (empty($metadataGPS['GPSLatitude']) && $metadataGPS['GPSLatitude'][0] != "0/0") {
            return false;
        }

        $coordinates = $this->getGPSCoordinates($metadataGPS);
        $place = $this->reverseGeocoding($coordinates);

        $map = $this->staticMapURL . '&size=640x280&center=' . implode(',', $coordinates) . '&zoom=13&markers=' . implode(',', $coordinates);

        return ([
            'lat' => round($coordinates[0], 2),
            'lng' => round($coordinates[1], 2),
            'place' => $place,
            'map' => $map
        ]);
    }

    protected function createAndUploadFile($flowIdentifier, $numChunks)
    {
        $file = '';
        $fileChunks = $_SESSION['flow'][$flowIdentifier];
        for ($i = 1; $i <= $numChunks; ++$i) {
            $file .= file_get_contents($fileChunks['_' . $i]['tmp_name']);
        }
        file_put_contents(__ROOT__ . '/src/views/assets/images/pictures/' . $flowIdentifier . '.jpg', $file);
    }

    protected function checkAndDeleteDuplicates($uniquid)
    {
        $duplicateFile = @$_SESSION['flow'][$uniquid];
        // We save this image for this uniquid to avoid double uploads.
        if (!empty($duplicateFile) && file_exists($this->uploadFolder . $this->uploadFName . '/' . $duplicateFile)) {
            unlink($this->uploadFolder . $this->uploadFName . '/' . $duplicateFile);
        }
    }

    protected function checkAndDeletePreviousUpload($type)
    {
        $image = Session::get($type . 'Image');
        if ($image) {
            $this->deleteImage($this->uploadTmp . '/' . $image);
            $this->deleteImage($this->uploadTmp . '/thumb_' . $image);
            $this->deleteImage($this->uploadTmp . '/mini_' . $image);
        }
    }

    protected function deleteImage($imagePath)
    {
        if (file_exists($imagePath)) {
            return unlink($imagePath);
        }
        return false;
    }

    protected function getGPSCoordinates($gps, $gps2 = false)
    {
        if ($gps['GPSLatitude'][0] == '0/1') {
            $gps = $gps2;
        }
        $latIndex = ($gps['GPSLatitudeRef'] == 'N' ? 1 : -1);
        $lngIndex = ($gps['GPSLongitudeRef'] == 'E' ? 1 : -1);
        $latitude = $gps['GPSLatitude'];
        $longitude = $gps['GPSLongitude'];
        $lat = $latitude
            ? ($this->gpsVal($latitude[0]) + ($this->gpsVal($latitude[1])) / 60 + ($this->gpsVal($latitude[2])) / 3600) * $latIndex
            : 0;
        $lng = $longitude
            ? ($this->gpsVal($longitude[0]) + ($this->gpsVal($longitude[1])) / 60 + ($this->gpsVal($longitude[2])) / 3600) * $lngIndex
            : 0;
        return [$lat, $lng];
    }

    protected function gpsVal($coord)
    {
        list($number, $divider) = explode('/', $coord);
        return ($divider == 0 ? $divider : floatval($number) / floatval($divider));
    }

    protected function reverseGeocoding($coordinates)
    {
        $url = $this->geocoding . '&latlng=' . $coordinates[0] . ',' . $coordinates[1] . '&sensor=true';
        $data = @file_get_contents($url);
        $jsondata = json_decode($data, true);

        if (is_array($jsondata) && $jsondata['status'] == "OK") {
            $results = $jsondata['results']['0']['address_components'];
            $full = $jsondata['results']['0']['formatted_address'];

            $city = '';
            $country = '';
            for ($i = 0; $i < count($results); ++$i) {
                $j = $results[(string)$i];
                switch ($j['types'][0]) {
                    case 'locality':
                    case 'postal_town':
                    case 'administrative_area_level_2':
                    case 'administrative_area_level_1':
                        $city = $city ?: $j['long_name'];
                        break;
                    case 'country':
                        $country = $j['long_name'];
                        break;
                    default:
                        break;
                }
            }

            return ($city && $country ? ($city . ', ' . $country) : $full);
        }
        return '(' . round($coordinates[0], 4) . ',' . round($coordinates[1], 4) . ')';
    }

    private function getImageColor($selfieID, $type, $file)
    {
        ini_set('memory_limit', '256M');
        include_once __ROOT__ . '/app/libs/ColorThief/ColorThief.php';

        // We get the main color and brightness out of both pictures:
        $colorRGB = ColorThief::getColor($file);
        $color = $colorRGB[0] . ',' . $colorRGB[1] . ',' . $colorRGB[2];
        $brightness = round(sqrt(0.299 * pow((int)$colorRGB[0], 2) + 0.587 * pow((int)$colorRGB[1], 2) + 0.114 * pow((int)$colorRGB[2], 2)), 2);

        // var_dump($selfieID, $type, $color, $brightness);die();
        $edited = $this->model->editSelfieColorData($selfieID, $type, $color, $brightness);

        return ['color' => $color, 'brightness' => $brightness];
    }

    /**
     * Rotates an image
     *
     */
    public function rotateImage()
    {
        ini_set('memory_limit', '256M');
        $type = $this->getPost('type');
        $fileName = $type . '.jpg';

        $selfieHash = $this->getPost('hash');

        $uploadPath   = $this->uploadPath($selfieHash);
        $uploadFolder = $this->uploadFolder($selfieHash);

        //die($this->uploadFolder . '/original_' . $fileName);
        $image = imagecreatefromjpeg($uploadFolder . '/original_' . $fileName);
        $rotate = imagerotate($image, 270, 0);

        imagejpeg($rotate, $uploadFolder . '/original_' . $fileName);
        imagedestroy($image);
        imagedestroy($rotate);

        copy($uploadFolder . '/original_' . $fileName, $uploadFolder . '/' . $fileName);

        $this->resizeFile($uploadFolder, $fileName);

        $this->json(['path' => $uploadPath . '/' . $fileName]);
    }

    /**
     * Returns a unique base64-hash (using ::generateHash()), i.e. that has not been used by any selfie.
     * @param int $length The length of the string
     *
     * @return string Unique hash
     */
    public function getUniqueSelfieHash($length = 5)
    {
        do {
            $hash = $this->generateHash($length);
            $exists = (bool)$this->model->getSelfie($hash);
        } while ($exists);
        return $hash;
    }

    /**
     * Generates a base-64 hash of length $length
     * @param int $length The length for the  hash
     *
     * @return string A base-64, youtube-like hash
     */
    private function generateHash($length = 11)
    {
        $validCharacters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; // base62
        //$validCharacters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_'; // base64
        $validCharNumber = strlen($validCharacters);
        $result = '';

        for ($i = 0; $i < $length; $i++) {
            $index = mt_rand(0, $validCharNumber - 1);
            $result .= $validCharacters[$index];
        }

        return $result;
    }

    /**
     * Adds a new Hash to selfie_url table if the $selfieURL is not associated with $selfieHash and it's available.
     *
     * @param string $selfieHash The self-generated Hash for the selfie
     * @param string $selfieURL The custom (user-entered) Hash for the selfie
     *
     * @return bool|mixed
     */
    private function addSelfieURL($selfieHash, $selfieURL)
    {
        // first we sanitise:
        $selfieURL = $this->sanitiseURL($selfieURL);

        // We add a custom URL if the user has set one
        if ($selfieURL != $selfieHash && $selfieURL != 'custom-url' && !empty($selfieURL)) {
            // this means there's been a customisation of the hash!
            $selfieIDFromURL  = $this->model->getSelfieID($selfieURL);
            $selfieIDFromHash = $this->model->getSelfieID($selfieHash);
            if ($selfieIDFromURL == 0) {
                // it means that it's not taken, so we add in the model:
                $this->model->addSelfieURL($selfieIDFromHash, $selfieURL);
                return $selfieURL;
            } elseif ($selfieIDFromHash == $selfieIDFromURL) {
                // exists BUT/and it's from the same selfie. Nothing to do
                return $selfieURL;
            } else {
                // WRONG! URL taken, must notify
                return false;
            }
        } else {
            return $selfieHash;
        }
    }

    /**
     * $longDesc can *only* contain &lt;iframe&gt; that are from youtube.com:
     * @param string $longDesc the content of the Selfie wysiwyg editor
     *
     * @return string encoded with htmlentities
     */
    private function filterLongDescIframes($longDesc)
    {
        if (empty($longDesc)) {
            return "";
        }

        $longDescDoc = new DOMDocument;

        @$longDescDoc->loadHTML($longDesc);

        if (!$longDescDoc->doctype) { // if the description is empty:
            return $longDesc;
        }

        $longDescDoc->removeChild($longDescDoc->doctype);

        //            $iframe = $longDescDoc->getElementsByTagName('iframe');
        //
        //            if (0 == $iframe->length) {
        //                return $longDesc;
        //            }

        $xp  = new DOMXPath($longDescDoc);
        $longDescTag = $xp->query("//iframe[not(contains(@src, 'youtube.com') or
                                contains(@src, 'youtube-nocookie.com'))]");


        foreach ($longDescTag as $t) {
            $t->parentNode->removeChild($t);
        }

        return htmlentities($longDescDoc->saveHTML());
    }

    private function checkSelfieDetailsValidity($title, $subtitle, $date, $lat, $lng, $place)
    {
        if (empty($title) || strlen($title) < __SELFIE_TITLE_MIN_LENGTH__) {
            $this->json(['success' => 0, 'message' => 'title too short... at least ' . __SELFIE_TITLE_MIN_LENGTH__ . ' characters']);
        } elseif (empty($subtitle) || strlen($subtitle) < __SELFIE_DESCR_MIN_LENGTH__) {
            $this->json(['success' => 0, 'message' => 'subtitle too short... at least ' . __SELFIE_DESCR_MIN_LENGTH__ . ' characters']);
        } elseif (strlen($subtitle) > __SELFIE_DESCR_MAX_LENGTH__) {
            $this->json(['success' => 0, 'message' => 'subtitle too long... maximum ' . __SELFIE_DESCR_MAX_LENGTH__ . ' characters']);
        } elseif (empty($date) || !validateDate($date, __DATE_FORMAT_SELFIE__)) {
            $this->json(['success' => 0, 'message' => 'invalid date... (use format ' . __DATE_FORMAT_SELFIE__ . ')']);
        } elseif (empty($lat) || empty($lng)) {
            $this->json(['success' => 0, 'message' => 'no valid coordinates were set']);
        } elseif (empty($place)) {
            $this->json(['success' => 0, 'message' => 'please enter the place where the pictures were taken']);
        }
    }
}
