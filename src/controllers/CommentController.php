<?php

    use Core\Session;

    class CommentController extends Controller
    {

        /**
         * @var CommentController The class instance.
         * @internal
         */
        private static $instance = null;

        /**
         * @var CommentModel The instance of CommentModel.
         */
        protected $model;

        /**
         * Returns a CommentController instance, creating it if it did not exist.
         * @return CommentController
         */
        public static function singleton()
        {
            if (static::$instance === null) {
                $v = __CLASS__;
                static::$instance = new $v();
            }

            return static::$instance;
        }

        protected function __construct() {
            parent::__construct();

            self::$instance = $this;
            $this->model = CommentModel::singleton();
        }

        /**
         * Returns the instance of the model for this controller
         *
         * @return \CommentModel
         */
        public function getModel() {
            return $this->model;
        }

        public function getComments() {
            $selfieHash = $this->getPost('selfie');
            $selfieID   = $this->model->getSelfieID($selfieHash);

            $comments = $this->model->getComments($selfieID);

            $this->setUserAvatarArray($comments);

            $this->json(['comments' => $comments]);
        }

        public function post() {
            $comment = str_replace('&#10;', '<br>', strip_tags($this->getPost('comment'), '<a><b><i>'));
            $user = Session::getUser();
            $selfieHash = $this->getPost('selfie');

            if (empty($selfieHash)) {
                $this->json(['success' => 0, 'message' => 'wrong request. i need the selfie#id']);
            }
            $selfieID = $this->model->getSelfieID($selfieHash);
            // now we have the $selfie['id']. We don't send it because we want to avoid brute-force attacks

            $commentID = $this->model->saveComment($comment, $selfieID, $user['id']);

            if ($commentID == 0) {
                $this->json(['success' => 0, 'message' => 'couldn\'t save...']);
            }

            NotificationController::singleton()->add('comment', $selfieID, $commentID);

            $newComment = $this->model->getCommentByID($commentID);
            $newComment['avatar'] = Session::getUser('avatar');

            $this->json(['comment' => $newComment]);
        }

        public function approveComment($commentID) {

        }

        public function getUsersThatCommented($selfieID) {
            return $this->model->getUsersThatCommented($selfieID);
        }

    }