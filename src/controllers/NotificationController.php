<?php

    use Core\Session;

    class NotificationController extends Controller
    {

        /**
         * @var NotificationController The class instance.
         * @internal
         */
        protected static $instance;

        /**
         * @var int maximum number of notifications shown
         */
        private $notificationsLimit = 10;

        /**
         * @var mixed The model instance associated to NotificationController
         */
        protected $model;

        /**
         * @var CommentController
         */
        protected $comments;

        /**
         * Returns a NotificationController instance, creating it if it did not exist.
         * @return NotificationController
         */
        public static function singleton()
        {
            if (static::$instance === null) {
                $v = __CLASS__;
                static::$instance = new $v();
            }

            return static::$instance;
        }

        protected function __construct() {
            parent::__construct();
            $this->model = NotificationModel::singleton();
            $this->comments = CommentController::singleton();
        }

        /**
         * Returns the instance of the model for this controller
         * @return NotificationModel
         */
        public function getModel() {
            return $this->model;
        }

        /**
         * Alias for NotificationController::addNotification
         * @param string $type (comment|post)
         * @param mixed $selfieID the ID of the selfie
         * @param null $commentID the ID of the comment (or null)
         *
         * @return int The notification ID
         */
        public function add($type, $selfieID, $commentID = null) {
            return $this->addNotification($type, $selfieID, $commentID);
        }

        public function addNotification($type, $selfieID, $commentID = null) {

            $fromUserID = (int)Session::getUser('id');
            $selfie = $this->model->getSelfieByID($selfieID);
            $toUserID = (int)$selfie['user_id']; // selfie owner

            // When type is POST the user owns the selfie so it's a special case
            if ($fromUserID === $toUserID && $type !== 'post') {
                // only notify if there's a reply or a post. These are the notifications TRIGGERED by the selfie owner.
                return true;
            }

            // We're notifying someone else now:
            switch($type) {
                case 'comment':
                    // first, we get all the PEOPLE that have commented on this selfie, NO repeated users
                    $users = $this->comments->getUsersThatCommented($selfieID);
                    foreach ($users as $userID => $commentObj) {
                        if ($userID == $fromUserID || $userID == $toUserID) {
                            // I won't notify myself
                            // I also won't notify the selfie owner, since we're doing that anyways...
                            continue;
                        }

                        if ($userID != $toUserID) {
                            // the comment is NOT from the selfie owner
                            // notify "someone has replied to your comment"
                            // IF there's an UNREAD 'reply' notification to $userID for $selfieID:
                            $unreadReply = $this->model->getUnreadNotification('reply', $selfieID, $userID);

                            if ($unreadReply) {
                                // add "one more person" to the notification
                                $this->model->updateRow('notifications', 'extra', $unreadReply['id'], (int)$unreadReply['extra'] + 1);
                            } else {
                                // we need to notify
                                $this->model->saveNotification('reply', $fromUserID, $userID, $selfieID, $commentID);
                            }
                        }
                    }


                    // COMMENT notifications in your selfies don't pile up, just say "and X other people"
                    $unreadReply = $this->model->getUnreadNotification('comment', $selfieID, $toUserID);
                    if ($unreadReply) {
                        $this->model->updateRow('notifications', 'extra', $unreadReply['id'], (int)$unreadReply['extra'] + 1);
                    } else {
                        $this->model->saveNotification($type, $fromUserID, $toUserID, $selfieID, $commentID);
                    }

                    break;

                case 'post':
                    $this->removeNotification('post', $selfieID);
                    // fromUserID and toUserID are the same in this case, but it doesn't matter
                    $followers = $this->model->getFollowersForUser($fromUserID);
                    foreach ($followers as $follower) {
                        $this->model->saveNotification($type, $fromUserID, $follower['follower_id'], $selfieID);
                    }
                    return '?';
                    break;

                case 'like':
                    return $this->model->saveNotification('like', $fromUserID, $toUserID, $selfieID);
                    break;

                default:
                    break;
            }
        }

        public function removeNotification($type, $selfieID, $commentID = null) {
            $fromUserID = Session::getUser('id');
            $this->model->removeNotification($type, $fromUserID, $selfieID, $commentID);
        }

        public function getNotifications() {
            $userID = Session::getUser('id');
            $limit = $this->getGet('limit') ?: $this->notificationsLimit;
            $counter = (int)$this->model->countUnreadNotifications($userID);
            $notifications = $this->model->getAllNotifications($userID, $limit);

            foreach ($notifications as &$n) {
                $this->setUserAvatar($n);
            }

            $this->json(['notifications' => $notifications, 'unread' => $counter]);
        }

        public function markNotification() {
            $userID = Session::getUser('id');
            $notificationID = $this->getPost('id');
            $unread  = isset($_POST['unread']) ? (bool)(int)$this->getPost('unread') : false;

            $finalState = $this->model->markNotificationState($notificationID, $userID, $unread);
            $this->json(['state' => $finalState, 'unread' => $this->model->countUnreadNotifications($userID)]);
        }
    }
