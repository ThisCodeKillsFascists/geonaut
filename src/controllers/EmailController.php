<?php

    use Core\Session;
    use Detection\MobileDetect;

/**
 * The EmailController is a user-defined controller. Should be in charge of managing the Email.
 */
class EmailController extends Controller {

    /**
     * @var EmailController The class instance.
     * @internal
     */
    private static $instance = null;

    /**
     * @var EmailModel The instance of EmailModel.
     */
    protected $model;

    /**
     * Returns an EmailController instance, creating it if it did not exist.
     *
     * @return EmailController
     */
    public static function singleton()
    {
        if (static::$instance === null) {
            $v = __CLASS__;
            static::$instance = new $v();
        }

        return static::$instance;
    }

    protected function __construct() {
        parent::__construct();
        $this->model = EmailModel::singleton();

        self::$instance = $this;
    }

    /**
     * Returns the instance of the model for this controller
     *
     * @return \EmailModel
     */
    public function getModel() {
        return $this->model;
    }

    public function showEmail() {
        $token = $this->getGet('usertoken');
        $user = $this->model->selectUserByToken($token);
        $emailTemplate = $this->getGet('emailtemplate');

        if (!$user) {
            Session::setAlert(['type' => 'error', 'message' => 'wrong user token']);
            $this->redirect($this->url('Home'));
        } // we have a valid user
        // $nico, $social, $canUnsubscribe, $userToken, $user

        if (!in_array($emailTemplate, ['forgot', 'welcome'])) {
            Session::setAlert(['type' => 'error', 'message' => "that's not a valid type of email"]);
            $this->redirect($this->url('Home'));
        } // it's a valid type of email

        if ($emailTemplate == 'forgot' && empty($user['token'])) {
            // we don't allow 'forgot' emails without a user token:
            Session::setAlert(['type' => 'error', 'message' => "you have already set a new password"]);
            $this->redirect($this->url('Home'));
        } // if it's a reset-password email, the user hasn't used it yet

        // ahoy! all clear and ready to go!
        $this
            ->add('user', $user)
            ->add('userToken', $token)
            ->add('nico', $this->model->getRow('users', 1))
            ->add('randomSelfies', $this->model->getRandomSelfie(3))
            ->show('email/' . $emailTemplate, 'emails/default');
    }

    public function confirmEmail() {
        $token = $this->getGet('usertoken');
        $user = $this->model->selectUserByToken($token);
        if (!$user) {
            $this->redirect($this->url('NOT_FOUND_PAGE'));
        } else {
            $this->model->confirmEmail($user['id']);
            $this->redirect($this->url('Home'), ['confirmed' => 1]);
        }
    }
}
