<?php

use Core\Session;

/**
 * Class Controller acts as a stepping stone (or "intermediate agent") between the user-defined code (<b>/src/controllers/</b>)
 * and the system-defined code (<b>ParentController</b>) that helps to better structure the code. See &#64;example
 * @example
 * <code>
 * // To avoid:
 * FooController->showProducts();
 * // and
 * BarController->showProducts();
 * // to be defined twice (one in each controller) or once (in ParentController, bad code practices)
 * Controller->showProducts();
 * // can be defined and thus accessed from both <b>Foo</b> and <b>Bar</b> Controllers.
 * </code>
 */
class Controller extends \Core\ParentController
{

  /**
   * @var \Controller The class instance.
   * @internal
   */
  private static $instance = null;

  /**
   * @var \Model The instance of Model.
   */
  protected $model;

  /**
   * @var \NotificationController
   */
  protected $notification;

  /**
   * @var string Root path to the /uploads folder
   */
  protected $uploadFolder;

  /**
   * @var string Web path tho the /uploads folder
   */
  public $uploadPath;

  /**
   * @var string Name of the folder that contains uploads: "/uploads"
   */
  protected $uploadFName;

  /**
   * @var string ROOT path to the tmp folder for uploads
   */
  protected $uploadTmp;

  /**
   * @var string Web path to the tmp folder for uploads
   */
  protected $uploadTmpPath;

  /**
   * @var string Name of the folder that contains the uploads BEFORE saving
   */
  protected $uploadTmpName;

  /**
   * @var Mobile_Detect The raw object from the Detect class.
   */
  protected $detect;

  /**
   * @var bool Whether the user is using the website from a mobile phone or not
   */
  protected $isMobile;

  /**
   * @var int The ID of the user, or FALSE if not authenticated
   */
  protected $selfID;

  //    public $googleMapsURL = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCuZL1Ov-rQksFB9gzHz4OvhoogtU-gStg&libraries=places';

  public $mapboxURL = 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js';

  public $mapboxPublicKey = 'pk.eyJ1Ijoibmljb2pvbmVzIiwiYSI6ImNqYjhvejQxNjA1eXkzMm9mcTljanBubGcifQ.WM7Ry1e1-1liibdD7PSNsA';

  /**
   * Returns a Controller instance, creating it if it did not exist.
   * @return Controller
   */
  public static function singleton()
  {
    if (static::$instance === null) {
      static::$instance = new self();
    }

    return static::$instance;
  }

  protected function __construct()
  {
    parent::__construct();

    $user = Session::getUser();

    $this->selfID = !empty($user['id']) ? (int)$user['id'] : 0;

    // Name of the folder for the final uploads
    $this->uploadFName = __UPLOAD_FOLDER__;

    // WEB  path to the images folder, both final (FName) and temporary (TmpName)
    $this->uploadPath = $this->asset(__UPLOAD_PATH__ . $this->uploadFName, TRUE);
    // ROOT path to the images folder
    $this->uploadFolder = __ROOT__ . '/public/' . __UPLOAD_PATH__;

    if (Session::isAuthenticated()) {
      // Name of the folder for the uploads during the process
      $this->uploadTmpName = '/on_transit/' . md5($user['id']);

      // ROOT path to the temporary uploads folder
      $this->uploadTmp = $this->uploadFolder . $this->uploadTmpName;
      // WEB  path to the temporary uploads folder
      $this->uploadTmpPath = $this->asset('images/pictures' . $this->uploadTmpName, TRUE);
      if (!file_exists($this->uploadTmp)) {
        mkdir($this->uploadTmp);
      }
    }

    //        if ($user['view_mode'] == 'flat') {
    //            $this->isMobile = true;
    //        } elseif ($user['view_mode'] == 'monocle') {
    //            $this->isMobile = false;
    //        } else {
    //            // auto or guest:
    //            // Detecting the user-agent
    //            $this->detect = new Mobile_Detect();
    //            $this->isMobile = $this->detect->isMobile() || $this->detect->isTablet();
    //        }

    $this->detect = new Mobile_Detect();
    $this->isMobile = ($this->detect->isMobile() || $this->detect->isTablet());
    if (!defined('_IS_MOBILE_')) {
      define('_IS_MOBILE_', $this->isMobile);
    }

    $this->model = Model::singleton();

    self::$instance = $this;
  }

  /**
   * Checks for the existance and ownership of a given selfie. Returns a FLASH alert if the selfie doesn't exist
   * or if the user doesn't own it. returns a TRUE boolean if all is OK
   *
   * @param $userID
   * @param $selfie
   *
   * @return bool
   */
  protected function checkSelfieValidity($userID, $selfie)
  {
    $selfieHash = !empty($selfie['hash']) ? $selfie['hash'] : FALSE;
    if (!$selfieHash) {
      // Selfie does not exist
      $this->json(['success' => 0, 'message' => 'Selfie doesn\'t exist!']);
    } elseif ($selfie['user_id'] != $userID) {
      // Selfie is not from this user
      $this->json(['success' => 0, 'message' => 'Selfie #' . $selfieHash . ' is not yours!']);
    } else {
      // everything OK!
    }
    return true;
  }

  public function getActiveHash($possibleHash, $redirect = FALSE)
  {
    // first, we need to get the "active" hash and see if it matches $selfieHash. If not, we must redirect.
    $activeHash = $this->model->getActiveHash($possibleHash);

    if ($activeHash != $possibleHash && $redirect) {
      $this->redirect($this->url('Selfie', [':selfie' => $activeHash]));
    } else {
      // all well, we have the valid "active" hash for this selfie.
      return $activeHash;
    }
  }

  public function getOriginalHash($hash)
  {
    // we get the $hash tagged with "original=1" from the set of $hash that point to our selfie
    $originalHash = $this->model->getOriginalHash($hash);

    return $originalHash;
  }

  protected function sanitiseURL($selfieURL)
  {
    $selfieURL = preg_replace(['/[ \-_]+/', '/[^a-zA-Z0-9\-]+/'], ['-', ''], $selfieURL);
    return $selfieURL;
  }

  /**
   * Sets the URL to the profile pic under the new key <pre>avatar</pre>
   * @param $user array the user containing profile_pic and profile_pic_default keys
   * @param $large boolean whether to return the original image me.jpg or mini_me.jpg
   */
  public function setUserAvatar(&$user, $large = false)
  {
    // setting profile picture properly
    if (!empty($user['profile_pic'])) {
      $user['avatar'] = $this->asset('images/pictures/uploads/' . $user['profile_pic'] . ($large ? '/' : '/mini_') . __ME__, TRUE);
    } else {
      $user['avatar'] = $this->asset('images/icons/default_profile/' . $user['profile_pic_default'], TRUE);
    }
  }

  /**
   * See \Controller::setUserAvatar for more info.
   * Make sure every item has the key profile_pic OR profile_pic_default (or you'll get an error)
   * @param $user
   * @param bool|false $large
   */
  public function setUserAvatarArray(&$user, $large = false)
  {
    foreach ($user as &$u) {
      $this->setUserAvatar($u, $large);
    }
  }

  protected function uploadPath($selfieHash)
  {
    return $this->uploadPath . '/' . $selfieHash;
  }

  protected function uploadFolder($selfieHash)
  {
    return $this->uploadFolder . '/' . $this->uploadFName . '/' . $selfieHash;
  }
}
