<?php

/**
 * Used as a user-defined controller for errors and Error pages.
 */
class ErrorController extends \Core\ErrorController {

    /**
     * @var ErrorController The class instance.
     * @internal
     */
    private static $instance = null;

    /**
     * @var mixed The model instance associated to ErrorController
     */
    protected $model;

    /**
     * Returns a ErrorController instance, creating it if it did not exist.
     * @return ErrorController
     */
    public static function singleton()
    {
        if (static::$instance === null) {
            $v = __CLASS__;
            static::$instance = new $v();
        }

        return static::$instance;
    }

    /**
     * This method is overriding \Core\ErrorController:showNotFoundPage
     */
    public function showNotFoundPage() {
        $this->header(404);

        if ($asset = $this->getGet('asset', FALSE)) {
            if (in_array($asset, ['css', 'js', 'fonts'])) {
                die("/* the asset " . $_SERVER['REQUEST_URI'] . " could not be found... */");
            } else {
                die(); // it's an asset, there should be no output!
            }
        } else {
            $this
                ->setTitle("oh, no! the infamous 404")
                ->addHeader()
                ->addFooter()
                ->show('home/empty');
//                ->show('error/404_not_found');
        }
    }

    public function showNotFoundSelfie() {
        $this->header(404);

        $selfie = [
            'title' => 'Mmmm... not found',
            'short_desc' => "You are looking for a selfie that does not exist. Sorry about that.",
            'me_src' => $this->asset('images/icons/generic/me.png', true),
            'lc_src' => $this->asset('images/icons/generic/lc.jpg', true),
            'place' => 'Server, Planet Earth',
            'selfie_date' => time(),
            'long_desc' => ''
        ];

        $this
            ->addHeader()
            ->addFooter()
            ->add('selfie', $selfie)
            ->show('home/empty');
        }
}