<?php

use Core\Session;

class ApiController extends Controller
{

    /**
     * @var ApiController The class instance.
     * @internal
     */
    private static $instance = null;

    /**
     * @var ApiModel The instance of ApiModel.
     */
    protected $model;

    /**
     * @var SelfieModel The instance of SelfieModel
     */
    protected $selfieModel;

    /**
     * Returns a ApiController instance, creating it if it did not exist.
     * @return ApiController
     */
    public static function singleton()
    {
        if (static::$instance === null) {
            $v = __CLASS__;
            static::$instance = new $v();
        }

        return static::$instance;
    }

    protected function __construct()
    {
        parent::__construct();

        self::$instance = $this;
        $this->model = ApiModel::singleton();
        $this->selfieModel = SelfieModel::singleton();
    }

    /**
     * Returns the instance of the model for this controller
     *
     * @return \ApiModel
     */
    public function getModel()
    {
        return $this->model;
    }


    public function getSelfies()
    {
        $section = $this->getPost('s');
        $start = (int)$this->getPost('start', 0);
        $limit = (int)$this->getPost('limit', 10);

        switch ($section) {

            case 'home':
                $selfies = $this->model->getAllSelfies($this->selfID, 0, $start, $limit);
                return $this->json([
                    'selfies' => $selfies,
                    'title' => null,
                    'more' => (bool)($limit == count($selfies))
                ]);
                break;

            case 'discover':
                $selfies = $this->model->getFollowedSelfies($this->selfID, $start, $limit);
                return $this->json([
                    'selfies' => $selfies,
                    'title' => 'discover',
                    'more' => (bool)($limit == count($selfies))
                ]);
                break;

            case 'loves':
                $selfies = $this->model->getLovedSelfies($this->selfID, $start, $limit);
                return $this->json([
                    'selfies' => $selfies,
                    'title' => 'loves',
                    'more' => (bool)($limit == count($selfies))
                ]);
                break;

            case 'users':
                $selfies = $this->model->getLastSelfieUsers($this->selfID, $start, $limit);
                return $this->json([
                    'selfies' => $selfies,
                    'title' => 'users',
                    'more' => (bool)($limit == count($selfies))
                ]);
                break;

            case 'user':
                $username = $this->getPost('username');
                $userID = $this->model->convertUserKey(false, $username);
                if (!$userID) {
                    $this->header(404);
                    $this->json(['success' => 0]);
                }

                if ($this->getPost('info', false)) {
                    $userInfo = $this->model->getUserInfo($userID, $this->selfID);
                    $userInfo['ifollow'] = $this->selfID ? $this->model->follows($this->selfID, $userID) : 0;
                    $this->setUserAvatar($userInfo, true);
                    escape_htmlentities($userInfo['profile']);
                } else {
                    // not requesting the information
                    $userInfo = [];
                }

                $selfies = $this->model->getAllSelfies($this->selfID, $userID, $start, $limit);
                //                    if (empty($selfies)) {
                //                        $this->header(204);
                //                    }

                return $this->json([
                    'selfies' => $selfies ?: [],
                    'user' => $userInfo,
                    'title' => $username,
                    'more' => (int)($limit == count($selfies))
                ]);
                break;

            case 'one':
                $hash = $this->getOriginalHash($this->getPost('hash'));
                $selfie = $this->model->getSelfie($hash, $this->selfID, 'long description too');

                if (!$selfie) {
                    $this->header(404);
                    $this->json(['success' => 0, 'selfie' => false]);
                } // else:
                return $this->json([
                    'selfie' => $selfie,
                    'title' => $selfie['title'] . ' by ' . $selfie['username']
                ]);
                break;

            case 'about':
                $lastSelfie = $this->model->getAllSelfies($this->selfID, 1 /* my user:id */, 0, 1);
                return $this->json([
                    'selfies' => $lastSelfie,
                    'title' => 'About',
                    'type' => 'home',
                    'more' => 0
                ]);
                break;

            case 'login':
                if (!empty($_COOKIE['llut'])) {
                    $user = EmailModel::singleton()->selectUserByToken($_COOKIE['llut']);
                    $this->setUserAvatar($user);
                    $avatar =   $user['avatar'];
                    $username = !empty($user['username']) ? $user['username'] : false;
                }
                // if the $_COOKIE was invalid or empty, we set the default
                if (empty($username)) {
                    $handle = scandir(__ROOT__ . '/public/images/icons/default_profile');
                    $avatar = $this->asset('images/icons/default_profile/' . $handle[mt_rand(3, count($handle) - 1)], true);
                    $username = '';
                }

                return $this->json([
                    'avatar' => $avatar,
                    'username' => $username,
                    'title' => 'login'
                ]);
                break;

            case 'search':
                $search = $this->getPost('search');
                $type   = $this->getPost('type') ?: 'all';

                $this->search($type, $search, $start, $limit);
                break;

            default:
                $this->header(404);
                return $this->json(['success' => 0]);
        }
    }

    public function getAdminData()
    {
        $section = $this->getPost('s');
        switch ($section) {

            case 'new':

                $selfie = LoginSelfieController::singleton()->createSelfieDraft();

                $this->json(['redirect' => $this->url('SelfieEdit', [':selfie' => $selfie['hash']])]);

                break;

            case 'edit';

                $newSelfieTitle = 'new post';

                $selfieHash = $this->getPost('selfie');
                $selfie = $this->model->getSelfieByHash($selfieHash);
                $selfieHashes = $this->model->getSelfieHash($selfie['id'], 1, 1);

                $this->checkSelfieValidity($this->selfID, $selfie);

                $selfie['title'] = unescape($selfie['title']);
                $selfie['short_desc'] = unescape($selfie['short_desc']);
                $selfie['long_desc'] = html_entity_decode($selfie['long_desc']);
                $selfie['place'] = unescape($selfie['selfie_place']);

                $selfieArray = [];

                if (!empty($selfie)) {
                    $selfieArray = array_merge($selfie, [
                        'subtitle' => $selfie['short_desc'] ?: '',
                        'date' => $selfie['selfie_date'] ?: '',
                        'lat' => $selfie['lat'] ?: 0,
                        'lng' => $selfie['lng'] ?: 0,
                        'place' => $selfie['selfie_place'] ?: '',
                        'description' => $selfie['long_desc'] ?: '',
                        'url' => ($selfieHashes['active'] === $selfieHashes['original']) ? '' : $selfieHashes['active'],
                        'hash' => $selfieHashes['original'],
                        'visible' => $selfie['visible'] == 1,
                        'draft' => $selfie['draft'] == 1
                    ]);
                } else {
                    $this->json(['success' => 0, 'message' => 'unknown error... really']);
                }

                $constraints = [
                    'titleML'    => __SELFIE_TITLE_MIN_LENGTH__,
                    'titleXL'    => __SELFIE_TITLE_MAX_LENGTH__,
                    'subtitleML' => __SELFIE_DESCR_MIN_LENGTH__,
                    'subtitleXL' => __SELFIE_DESCR_MAX_LENGTH__,
                ];

                $attachmentFiles = array_map(function ($file) use ($selfieHash) {
                    return $this->uploadPath($selfieHash) . '/' . $file;
                }, getAttachmentFiles($this->uploadFolder($selfieHash)));

                return $this->json([
                    'title' => $selfie['title'] ?: $newSelfieTitle,
                    'selfie' => $selfieArray,
                    'selfie_full' => $selfie,
                    'images' => $this->createImagesDraft($selfie),
                    'attachments' => $attachmentFiles,
                    'constraints' => $constraints
                ]);
                break;

            case 'home':
                $lastSelfies = $this->model->getAllSelfies($this->selfID, $this->selfID, 0, 15);
                $followingUsers = $this->model->followingUsers($this->selfID);
                $mapSelfies = $this->model->getSelfiesForMap($this->selfID);
                $unpublished = $this->model->getSelfieDraft($this->selfID);

                return $this->json([
                    'following' => $followingUsers,
                    'last' => $lastSelfies,
                    'unpublished' => $unpublished,
                    'mapSelfies' => $mapSelfies,
                    'mapSelfiesRange' => count($mapSelfies)
                        ? [
                            strtotime($mapSelfies[0]['selfie_date']) * 1000,
                            strtotime(end($mapSelfies)['selfie_date']) * 1000
                        ]
                        : null,
                    'title' => 'your selfies'
                ]);
                break;

            case 'settings':
                $settings = Session::getUser();
                $user = $this->model->getRow('users', $settings['id']);
                return $this->json([
                    'title' => 'settings',
                    'settings' => [
                        'name' => unescape($settings['name']),
                        'username' => $settings['username'],
                        'email' => $settings['email'],
                        'bell_position' => $settings['bell_position'],
                        'theme' => $settings['theme'],
                        'short_desc' => $user['short_desc'],
                        'gender' => $user['gender'],
                        'profile_pic' => $user['profile_pic'],
                    ],
                ]);

            default:
                $this->header(404);
                return $this->json(['success' => 0]);
        }
    }

    public function follow()
    {
        $userID = Session::getUser('id');
        $username = $this->getPost('username');

        $followed = $this->model->convertUserKey(false, $username);

        // A user can't follow its own person
        if ($userID == $followed) {
            $this->json(['following' => 0]);
        }

        if (!$followed) {
            $this->json(['following' => 0]);
        }

        if ($this->model->follows($userID, $followed)) {
            $this->model->unfollow($userID, $followed);
            $followingAfter = 0;
        } else {
            $this->model->follow($userID, $followed);
            $followingAfter = 1;
        }

        $this->json(['following' => $followingAfter]);
    }

    public function love()
    {
        $userID = Session::getUser('id');

        $hash = $_POST['selfie'];
        $selfieID = $this->model->getSelfieID($hash);

        if ($this->model->loves($userID, $selfieID)) {
            $this->model->unlove($userID, $selfieID);
            NotificationController::singleton()->removeNotification('like', $selfieID);
            $lovingAfter = 0;
        } else {
            $this->model->love($userID, $selfieID);
            NotificationController::singleton()->addNotification('like', $selfieID);
            $lovingAfter = 1;
        }

        $loves = $this->model->getSelfie($hash, $this->selfID);

        $this->json(['loving' => $lovingAfter, 'hash' => $hash, 'loves' => $loves['loves']]);
    }

    public function getSelfiesHistory()
    {
        $username = $this->getPost('username');
        $userID = $this->model->convertUserKey(false, $username);
        if (!$userID) {
            $this->header(404);
            $this->json(['success' => 0]);
        }
        $mapSelfies = $this->model->getSelfiesForMap($userID);

        $this->json([
            'mapSelfies' => $mapSelfies,
            'mapSelfiesRange' => [
                strtotime($mapSelfies[0]['selfie_date']) * 1000,
                strtotime(end($mapSelfies)['selfie_date']) * 1000
            ],
        ]);
    }

    public function getSelfieNext()
    {
        $hash = $_GET['selfie'];

        $selfie = $this->model->getRow('selfies_with_user', $hash, 'hash');

        $neighbours = $this->model->getSelfieNext2($selfie, $this->selfID);

        $this->json($neighbours);
    }

    /**
     * Performs a search and returns results (if &gt; 1) or redirects (if == 1)
     *
     * @param $search
     * @param $start
     * @param $limit
     *
     * @return string
     */
    private function search($type, $search, $start, $limit)
    {
        $returnCount = isset($_POST['return']);

        if (empty($search)) {
            $searchResults = [];
        } elseif ($type !== 'all') {
            $searchResults = $this->model->search($type, $search, $start, $limit);
        } else { // type == 'all' -> searching generally
            $searchResults = $this->model->searchAll($search);
        }

        $resultCount = count($searchResults);

        if ($resultCount == 0) {
            $this->json([
                'success' => 1,
                'message' => 'no ' . ($type !== 'all' ? $type : '') . ' matches for "' . $search . '"',
                'more' => 0,
                'selfies' => []
            ]);
        } elseif ($resultCount == 1 && $type == 'all') {
            $result = $searchResults[0];
            // is it a user or a selfie?
            if ($result['hash'] == '0') {
                // it's a user
                $this->json(['redirect' => $this->url('UserSelfies', [':username' => $result['username']])]);
            } else {
                $this->json(['redirect' => $this->url('Selfie', [':selfie' => $result['active_hash']])]);
            }
        } else {
            // there are results!
            if ($returnCount) {
                $this->json([
                    'redirect' => $this->url('Search', [':search' => urlencode($search)]),
                    'searchType' => $searchResults[0]['type']
                ]);
            } else {
                $final = [];
                foreach ($searchResults as $result) {
                    if ($result['hash'] == '0') {
                        $final[] = $this->model->getUserAndLastSelfie($result['username'], $this->selfID);
                    } else {
                        $selfie = $this->model->getSelfie($result['hash'], $this->selfID);
                        $final[] = $selfie;
                    }
                }
                return $this->json([
                    'title' => 'search results for "' . $search . '"',
                    'selfies' => $final,
                    'more' => (int)(count($final) >= $limit)
                ]);
            }
        }
    }

    public function checkUsername()
    {
        $username = $this->getPost('username');
        if (strlen($username) < 4) {
            $this->json(['success' => 0, 'message' => 'username too short!']);
        }

        $used = $this->model->getRow('users', $username, 'username');

        if (count($used) && ($this->selfID == 0 || $this->selfID && $used['id'] != $this->selfID)) {
            $this->json(['success' => 0, 'message' => 'username taken, choose a different one']);
        } else {
            $this->json(['message' => 'good choice!']);
        }
    }

    public function checkEmail()
    {
        $email = $this->getPost('email');
        $valid = preg_match(
            '/^[A-Za-z0-9][A-Za-z0-9._%+-]{0,63}@(?:[A-Za-z0-9](?:[A-Za-z0-9-]{0,62}[A-Za-z0-9])?\.){1,8}[A-Za-z]{2,63}$/',
            $email
        );
        if (!$valid) {
            $this->json(['success' => 0, 'message' => 'not a valid email, sorry']);
        }

        $used = $this->model->getRow('users', $email, 'email');

        // if it's used and: either the user is not logged in or it's used by someone else:
        if (count($used) && ($this->selfID == 0 || $this->selfID && $used['id'] != $this->selfID)) {
            $this->json(['success' => 0, 'message' => 'email is in use...']);
        } else {
            $this->json(['message' => 'cool!']);
        }
    }

    public function checkURL()
    {
        $selfieURL  = $this->sanitiseURL($this->getPost('url'));
        $selfieHash = $this->getPost('hash') ?: false;

        if ($selfieURL == 'custom-url') {
            $this->json(['success' => -1, 'message' => 'That URL is reserved and cannot be used']);
        }

        if (strlen($selfieURL) < 5) {
            $this->json(['success' => 0, 'message' => 'the URL must be at least 5 characters long']);
        }

        $selfieURLID  = $this->model->getSelfieID($selfieURL);
        if ($selfieURLID == 0) {
            // valid, it's not taken!
            $this->json(['message' => 'URL available']);
            // we have to see that if the hash exists, is for the same selfie
        } elseif ($selfieHash) {
            $selfieHashID = $this->model->getSelfieID($selfieHash);
            // if they are for the same selfie, it's also OK
            if ($selfieHashID == $selfieURLID) {
                $this->json(['message' => 'URL available']);
            } else {
                // they are both != 0 and different
                $this->json(['success' => 0, 'message' => 'sorry, taken. use a different one']);
            }
        } else {
            // our $selfieURL is taken, and $selfieHash doesn't exist (new selfie, not editing), so not valid
            $this->json(['success' => 0, 'message' => 'sorry, taken. use a different one']);
        }
    }


    public function createImagesDraft($selfie)
    {
        $uploadPath = $this->uploadPath($selfie['hash']);

        $me = __ME__;
        $lc = __LC__;

        $imageMe = new stdClass();
        $imageLc = new stdClass();


        if (!empty($selfie['me_color'])) {
            $imageMe = [
                'finalName' => $me,
                'img'       => $uploadPath . '/' . $me,
                'mini'      => $uploadPath . '/' . 'mini_' . $me,
                'color'     => ['color' => $selfie['me_color'], 'brightness' => $selfie['me_brightness']],
                'resized'   => 1
            ];
            $metadataMe = json_decode($selfie['meta_me'], true);
            if (!empty($metadataMe['GPS'])) {
                $gpsMe = ($metadataMe ? LoginSelfieController::singleton()->getImageLocationFromGPS($metadataMe['GPS']) : null) ?: null;
                $imageMe = array_merge($imageMe, [
                    'gps'       => $gpsMe,
                    'date'      => !empty($metadataMe['EXIF']) ? @date('Y-m-d', strtotime($metadataMe['EXIF']['DateTimeOriginal'])) : 0,
                ]);
            }
        }

        if (!empty($selfie['lc_color'])) {
            $imageLc = [
                'finalName' => $lc,
                'img'       => $uploadPath . '/' . $lc,
                'mini'      => $uploadPath . '/' . 'mini_' . $lc,
                'color'     => ['color' => $selfie['lc_color'], 'brightness' => $selfie['lc_brightness']],
                'resized'   => 1
            ];
            $metadataLc = json_decode($selfie['meta_lc'], true);
            if (!empty($metadataLc['GPS'])) {
                $gpsLc = ($metadataLc ? LoginSelfieController::singleton()->getImageLocationFromGPS($metadataLc['GPS']) : null) ?: null;
                $imageLc = array_merge($imageLc, [
                    'gps'       => $gpsLc,
                    'date'      => !empty($metadataLc['EXIF']) ? @date('Y-m-d', strtotime($metadataLc['EXIF']['DateTimeOriginal'])) : 0,
                ]);
            }
        }


        return [
            'me' => $imageMe,
            'lc' => $imageLc
        ];
    }
}
