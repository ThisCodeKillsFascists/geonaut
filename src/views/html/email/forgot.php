<?php
  $__URL__ = __SITE_URL__ . $this->url('FrontendResetPassword', [':token' => $user['token'], ':action' => 'reset', ':email' => $user['email']]);
?>
<div class="oldwebkit" style="max-width: 570px;">
    <table width="570" border="0" cellpadding="0" cellspacing="18" class="vb-container fullpad" bgcolor="#595959" style="border-collapse: separate;border-spacing: 18px;padding-left: 0;padding-right: 0;width: 100%;max-width: 570px;background-color: #595959;">
        <tbody>
        <tr>
            <td align="left" class="long-text links-color" style="text-align: left; font-size: 13px; font-family: Arial, Helvetica, sans-serif; color: #eeece1;">
                <h3 style="text-align: center;" data-mce-style="text-align: center;">
                    hello <?= $user['name']?></h3>
                <p style="margin: 1em 0px;text-align: center;" data-mce-style="text-align: center;">
                    it happens. we all forget passwords.</p>
                <p style="margin: 1em 0px;margin-bottom: 0px;text-align: center;"
                   data-mce-style="text-align: center;">just click on the link below to reset it:</p>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<!--[if (gte mso 9)|(lte ie 8)]>
</td>
</tr>
</table>
<![endif]-->
</td>
</tr>
</tbody>
</table>
<table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#595959"
       style="background-color: #595959;" id="ko_buttonBlock_11">
    <tbody>
    <tr>
        <td class="vb-outer" align="center" valign="top" bgcolor="#595959"
            style="padding-left: 9px;padding-right: 9px;background-color: #595959;">

            <!--[if (gte mso 9)|(lte ie 8)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="570">
                <tr>
                    <td align="center" valign="top">
            <![endif]-->
            <div class="oldwebkit" style="max-width: 570px;">
                <table width="570" border="0" cellpadding="0" cellspacing="18" class="vb-container fullpad"
                       bgcolor="#595959" style="border-collapse: separate;border-spacing: 18px;padding-left: 0;padding-right: 0;width: 100%;max-width: 570px;background-color: #595959;">
                    <tbody>
                    <tr>
                        <td valign="top" bgcolor="#595959" align="center" style="background-color: #595959;">

                            <table cellpadding="0" border="0" align="center" cellspacing="0" class="mobile-full">
                                <tbody>
                                <tr>
                                    <td width="auto" valign="middle" bgcolor="#bfbfbf" align="center" height="50"
                                        style="font-size: 22px; font-family: Arial, Helvetica, sans-serif; color: #3f3f3f; font-weight: normal; padding-left: 14px; padding-right: 14px; background-color: #bfbfbf; border-radius: 4px;">
                                        <a href="<?php echo $__URL__ ?>">reset password</a><br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!--[if (gte mso 9)|(lte ie 8)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>
<table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#595959" style="background-color: #595959;" id="ko_textBlock_12">
    <tbody>
    <tr>
        <td class="vb-outer" align="center" valign="top" bgcolor="#595959" style="padding-left: 9px;padding-right: 9px;background-color: #595959;">

            <!--[if (gte mso 9)|(lte ie 8)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="570">
                <tr>
                    <td align="center" valign="top">
            <![endif]-->
            <div class="oldwebkit" style="max-width: 570px;">
                <table width="570" border="0" cellpadding="0" cellspacing="18" class="vb-container fullpad" bgcolor="#595959" style="border-collapse: separate;border-spacing: 18px;padding-left: 0;padding-right: 0;width: 100%;max-width: 570px;background-color: #595959;">
                    <tbody>
                    <tr>
                        <td align="left" class="long-text links-color" style="text-align: left; font-size: 13px; font-family: Arial, Helvetica, sans-serif; color: #eeece1;">
                            <p style="margin: 1em 0px;margin-bottom: 0px;margin-top: 0px;text-align: center;" data-mce-style="text-align: center;">or copy paste this link into your browser<br>
                                <a href="<?= $__URL__ ?>" style="color: #c4bd97;text-decoration: underline;"><?= $__URL__ ?></a>
                                <br>
                            </p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!--[if (gte mso 9)|(lte ie 8)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>
</div>
