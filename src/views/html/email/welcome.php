<table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#595959" style="background-color: #595959;" id="ko_textBlock_5"><tbody><tr><td class="vb-outer" align="center" valign="top" bgcolor="#595959" style="padding-left: 9px;padding-right: 9px;background-color: #595959;">

                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" border="0" cellpadding="0" cellspacing="18" class="vb-container fullpad" bgcolor="#595959" style="border-collapse: separate;border-spacing: 18px;padding-left: 0;padding-right: 0;width: 100%;max-width: 570px;background-color: #595959;"><tbody><tr><td align="left" class="long-text links-color" style="text-align: left; font-size: 13px; font-family: Arial, Helvetica, sans-serif; color: #eeece1;">
                                <p style="text-align: center; color: #ccc !important; font-size: 40px" data-mce-style="text-align: center;"
                                    >hello, <?= $user['name'] ?></p>
                                <p style="margin: 1em 0px; color: #ccc !important">welcome to <a href="<?= __SITE_URL__?>" style="color:white !important" target="_blank"><?= __SITE_NAME__?></a>, a simple blog for travelers (<strong>geo:</strong> earth, <strong>naut</strong>: explorer).</p>
                                <p style="margin: 1em 0px; color: #ccc !important">your username is <strong><?= $user['username'] ?></strong>.
                                    you can access your profile on 
                                    <a href="<?php echo __SITE_URL__, $this->url('FrontendUserSelfies', [':username' => $user['username']])?>"
                                       target="_blank" style="color: #c4bd97;text-decoration: underline;">
                                        <?php echo __SITE_URL__, $this->url('FrontendUserSelfies', [':username' => $user['username']])?></a>,
                                    where all your posts will appear in reverse chronological order.</p><p style="margin: 1em 0px; color: #ccc !important">
                                    if you have any questions, feedback or just want to say hi, drop me a line at
                                    <a href="mailto:<?= __ADMIN_EMAIL__?>" style="color:white !important"><?= __ADMIN_EMAIL__?></a></p><p style="margin: 1em 0px;margin-bottom: 0px;">thank you for joining!</p></td>
                        </tr></tbody></table></div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
            </td>
        </tr></tbody></table><table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#595959" style="background-color: #595959;" id="ko_buttonBlock_11"><tbody><tr><td class="vb-outer" align="center" valign="top" bgcolor="#595959" style="padding-left: 9px;padding-right: 9px;background-color: #595959;">

                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" border="0" cellpadding="0" cellspacing="18" class="vb-container fullpad" bgcolor="#595959" style="border-collapse: separate;border-spacing: 18px;padding-left: 0;padding-right: 0;width: 100%;max-width: 570px;background-color: #595959;"><tbody><tr><td valign="top" bgcolor="#595959" align="center" style="background-color: #595959;">

                                <table cellpadding="0" border="0" align="center" cellspacing="0" class="mobile-full"><tbody><tr><td width="auto" valign="middle" bgcolor="#bfbfbf" align="center" height="50" style="font-size: 22px; font-family: Arial, Helvetica, sans-serif; color: #3f3f3f; font-weight: normal; padding-left: 14px; padding-right: 14px; background-color: #bfbfbf; border-radius: 4px;">
                                            <a style="color:#444" href="<?php echo __SITE_URL__, $this->url('FrontendEmailConfirm', [':usertoken' => $userToken])?>">confirm your email address</a><br></td>
                                    </tr></tbody></table></td>
                        </tr></tbody></table></div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
            </td>
        </tr></tbody></table><table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#595959" style="background-color: #595959;" id="ko_textBlock_12"><tbody><tr><td class="vb-outer" align="center" valign="top" bgcolor="#595959" style="padding-left: 9px;padding-right: 9px;background-color: #595959;">

                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" border="0" cellpadding="0" cellspacing="18" class="vb-container fullpad" bgcolor="#595959" style="border-collapse: separate;border-spacing: 18px;padding-left: 0;padding-right: 0;width: 100%;max-width: 570px;background-color: #595959;"><tbody><tr><td align="left" class="long-text links-color" style="text-align: left; font-size: 13px; font-family: Arial, Helvetica, sans-serif; color: #eeece1;"><p style="margin: 1em 0px;margin-bottom: 0px;margin-top: 0px;text-align: center;" data-mce-style="text-align: center;">or copy paste this link into your browser<br><a href="<?php echo __SITE_URL__, $this->url('FrontendEmailConfirm', [':usertoken' => $userToken])?>" style="color: #c4bd97;text-decoration: underline;"><?php echo __SITE_URL__, $this->url('FrontendEmailConfirm', [':usertoken' => $userToken])?></a><br></p></td>
                        </tr></tbody></table></div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
            </td>
        </tr></tbody></table><table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#eeece1" style="background-color: #eeece1;" id="ko_titleBlock_6"><tbody><tr><td class="vb-outer" align="center" valign="top" bgcolor="#eeece1" style="padding-left: 9px;padding-right: 9px;background-color: #eeece1;">

                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" border="0" cellpadding="0" cellspacing="9" class="vb-container halfpad" bgcolor="#eeece1" style="border-collapse: separate;border-spacing: 9px;padding-left: 9px;padding-right: 9px;width: 100%;max-width: 570px;background-color: #eeece1;"><tbody><tr><td bgcolor="#eeece1" align="center" style="background-color: #eeece1; font-size: 22px; font-family: Arial, Helvetica, sans-serif; color: #262626; text-align: center;">
                                <span><br>what's next?<br></span>
                            </td>
                        </tr></tbody></table></div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
            </td>
        </tr></tbody></table><table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#eeece1" style="background-color: #eeece1;" id="ko_doubleArticleBlock_7"><tbody><tr><td class="vb-outer" align="center" valign="top" bgcolor="#eeece1" style="padding-left: 9px;padding-right: 9px;background-color: #eeece1;">

                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" border="0" cellpadding="0" cellspacing="9" class="vb-row fullpad" bgcolor="#eeece1" style="border-collapse: separate;border-spacing: 9px;width: 100%;max-width: 570px;background-color: #eeece1;"><tbody><tr><td align="center" valign="top" style="font-size: 0;">

                                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="552"><tr><![endif]-->
                                <!--[if (gte mso 9)|(lte ie 8)]><td align="left" valign="top" width="276"><![endif]-->
                                <div style="display: inline-block; max-width: 276px; vertical-align: top; width: 100%;" class="mobile-full">

                                    <table class="vb-content" border="0" cellspacing="9" cellpadding="0" width="276" style="border-collapse: separate;width: 100%;" align="left"><tbody><tr><td width="100%" align="left" class="links-color" style="padding-bottom: 9px;">

                                                <img border="0" hspace="0" vspace="0" width="258" height="100" class="mobile-full" alt="" style="border: 0px;display: block;vertical-align: top;width: 100%;height: auto;" src="<?= __SITE_URL__, $this->asset('images/icons/generic/earth.png') ?>"></td>
                                        </tr><tr><td style="font-size: 18px; font-family: Arial, Helvetica, sans-serif; color: #eeece1; text-align: left;">
                                                <span style="color: #eeece1;">add content</span>
                                            </td>
                                        </tr><tr><td align="left" class="long-text links-color" style="text-align: left; font-size: 13px; font-family: Arial, Helvetica, sans-serif; color: #262626;"><p style="margin: 1em 0px;margin-top: 0px;">with a profile like yours, it would be a shame to abandon it...<br></p><p style="margin: 1em 0px;margin-bottom: 0px;">next steps are creating a new "selfie" and sharing it with the world!</p></td>
                                        </tr><tr><td valign="top">
                                                <table cellpadding="0" border="0" align="left" cellspacing="0" class="mobile-full" style="padding-top: 4px;"><tbody><tr><td width="auto" valign="middle" bgcolor="#bfbfbf" align="center" height="26" style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; text-align: center; color: #3f3f3f; font-weight: normal; padding-left: 18px; padding-right: 18px; background-color: #bfbfbf; border-radius: 4px;">
                                                            <a style="text-decoration: none; color: #3f3f3f; font-weight: normal;" target="_new" href="<?= __SITE_URL__, $this->url('FrontendSelfieAddNew') ?>">add new content<br></a>
                                                        </td>
                                                    </tr></tbody></table></td>
                                        </tr></tbody></table></div><!--[if (gte mso 9)|(lte ie 8)]></td>
                                <td align="left" valign="top" width="276">
                                <![endif]--><div style="display: inline-block; max-width: 276px; vertical-align: top; width: 100%;" class="mobile-full">

                                    <table class="vb-content" border="0" cellspacing="9" cellpadding="0" width="276" style="border-collapse: separate;width: 100%;" align="right"><tbody><tr><td width="100%" valign="top" align="left" class="links-color" style="padding-bottom: 9px;">

                                                <img border="0" hspace="0" vspace="0" width="258" height="100" class="mobile-full" alt="" style="border: 0px;display: block;vertical-align: top;width: 100%;height: auto;" src="<?= __SITE_URL__, $this->asset('images/icons/generic/love-help.png') ?>"></td>
                                        </tr><tr><td style="font-size: 18px; font-family: Arial, Helvetica, sans-serif; color: #eeece1; text-align: left;">
                                                <span style="color: #eeece1;">follow people<br></span>
                                            </td>
                                        </tr><tr><td align="left" class="long-text links-color" style="text-align: left; font-size: 13px; font-family: Arial, Helvetica, sans-serif; color: #262626;"><p style="margin: 1em 0px;margin-top: 0px;">what better way to connect than showing support and empathy for other people's stories!</p><p style="margin: 1em 0px;margin-bottom: 0px;">click on the heart to like a post, or follow the user altogether!</p></td>
                                        </tr><tr><td valign="top">
                                                <table cellpadding="0" border="0" align="left" cellspacing="0" class="mobile-full" style="padding-top: 4px;"><tbody><tr><td width="auto" valign="middle" bgcolor="#bfbfbf" align="center" height="26" style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; text-align: center; color: #3f3f3f; font-weight: normal; padding-left: 18px; padding-right: 18px; background-color: #bfbfbf; border-radius: 4px;">
                                                            <a style="text-decoration: none; color: #3f3f3f; font-weight: normal;" target="_new" href="<?= __SITE_URL__, $this->url('FrontendUserSelfies', [':username' => $nico['username']])?>">visit <strong><?= $nico['username']?></strong>'s profile</a>
                                                        </td>
                                                    </tr></tbody></table></td>
                                        </tr></tbody></table></div>
                                <!--[if (gte mso 9)|(lte ie 8)]></td><![endif]-->§
                                <!--[if (gte mso 9)|(lte ie 8)]></tr></table><![endif]-->

                            </td>
                        </tr></tbody></table></div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
            </td>
        </tr></tbody></table><table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#595959" style="background-color: #595959;" id="ko_titleBlock_1"><tbody><tr><td class="vb-outer" align="center" valign="top" bgcolor="#595959" style="padding-left: 9px;padding-right: 9px;background-color: #595959;">

                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" border="0" cellpadding="0" cellspacing="9" class="vb-container halfpad" bgcolor="#595959" style="border-collapse: separate;border-spacing: 9px;padding-left: 9px;padding-right: 9px;width: 100%;max-width: 570px;background-color: #595959;"><tbody><tr><td bgcolor="#595959" align="center" style="background-color: #595959; font-size: 22px; font-family: Arial, Helvetica, sans-serif; color: #eeece1; text-align: center;">
                                <span>the website<br></span>
                            </td>
                        </tr></tbody></table></div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
            </td>
        </tr></tbody></table><table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#595959" style="background-color: #595959;" id="ko_tripleArticleBlock_13"><tbody><tr><td class="vb-outer" align="center" valign="top" bgcolor="#595959" style="padding-left: 9px;padding-right: 9px;background-color: #595959;">

                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" border="0" cellpadding="0" cellspacing="9" class="vb-row fullpad" bgcolor="#595959" style="border-collapse: separate;border-spacing: 9px;width: 100%;max-width: 570px;background-color: #595959;"><tbody><tr><td align="center" valign="top" class="mobile-row" style="font-size: 0;">

                                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="552"><tr><![endif]-->
                                <!--[if (gte mso 9)|(lte ie 8)]><td align="left" valign="top" width="184"><![endif]-->
                                <div style="display: inline-block; max-width: 184px; vertical-align: top; width: 100%;" class="mobile-full">

                                    <table class="vb-content" border="0" cellspacing="9" cellpadding="0" width="184" style="border-collapse: separate;width: 100%;" align="left"><tbody><tr><td width="100%" valign="top" align="left" class="links-color" style="padding-bottom: 9px;">

                                                <img border="0" hspace="0" vspace="0" width="166" class="mobile-full" alt="" style="border: 0px;display: block;vertical-align: top;width: 100%;" src="<?= __SITE_URL__, selfie($randomSelfies[0]['hash'], 'thumb_' . __ME__)?>"></td>
                                        </tr><tr><td style="font-size: 18px; font-family: Arial, Helvetica, sans-serif; color: #eeece1; text-align: left;">
                                                <span style="color: #eeece1;">discover<br></span>
                                            </td>
                                        </tr><tr><td align="left" class="long-text links-color" style="text-align: left; font-size: 13px; font-family: Arial, Helvetica, sans-serif; color: #eeece1;"><p style="margin: 1em 0px;margin-bottom: 0px;margin-top: 0px;">don't miss out on any new posts by other people</p></td>
                                        </tr><tr><td valign="top">
                                                <table cellpadding="0" border="0" align="left" cellspacing="0" class="mobile-full" style="padding-top: 4px;"><tbody><tr><td width="auto" valign="middle" bgcolor="#bfbfbf" align="center" height="26" style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; text-align: center; color: #3f3f3f; font-weight: normal; padding-left: 18px; padding-right: 18px; background-color: #bfbfbf; border-radius: 4px;">
                                                            <a style="text-decoration: none; color: #3f3f3f; font-weight: normal;" target="_blank" href="<?= __SITE_URL__, $this->url('FrontendDiscover');?>">go<br></a>
                                                        </td>
                                                    </tr></tbody></table></td>
                                        </tr></tbody></table></div><!--[if (gte mso 9)|(lte ie 8)]></td>
                                <td align="left" valign="top" width="184">
                                <![endif]--><div style="display: inline-block; max-width: 184px; vertical-align: top; width: 100%;" class="mobile-full">

                                    <table class="vb-content" border="0" cellspacing="9" cellpadding="0" width="184" style="border-collapse: separate;width: 100%;" align="left"><tbody><tr><td width="100%" valign="top" align="left" class="links-color" style="padding-bottom: 9px;">

                                                <img border="0" hspace="0" vspace="0" width="166" class="mobile-full" alt="" style="border: 0px;display: block;vertical-align: top;width: 100%;height: auto;" src="<?= __SITE_URL__, selfie($randomSelfies[1]['hash'], 'thumb_' . __ME__)?>"></td>
                                        </tr><tr><td style="font-size: 18px; font-family: Arial, Helvetica, sans-serif; color: #eeece1; text-align: left;">
                                                <span style="color: #eeece1;">users<br></span>
                                            </td>
                                        </tr><tr><td align="left" class="long-text links-color" style="text-align: left; font-size: 13px; font-family: Arial, Helvetica, sans-serif; color: #eeece1;"><p style="margin: 1em 0px;margin-bottom: 0px;margin-top: 0px;">every user is a story, and every story appears here<br></p></td>
                                        </tr><tr><td valign="top">
                                                <table cellpadding="0" border="0" align="left" cellspacing="0" class="mobile-full" style="padding-top: 4px;"><tbody><tr><td width="auto" valign="middle" bgcolor="#bfbfbf" align="center" height="26" style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; text-align: center; color: #3f3f3f; font-weight: normal; padding-left: 18px; padding-right: 18px; background-color: #bfbfbf; border-radius: 4px;">
                                                            <a style="text-decoration: none; color: #3f3f3f; font-weight: normal;" target="_blank" href="<?= __SITE_URL__, $this->url('FrontendUsers');?>">go<br></a>
                                                        </td>
                                                    </tr></tbody></table></td>
                                        </tr></tbody></table></div><!--[if (gte mso 9)|(lte ie 8)]></td>
                                <td align="left" valign="top" width="184">
                                <![endif]--><div style="display: inline-block; max-width: 184px; vertical-align: top; width: 100%;" class="mobile-full">

                                    <table class="vb-content" border="0" cellspacing="9" cellpadding="0" width="184" style="border-collapse: separate;width: 100%;" align="right"><tbody><tr><td width="100%" valign="top" align="left" class="links-color" style="padding-bottom: 9px;">

                                                <img border="0" hspace="0" vspace="0" width="166" class="mobile-full" alt="" style="border: 0px;display: block;vertical-align: top;width: 100%;height: auto;" src="<?= __SITE_URL__, selfie($randomSelfies[2]['hash'], 'thumb_' . __ME__)?>"></td>
                                        </tr><tr><td style="font-size: 18px; font-family: Arial, Helvetica, sans-serif; color: #eeece1; text-align: left;">
                                                <span style="color: #eeece1;">posts<br></span>
                                            </td>
                                        </tr><tr><td align="left" class="long-text links-color" style="text-align: left; font-size: 13px; font-family: Arial, Helvetica, sans-serif; color: #eeece1;"><p style="margin: 1em 0px;margin-bottom: 0px;margin-top: 0px;">in a list feed, or one by one. you choose how to read<br></p></td>
                                        </tr><tr><td valign="top">
                                                <table cellpadding="0" border="0" align="left" cellspacing="0" class="mobile-full" style="padding-top: 4px;"><tbody><tr><td width="auto" valign="middle" bgcolor="#bfbfbf" align="center" height="26" style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; text-align: center; color: #3f3f3f; font-weight: normal; padding-left: 18px; padding-right: 18px; background-color: #bfbfbf; border-radius: 4px;">
                                                            <a style="text-decoration: none; color: #3f3f3f; font-weight: normal;" target="_blank" href="<?= __SITE_URL__, $this->url('Home')?>">go<br></a>
                                                        </td>
                                                    </tr></tbody></table></td>
                                        </tr></tbody></table></div>
                                <!--[if (gte mso 9)|(lte ie 8)]></td><![endif]-->
                                <!--[if (gte mso 9)|(lte ie 8)]></tr></table><![endif]-->

                            </td>
                        </tr></tbody></table></div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
            </td>
        </tr></tbody></table>
