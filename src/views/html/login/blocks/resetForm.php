<?php
    $memory = [
        ["The advantage of a bad memory is that one enjoys several times the same good things for the first time.", "Friedrich Nietzsche"],
        ["A clear conscience is the sure sign of a bad memory.", "Mark Twain"],
        ["Time moves in one direction, memory in another.", "William Gibson"],
        ["No man has a good enough memory to be a successful liar.", "Abraham Lincoln"],
        ["Take care of all your memories. For you cannot relive them.", "Bob Dylan"],
        ["The richness of life lies in memories we have forgotten.", "Cesare Pavese"]
    ];
?>

<form id="resetForm" name="resetForm" ng-submit="doSubmit(resetForm, 'reset', 'reset')" novalidate ng-show="showReset">
    <fieldset>
        <div class="form-group">
            <label>Email</label>
            <input class="form-control" ng-value="::reset.email" readonly="" type="text" name="email"/>
        </div>
        <div class="form-group valideight-box has-feedback">
            <label>password</label>
            <input class="form-control" placeholder="new password" name="password" autofocus=""
                   type="password" ng-model="::reset.password" required="" minlength="8">
            <span ng-messages="resetForm.password.$error" class="error-block"
                  ng-show="h.invalid(resetForm, 'password')">
                <span ng-message="required">please enter a password</span>
                <span ng-message="minlength">8 characters or more</span>
            </span>
        </div>
        <div class="form-group valideight-box has-feedback">
            <label>confirm password</label>
            <input class="form-control" placeholder="repeat password" name="confirm"
                   type="password" ng-model="::reset.confirm" required="" minlength="8"
                   ng-change="resetPasswordsMatch = (reset.password == reset.confirm)">
            <span class="error-block" ng-show="reset.password && reset.confirm && !resetPasswordsMatch">
                passwords don't match!
            </span>
        </div>
        <div class="form-group">
            <label>here's a quote for the occasion:</label>
            <blockquote>
                <?php list($quote, $author) = $memory[array_rand($memory)]; ?>
                <p><?php echo $quote ?></p>
                <footer><?php echo $author ?></footer>
            </blockquote>
        </div>
        <button type="submit" class="btn btn-block btn-success"
                ng-show="resetPasswordsMatch">set new password</button>
        <button class="btn btn-block btn-danger" disabled="disabled" style="opacity:0.7;cursor: not-allowed"
                ng-hide="resetPasswordsMatch">set new password</button>
    </fieldset>
</form>