<form name="forgotForm" id="forgotForm" ng-submit="::doSubmit(forgotForm, 'forgot', 'forgot')"
      novalidate ng-show="showForgot">
    <fieldset>
        <div class="form-group">
            <input class="form-control" placeholder="email" name="email" type="text" autofocus=""
                   required="" id="_username" ng-model="::forgot.email">
        </div>
        <input class="btn btn-lg btn-success btn-block" type="submit" value="recover">
    </fieldset>
</form>