<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="viewport" content="initial-scale=1.0"><meta name="format-detection" content="telephone=no"><title><?= __SITE_NAME__?> email</title><style type="text/css">table.vb-row.fullwidth {border-spacing: 0;padding: 0;}
        table.vb-container.fullwidth {padding-left: 0;padding-right: 0;}</style><style type="text/css">
        /* yahoo, hotmail */
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{ line-height: 100%; }
        .yshortcuts a{ border-bottom: none !important; }
        .vb-outer{ min-width: 0 !important; }
        .RMsgBdy, .ExternalClass{
            width: 100%;
            background-color: #3f3f3f;
            background-color: #3f3f3f}

        /* outlook */
        table{ mso-table-rspace: 0pt; mso-table-lspace: 0pt; }
        #outlook a{ padding: 0; }
        img{ outline: none; text-decoration: none; border: none; -ms-interpolation-mode: bicubic; }
        a img{ border: none; }

        @media screen and (max-device-width: 600px), screen and (max-width: 600px) {
            table.vb-container, table.vb-row{
                width: 95% !important;
            }

            .mobile-hide{ display: none !important; }
            .mobile-textcenter{ text-align: center !important; }

            .mobile-full{
                float: none !important;
                width: 100% !important;
                max-width: none !important;
                padding-right: 0 !important;
                padding-left: 0 !important;
            }
            img.mobile-full{
                width: 100% !important;
                max-width: none !important;
                height: auto !important;
            }
        }
    </style><style type="text/css">#ko_tripleArticleBlock_13 .links-color a, #ko_tripleArticleBlock_13 .links-color a:link, #ko_tripleArticleBlock_13 .links-color a:visited, #ko_tripleArticleBlock_13 .links-color a:hover {color: #3f3f3f;color: #c4bd97;text-decoration: underline;}
        #ko_doubleArticleBlock_7 .links-color a, #ko_doubleArticleBlock_7 .links-color a:link, #ko_doubleArticleBlock_7 .links-color a:visited, #ko_doubleArticleBlock_7 .links-color a:hover {color: #3f3f3f;color: #938953;text-decoration: underline;}
        #ko_textBlock_5 .links-color a:visited, #ko_textBlock_5 .links-color a:hover {color: #3f3f3f;color: #c4bd97;text-decoration: underline;}
        #ko_textBlock_12 .links-color a:visited, #ko_textBlock_12 .links-color a:hover {color: #3f3f3f;color: #c4bd97;text-decoration: underline;}
        #ko_textBlock_5 .long-text p:first-child {margin-top: 0px;}
        #ko_socialBlock_9 .links-color a:visited, #ko_socialBlock_9 .links-color a:hover {color: #ccc;color: #ccc;text-decoration: underline;}
        #ko_footerBlock_2 .links-color a:visited, #ko_footerBlock_2 .links-color a:hover {color: #ccc;color: #ccc;text-decoration: underline;}</style></head><body bgcolor="#3f3f3f" text="#919191" alink="#cccccc" vlink="#cccccc" style="margin: 0;padding: 0;background-color: #3f3f3f;color: #919191;">

<center>

    <!-- preheaderBlock -->


    <?php if(!empty($viewInBrowser) || !empty($canUnsubscribe)) { ?>

    <table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#3f3f3f" style="background-color: #3f3f3f;" id="ko_preheaderBlock_1"><tbody><tr><td class="vb-outer" align="center" valign="top" bgcolor="#3f3f3f" style="padding-left: 9px;padding-right: 9px;background-color: #3f3f3f;">
                <div style="display: none; font-size: 1px; color: #333333; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;"></div>

                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" border="0" cellpadding="0" cellspacing="0" class="vb-row halfpad" bgcolor="#3f3f3f" style="border-collapse: separate;border-spacing: 0;padding-left: 9px;padding-right: 9px;width: 100%;max-width: 570px;background-color: #3f3f3f;"><tbody><tr><td align="center" valign="top" bgcolor="#3f3f3f" style="font-size: 0; background-color: #3f3f3f;">

                                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="552"><tr><![endif]-->
                                <!--[if (gte mso 9)|(lte ie 8)]><td align="left" valign="top" width="276"><![endif]-->
                                <div style="display: inline-block; max-width: 276px; vertical-align: top; width: 100%;" class="mobile-full">
                                    <table class="vb-content" border="0" cellspacing="9" cellpadding="0" width="276" style="border-collapse: separate;width: 100%;" align="left"><tbody>

                                        <?php if (!empty($canUnsubscribe)) { ?>
                                            <tr><td width="100%" valign="top" align="left" style="font-weight: normal; text-align: left; font-size: 13px; font-family: Arial, Helvetica, sans-serif; color: #ffffff;">
                                                    <a style="text-decoration: underline; color: #ffffff;" target="_new" href="<?php echo __SITE_URL__, $this->url('EmailUnsubscribe', [':usertoken' => $userToken])?>">Unsubscribe</a>

                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody></table></div><!--[if (gte mso 9)|(lte ie 8)]>
                                </td><td align="left" valign="top" width="276">
                                <![endif]--><div style="display: inline-block; max-width: 276px; vertical-align: top; width: 100%;" class="mobile-full mobile-hide">

                                    <table class="vb-content" border="0" cellspacing="9" cellpadding="0" width="276" style="border-collapse: separate;width: 100%;text-align: right;" align="left"><tbody><tr><td width="100%" valign="top" style="font-weight: normal; font-size: 13px; font-family: Arial, Helvetica, sans-serif; color: #ffffff;">
                      <?php if (!empty($viewInBrowser)) { ?>
                        <span style="color: #ffffff; text-decoration: underline;">
                          <a style="text-decoration: underline; color: #ffffff;" href="<?php echo __SITE_URL__, $this->url('EmailView', [':emailtemplate' => 'welcome', ':usertoken' => $userToken])?>">View in your browser</a>
                         </span>
                    <?php } ?>
                                            </td>
                                        </tr></tbody></table></div><!--[if (gte mso 9)|(lte ie 8)]>
                                </td></tr></table><![endif]-->

                            </td>
                        </tr></tbody></table></div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->



                <?php } ?>
            </td>
        </tr></tbody></table><!-- /preheaderBlock --><table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#595959" style="background-color: #595959;" id="ko_logoBlock_3"><tbody><tr><td class="vb-outer" align="center" valign="top" bgcolor="#595959" style="padding-left: 9px;padding-right: 9px;background-color: #595959;">

                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" style="border-collapse: separate;border-spacing: 18px;padding-left: 0;padding-right: 0;width: 100%;max-width: 570px;" border="0" cellpadding="0" cellspacing="18" class="vb-container fullpad"><tbody><tr><td valign="top" align="center">

                                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="258"><tr><td align="center" valign="top"><![endif]-->
                                <div class="mobile-full" style="display: inline-block; max-width: 258px; vertical-align: top; width: 100%;">
                                    <img width="258" vspace="0" hspace="0" border="0" alt="" style="border: 0px;display: block;width: 100%;max-width: 258px;" src="<?php echo __SITE_URL__, $this->asset('images/icons/geonaut.png')?>"></div>
                                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->

                            </td>
                        </tr></tbody></table></div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->

            </td>
        </tr></tbody></table><table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#595959" style="background-color: #595959;" id="ko_titleBlock_4"><tbody><tr><td class="vb-outer" align="center" valign="top" bgcolor="#595959" style="padding-left: 9px;padding-right: 9px;background-color: #595959;">

                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" border="0" cellpadding="0" cellspacing="9" class="vb-container halfpad" bgcolor="#595959" style="border-collapse: separate;border-spacing: 9px;padding-left: 9px;padding-right: 9px;width: 100%;max-width: 570px;background-color: #595959;"><tbody><tr><td bgcolor="#595959" align="center" style="background-color: #595959; font-size: 22px; font-family: Arial, Helvetica, sans-serif; color: #eeece1; text-align: center;">
                                <span><span id="_mce_caret" data-mce-bogus="true"><strong><a href="<?= __SITE_URL__?>" style="color:white !important" target="_blank"><?= __SITE_NAME__?></a></strong></span><br data-mce-bogus="1"></span>
                            </td>
                        </tr></tbody></table></div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
            </td>
        </tr></tbody></table>
    <?php echo $_body;?>
    <table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#101010" style="background-color: #101010;" id="ko_logoBlock_8"><tbody><tr><td class="vb-outer" align="center" valign="top" bgcolor="#101010" style="padding-left: 9px;padding-right: 9px;background-color: #101010;">

                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" style="border-collapse: separate;border-spacing: 18px;padding-left: 0;padding-right: 0;width: 100%;max-width: 570px;" border="0" cellpadding="0" cellspacing="18" class="vb-container fullpad"><tbody><tr><td valign="top" align="center">

                                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="258"><tr><td align="center" valign="top"><![endif]-->
                                <div class="mobile-full" style="display: inline-block; max-width: 258px; vertical-align: top; width: 100%;">
                                    <img width="258" vspace="0" hspace="0" border="0" alt="" style="border: 0px;display: block;width: 100%;max-width: 258px;" src="<?php echo __SITE_URL__, $this->asset('images/icons/generic/thats-all-folks-email.jpg')?>"></div>
                                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->

                            </td>
                        </tr></tbody></table></div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->

            </td>
        </tr></tbody></table><table width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#3f3f3f" style="background-color: #3f3f3f;" id="ko_socialBlock_9"><tbody><tr><td align="center" valign="top" bgcolor="#3f3f3f" style="background-color: #3f3f3f;">
                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" style="border-collapse: separate;border-spacing: 9px;width: 100%;max-width: 570px;" border="0" cellpadding="0" cellspacing="9" class="vb-row fullpad" align="center"><tbody><tr><td valign="top" align="center" style="font-size: 0;">

                                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="552"><tr><![endif]-->
                                <!--[if (gte mso 9)|(lte ie 8)]><td align="left" valign="top" width="276"><![endif]-->
                                <div style="display: inline-block; max-width: 276px; vertical-align: top; width: 100%;" class="mobile-full">

                                    <table class="vb-content" border="0" cellspacing="9" cellpadding="0" width="276" style="border-collapse: separate;width: 100%;" align="center"><tbody><tr><td valign="middle" align="left" class="long-text links-color mobile-textcenter" style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; color: #919191; text-align: center;">
                                                <p style="margin: 1em 0px;margin-bottom: 0px;margin-top: 0px;">Made with love by <a href="http://kupfer.es/" style="color: #ccc;text-decoration: underline;" target="_blank">Nico Kupfer</a></p>
                                            </td>
                                        </tr></tbody></table></div><!--[if (gte mso 9)|(lte ie 8)]></td>
                                <td align="left" valign="top" width="276">
                                <![endif]-->

                                <?php if (!empty($social)) { ?>
                                    <div style="display: inline-block; max-width: 276px; vertical-align: top; width: 100%;" class="mobile-full">
                                        <table class="vb-content" border="0" cellspacing="9" cellpadding="0" width="276" style="border-collapse: separate;width: 100%;" align="right"><tbody><tr><td align="right" valign="middle" class="links-color socialLinks mobile-textcenter" style="font-size: 6px;">
                                                     
                                                    <a target="_new" href="" style="display: inline-block;color: #ccc;text-decoration: underline;">
                                                        <img src="https://mosaico.io/templates/versafix-1/img/social_def/facebook_ok.png" alt="Facebook" border="0" class="socialIcon" style="border: 0px;border-radius: 100%;display: inline-block;vertical-align: top;padding-bottom: 0px;"></a>
                                                     
                                                    <a target="_new" href="" style="display: inline-block;color: #ccc;text-decoration: underline;">
                                                        <img src="https://mosaico.io/templates/versafix-1/img/social_def/twitter_ok.png" alt="Twitter" border="0" class="socialIcon" style="border: 0px;border-radius: 100%;display: inline-block;vertical-align: top;padding-bottom: 0px;"></a>
                                                     
                                                    <a target="_new" href="" style="display: inline-block;color: #ccc;text-decoration: underline;">
                                                        <img src="https://mosaico.io/templates/versafix-1/img/social_def/google+_ok.png" alt="Google+" border="0" class="socialIcon" style="border: 0px;border-radius: 100%;display: inline-block;vertical-align: top;padding-bottom: 0px;"></a></td>

                                            </tr></tbody></table>
                                    </div>
                                <?php } ?>
                                <!--[if (gte mso 9)|(lte ie 8)]></td><![endif]-->
                                <!--[if (gte mso 9)|(lte ie 8)]></tr></table><![endif]-->

                            </td>
                        </tr></tbody></table></div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
            </td>
        </tr></tbody></table><!-- footerBlock -->

    <table width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#3f3f3f" style="background-color: #3f3f3f;" id="ko_footerBlock_2"><tbody><tr><td align="center" valign="top" bgcolor="#3f3f3f" style="background-color: #3f3f3f;">

                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" style="border-collapse: separate;border-spacing: 9px;padding-left: 9px;padding-right: 9px;width: 100%;max-width: 570px;" border="0" cellpadding="0" cellspacing="9" class="vb-container halfpad" align="center"><tbody><tr><td class="long-text links-color" style="text-align: center; font-size: 13px; color: #919191; font-weight: normal; text-align: center; font-family: Arial, Helvetica, sans-serif;"><p style="margin: 1em 0px;margin-bottom: 0px;margin-top: 0px;">Email sent to <a href="mailto:<?= $user['email']?>" style="color: #ccc;text-decoration: underline;"><?= $user['email']?></a></p></td>
                        </tr>

                        <?php if (!empty($canUnsubscribe)) { ?>
                            <tr><td style="text-align: center;">
                                    <a style="text-decoration: underline; color: #ffffff; text-align: center; font-size: 13px; font-weight: normal; font-family: Arial, Helvetica, sans-serif;" target="_new" href="<?php echo __SITE_URL__, $this->url('EmailUnsubscribe', [':usertoken' => $userToken])?>">Unsubscribe</a>
                                </td>
                            </tr>
                        <?php } ?></tbody></table></div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
            </td>
        </tr></tbody></table><!-- /footerBlock --></center>

</body></html>