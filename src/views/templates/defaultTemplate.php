<?php $pc = \Core\ParentController::singleton(); ?>
<!DOCTYPE html>
<html ng-app="geonaut" ng-strict-di>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0"/>
    <meta name="HandheldFriendly" content="true" />
    <link rel="mask-icon" href="<?php $pc->asset('images/icons/system_icons/geonaut_safari.svg') ?>" color="navy">
    <link href="<?php $pc->asset(!empty($_favicon) ? $_favicon : 'images/icons/geonaut.png');?>" rel="shortcut icon" type="image/x-icon" />

    <title ng-bind-html="(title ? title + ' - ' : '') + '<?php echo __SITE_NAME__;?>'">
        <?php echo (!empty($_title) ? unescape($_title) . " - " : "") . __SITE_NAME__;?></title>
    <meta property="og:title" content="<?php echo (!empty($_title) ? unescape($_title) . " - " : "") . __SITE_NAME__;?>">
    <meta property="og:site_name" content="<?= __SITE_NAME__ ?>"/>

    <meta name="description" content="<?php echo (!empty($_descr) ? unescape($_descr) : __SITE_DESCR__);?>">
    <meta property="og:description" content="<?php echo (!empty($_descr) ? unescape($_descr) : __SITE_DESCR__);?>">

    <meta property="og:url" content="<?php echo __SITE_URL__ . $_SERVER['REQUEST_URI'];?>">

    <meta property="og:image" content="<?php echo (!empty($_ogImage) ? $_ogImage : (__SITE_URL__ . $pc->asset('images/icons/geonaut.png', TRUE))); ?>">
    <?php if (!empty($_ogSize)) { ?>
        <meta property="og:image:width"  content="<?php echo $_ogSize['width'];  ?>"><meta property="og:image:height" content="<?php echo $_ogSize['height']; ?>">
    <?php } ?>

    <meta property="fb:app_id" content="<?= __FB_APP_ID__ ?>"/>
    <meta property="og:type" content="website">

    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />
    <link href="<?php $this->asset('css/ng-all.css');?>" type="text/css" rel="stylesheet"/>
    <?php echo $_style; // Extra, custom-set styles?>
</head>
<body ng-class="{'dark-theme': darkTheme}" ng-controller="MainController as main"><?php // This is to avoid weird side-scrollings! ?>

<div id="container-fluid">

    <?php /* Header */ echo $_header; ?>

    <base href="<?php $this->path('Home');?>"/>
    <span id="body_box" class="body_box"
          ng-view ng-hide="errorPage || errorSelfie">
        <?php echo $_body; // Here goes all the body content ?>
    </span>
    <span ng-if="errorPage || errorSelfie" style="height:100%">
        <?php echo $this->get('html/error/404_not_found');?>
    </span>

    <div ng-if="notificationsSplash" id="notifications-splash">
        <?php echo $this->get('html/blocks/notifications'); ?>
    </div>

    <div id="overlay" ng-show="lMenuOpen || rMenuOpen" ng-click="menu('l', false) && menu('r', false)"></div>
    <?php echo $this->get('html/blocks/cookies'); ?>
</div>
<?php echo $this->get('html/blocks/loader'); ?>

<?php /* Footer */ echo $_footer; ?>
<div id='fb-root'></div>


<script type="text/javascript" src="<?php $this->asset('js/ng-all.js');?>"
        defer="defer"></script>
<script type="text/javascript" src="<?= Controller::singleton()->mapboxURL ?>" defer="defer"></script>
<script type="text/javascript" defer="defer">
    <?php echo $_js; // JS vars added from the controllers?>
    <?php if ($alert = \Core\Session::getAlert()) {
        \Core\Session::cleanAlert();?>
        var __alert__ = <?= json_encode($alert); ?>;
    <?php } ?>
    <?php echo $pc->get('html/blocks/preloads');?>
</script>
<?php echo $_script; // Extra, custom-set scripts?>
</body>
</html>