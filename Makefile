XDG_CONFIG_HOME ?= $(HOME)/.config
export CLOUDSDK_CONFIG ?= $(XDG_CONFIG_HOME)/gcloud
export KUBECONFIG ?= $(HOME)/.kube/config

export PULZE_DEBUG := True
export PULZE_ENVIRONMENT := local

.SILENT: help
.PHONY: help
help:
	echo Available recipes:
	cat $(MAKEFILE_LIST) | grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' | awk 'BEGIN { FS = ":.*?## "; white="\033[1;37m"; reset="\033[0m" } { cnt++; a[cnt] = $$1; b[cnt] = $$2; if (length($$1) > max) max = length($$1) } END { for (i = 1; i <= cnt; i++) printf "  $(shell tput setaf 6)%-*s$(shell tput sgr0) %s%s%s\n", max, a[i], white, b[i], reset }'
	tput sgr0

.PHONY: dev
dev: ## Run local development environment
	docker compose up --build

.PHONY: clean
clean: ## Clean up local development
	docker compose down --remove-orphans --rmi all --volumes

.PHONY: logs log
log logs: ## + <CONTAINER_NAME> -- Follows the logs for the specified container
	docker-compose logs -f $(CONTAINER_NAME)
